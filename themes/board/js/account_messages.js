$(function() {

$(".openmesdialog").click(function() {

  var to = $(this).attr('touser');
  var username = $(this).attr('username');
  var from = $(this).attr('fromuser');

  $('#message_alert').hide();
  $('#talkusername').html(username);
  $('#frame_message_dlg_user').attr('src','/account/messagelist/all/'+from);
  $('#inp__obj_to_user').val(from);

  $('#messageModal').modal('show');
});










$("#account_send_message").click(function() {
  
  var _message = $('#inp__obj_send_message').val();
  var _to = $('#inp__obj_to_user').val();


  var jqxhr = $.post( "/ajax/sendmessage", {message:_message,to:_to,command:'account_send'} ,function(data) {
    var obj = jQuery.parseJSON(data);
    if (obj.count_errors>0){
       $("#message_alert").removeClass('alert-info');
       $("#message_alert").addClass("alert-danger");
       $('#message_alert').html(obj.error);
       $('#message_alert').show();
    }else{
       $("#message_alert").removeClass('alert-danger');
       $("#message_alert").addClass("alert-info");
       $('#message_alert').html(obj.info_message);
       $('#inp__obj_send_message').val('');
       var d = new Date();
       var n = d.getTime();
       $('#frame_message_dlg_user').attr('src','/account/messagelist/all/'+_to+'?time='+n);
       $('#message_alert').show();
    }
  });
  
  
});




});
