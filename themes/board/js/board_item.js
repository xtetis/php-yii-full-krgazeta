$(document).ready(function(){



$("#board_item_send_message").click(function() {
  
  $('#message_alert').hide();
  var sdata = $("#frm__send_message_to_user").serialize();

  var jqxhr = $.post( "/ajax/sendmessage", sdata ,function(data) {
    var obj = jQuery.parseJSON(data);
    if (obj.count_errors>0){
       $("#message_alert").removeClass('alert-info');
       $("#message_alert").addClass("alert-danger");
       $('#message_alert').html(obj.error);
       $('#message_alert').show();
    }else{
       $("#message_alert").removeClass('alert-danger');
       $("#message_alert").addClass("alert-info");
       $('#message_alert').html(obj.info_message);
       $('#inp__obj_send_message').val('');
       $('#inp__obj_send_email').val('');
       $('#inp__obj_send_name').val('');
       $('#message_alert').show();
    }
  });
  
  
});



















// Если нажата кнопка "Отправить жалобу" и выбран пункт
//==============================================================================
$(".btn_obj_abuse_item").click(function() {
  $('#inp__obj_abuse_message').val($(this).html());
  var idx=$(this).attr('idx');
  //Проверяем авторизирован ли юзер
  $.get("/ajax/fn__get_user_id", function(data) {
      if (data==0){
         $('#modal_obj_abuse').modal('show');
         }else{
         if (idx!=5){
            $("#board_item_send_abuse").click();
            }else{
            $('#modal_obj_abuse').modal('show');
            }
         }
    });
});
//==============================================================================













// Если нажата кнопка "Отправить жалобу" на форме
//==============================================================================
$("#board_item_send_abuse").click(function() {
  $('#board_item_abuse_alert').hide();
  var sdata = $("#frm__send_abuse").serialize();
  var jqxhr = $.post( "/ajax/sendabuse", sdata ,function(data) {
    var obj = jQuery.parseJSON(data);
    if (obj.count_errors>0){
       $('#board_item_abuse_alert').html(obj.error);
       $('#board_item_abuse_alert').show();
    }else{
       $('#board_item_abuse_alert').hide();
       $('#obj_item_alert_container').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Жалоба на объявление отправлена</div>');
       $('#modal_obj_abuse').modal('hide');
       $("html, body").animate({ scrollTop: 0 }, 600);
       
       $('#obj_item_alert_container').fadeToggle();
       $('#obj_item_alert_container').fadeToggle();
       $('#obj_item_alert_container').fadeToggle();
       $('#obj_item_alert_container').fadeToggle();
       $('#obj_item_alert_container').fadeToggle();
       $('#obj_item_alert_container').fadeToggle();
       
    }
  });
});
//==============================================================================








});
