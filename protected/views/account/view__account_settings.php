<?
$this->pageTitle='Мои настройки';
?>


<div style="padding:10px; padding-top:10px;">



<?
$params['category']='settings_common';
$this->widget('application.components.frontend.UserTopMenu',array('params'=>$params)); 
?>


<?if(strlen($info['alert'])):?>
<div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 10px; margin-bottom: 0px;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <?=$info['alert']?>
</div>
<?endif;?>


	<div class="panel panel-default" style="margin-top: 10px;">
		<div class="panel-heading">
		  <h3 class="panel-title">Общие настройки</h3>
		</div>
		<div class="panel-body">
			<form method="post">

			<div class="row">
				<div class="col-md-2" style="padding-top:10px;">
					<strong>Имя пользователя <span class="red" title="Обязательное поле">*</span></strong>
				</div>
				<div class="col-md-7">
				  <div class=" popover_hover" 
				       data-title="Контактное лицо" 
				       data-content="Как к Вам обращаться?" >
				    <input class="form-control" 
				           name="formdata[username]" 
				           type="text" 
				           value="<?=$info['username']?>" 
				           maxlength="40" 
				           required>

				    <div class="form_element_<?=((strlen($info['errform']['username']))?'error_message':'message')?>">
				       <?=((strlen($info['errform']['username']))?$info['errform']['username']:
				                                           'Максимальное количество символов - 40')?>
				    </div>
				  </div>
				</div>
			</div>
			
			
			
			
			<div class="row" style="margin-top: 10px;">
				<div class="col-md-2" style="padding-top:10px;">
					<strong>Номер телефона <span class="red" title="Обязательное поле">*</span></strong>
				</div>
				<div class="col-md-7">
				  <div class=" popover_hover" 
				       data-title="Номер телефона" 
				       data-content="Укажите контактный номер телефона" >
				    <input class="form-control" 
				           name="formdata[phone]" 
				           type="text" 
				           value="<?=$info['phone']?>" 
				           maxlength="40" 
				           required>

				    <div class="form_element_<?=((strlen($info['errform']['phone']))?'error_message':'message')?>">
				       <?=((strlen($info['errform']['phone']))?$info['errform']['phone']:
				                                           'Только цифры (формат 0971234567)')?>
				    </div>
				  </div>
				</div>
			</div>






			<div class="row" style="margin-top: 10px;">
				<div class="col-md-2" style="padding-top:10px;">
					<strong>Skype</strong>
				</div>
				<div class="col-md-7">
				  <div class=" popover_hover" 
				       data-title="Skype" 
				       data-content="Укажите skype, чтобы с Вами могли связаться" >
				    <input class="form-control" 
				           name="formdata[skype]" 
				           type="text" 
				           value="<?=$info['skype']?>" 
				           maxlength="40" 
				           required>

						<div class="form_element_<?=((strlen($info['errform']['skype']))?'error_message':'message')?>">
							 <?=((strlen($info['errform']['skype']))?$info['errform']['skype']:
												                           'Максимальное количество символов - 20')?>
						</div>
				  </div>
				</div>
			</div>
			
			
			
			<div class="row" style="margin-top: 10px;">
				<div class="col-md-7 col-md-offset-2">
					<button class="btn btn-primary" name="sbm_settings_common" type="submit">Сохранить</button>
				</div>
			</div>



			</form>
		</div>
	</div>



    
    











</div>
