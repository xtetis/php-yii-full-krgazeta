<?
$this->pageTitle='Смена пароля';
?>


<div style="padding:10px; padding-top:10px;">



<?
$params['category']='settings_password';
$this->widget('application.components.frontend.UserTopMenu',array('params'=>$params)); 
?>


<?if(strlen($info['alert'])):?>
<div class="alert <?=$info['alert_class']?> alert-dismissible" role="alert" style="margin-top: 10px; margin-bottom: 0px;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <?=$info['alert']?>
</div>
<?endif;?>


	<div class="panel panel-default" style="margin-top: 10px;">
		<div class="panel-heading">
		  <h3 class="panel-title">Смена пароля</h3>
		</div>
		<div class="panel-body">
			<form method="post">

			<div class="row">
				<div class="col-md-2" style="padding-top:10px;">
					<strong>Текущий пароль <span class="red" title="Обязательное поле">*</span></strong>
				</div>
				<div class="col-md-7">
				  <div class=" popover_hover" 
				       data-title="Текущий пароль" 
				       data-content="Введите пароль, который используете для входа на сайт" >
				    <input class="form-control" 
				           name="formdata[current_password]" 
				           type="password" 
				           value="<?=$info['current_password']?>" 
				           maxlength="10" 
				           required>

				    <div class="form_element_<?=((strlen($info['errform']['current_password']))?'error_message':
																																												'message')?>">
							<?=((strlen($info['errform']['current_password']))?$info['errform']['current_password']:
							                                                   'Введите текущий пароль')?>
				    </div>
				  </div>
				</div>
			</div>
			
			
			
			
			<div class="row" style="margin-top: 10px;">
				<div class="col-md-2" style="padding-top:10px;">
					<strong>Новый пароль <span class="red" title="Обязательное поле">*</span></strong>
				</div>
				<div class="col-md-7">
				  <div class=" popover_hover" 
				       data-title="Новый пароль" 
				       data-content="Укажите новый пароль, который хотите установить для вашей учетной записи">
				    <input class="form-control" 
				           name="formdata[new_password]" 
				           type="password" 
				           value="<?=$info['new_password']?>" 
				           maxlength="10" 
				           required>

				    <div class="form_element_<?=((strlen($info['errform']['new_password']))?'error_message':
				    																																				'message')?>">
				       <?=((strlen($info['errform']['new_password']))?$info['errform']['new_password']:
				                                           'Введите новый пароль, который хотите использовать')?>
				    </div>
				  </div>
				</div>
			</div>






			<div class="row" style="margin-top: 10px;">
				<div class="col-md-2" style="padding-top:0px;">
					<strong>Подтверждение нового пароля</strong>
				</div>
				<div class="col-md-7">
				  <div class=" popover_hover" 
				       data-title="Подтверждение нового пароля" 
				       data-content="Введите повторно новый пароль для проверки корректности ввода">
				    <input class="form-control" 
				           name="formdata[new_password_confirm]"
				           type="password"
				           value="<?=$info['new_password_confirm']?>"
				           maxlength="40"
				           required>

						<div class="form_element_<?=((strlen($info['errform']['new_password_confirm']))?'error_message':
																																														'message')?>">
							 <?=((strlen($info['errform']['new_password_confirm']))?$info['errform']['new_password_confirm']:
												                           'Введите повторно новый пароль')?>
						</div>
				  </div>
				</div>
			</div>
			
			
			
			<div class="row" style="margin-top: 10px;">
				<div class="col-md-7 col-md-offset-2">
					<button class="btn btn-primary" name="sbm_settings_password" type="submit">Сохранить</button>
				</div>
			</div>



			</form>
		</div>
	</div>



    
    











</div>
