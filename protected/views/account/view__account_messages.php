<?
$this->pageTitle='Мои сообщения';
?>

<script type="text/javascript" src="/themes/board/js/account_messages.js"></script>
<div style="padding:10px; padding-top:10px;">


<?
$params['category']='messages';
$this->widget('application.components.frontend.UserTopMenu',array('params'=>$params)); 
?>




<div style="padding-top:10px; font-size:18px; color:#204a87;">
Все сообщения:
</div>

<div style="padding-top:10px;">
  <?=$info['content']?>
</div>




<div class="modal fade" id="messageModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="padding: 10px;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Общение с пользователем <span id="talkusername">Васей</span></h4>
      </div>
      <div class="modal-body" style="padding:5px;">
        <div class="alert alert-info" role="alert" id="message_alert" 
             style="font-size:13px; display:none; padding:5px; margin-bottom: 5px;">
          
        </div>
<div class="panel panel-default" style="margin-bottom: 5px;">
  <div class="panel-body" style="padding: 5px;">
      <iframe src="/account/messagelist/1?from=1" 
              id="frame_message_dlg_user"
              style="height: 270px; overflow-y: scroll; width: 100%; border:0px;"
              scrolling="yes">
        Ваш браузер не поддерживает плавающие фреймы!
      </iframe>
  </div>
</div>

<div class="panel panel-default" style="margin-bottom: 5px;">
  <div class="panel-heading" style="padding: 5px;">
    <h3 class="panel-title">Ваше сообщение</h3>
  </div>
  <div class="panel-body" style="padding: 5px;">
    <input type="hidden" id="inp__obj_to_user" value="" />
    <textarea style="width: 100%; height: 63px; margin: 0px;" class="form-control"
              id="inp__obj_send_message" name="message" ></textarea>
  </div>
</div>
        
      </div>
      <div class="modal-footer"  style="padding: 10px;">
        <button class="btn btn-primary" id="account_send_message">Отправить</button>
        <button class="btn btn-default" 
                onclick="$('#inp__obj_send_message').val('');" 
                data-dismiss="modal" aria-hidden="true">Закрыть</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



    
    
    











</div>
