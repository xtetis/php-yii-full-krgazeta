<?
$params['function_name']=$info['table'];
$params['title']='Города';
$this->widget('application.components.backend.AdminDirectoryTop',array('params'=>$params)); 
?>







<!-- ******************************************************************************************* -->
<?php if ($info['command']=='select') echo $info['select_table'].$info['pagination']; ?>
<!-- ******************************************************************************************* -->













<!-- ******************************************************************************************* -->
<?php if (($info['command']=='edit')||($info['command']=='create')):?>
<form method="post" style="padding-top:10px;">

    <div class="form-group">
      <label>Страна</label>
      <select name="formdata[id_region]" class="form-control">
      <?=$info["id_region"]?>
      </select>
    </div>

    <div class="form-group">
      <label>Название города</label>
      <input type="text" 
             class="form-control" 
             maxlength="200" 
             name="formdata[name]" 
             value="<?=$info['name']?>">
    </div>

    <div class="form-group">
      <label>ЧПУ</label>
      <input type="text" 
             class="form-control" 
             maxlength="200" 
             name="formdata[sef]" 
             value="<?=$info['sef']?>">
    </div>
  
    <div class="form-group" style="padding-top:20px;">
    <input type="submit" 
           name="sbm" 
           value="Сохранить" 
           class="btn btn-primary" />
    </div>
</form>
<?endif;?>
<!-- ******************************************************************************************* -->
