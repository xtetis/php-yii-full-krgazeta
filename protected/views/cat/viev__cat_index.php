<?

// Title
//**************************************************************************************************
if (strlen($info['page_title'])){
   $this->pageTitle=$info['page_title'].' - объявления в Кривом Роге';
}elseif($info['id_parent']){
   $this->pageTitle=$info['categname'].' в Кривом Роге - '.
                    $info['parent_category_name'].' - объявления в Кривом Роге';
}elseif(!$info['id']){
   $this->pageTitle='Бесплатные объявления в Кривом Роге';
}else{
   $this->pageTitle=$info['categname'].' в Кривом Роге - объявления в Кривом Роге';
}
//**************************************************************************************************




// Description
//**************************************************************************************************
if (strlen($info['page_description'])){
   Yii::app()->clientScript->registerMetaTag($info['page_description'], 'Description');
}elseif($info['id_parent']){
   Yii::app()->clientScript->registerMetaTag('Все объявления &quot;'.$info['categname'].'&quot; - &quot;'.
   $info['parent_category_name'].
   '&quot; в Кривом Роге, здесь Вы сможете найти бесплатные объявления Кривого Рога', 'Description');
}elseif(!$info['id']){
   Yii::app()->clientScript->registerMetaTag('Все объявления Кривого Рога на сайте &quot;KrGazeta&quot;'.
                    ' - разместите бесплатные объявления на нашем сайте.', 'Description');
}else{
   Yii::app()->clientScript->registerMetaTag('Все объявления &quot;'.$info['categname'].
   '&quot; в Кривом Роге, здесь Вы сможете найти бесплатные объявления по теме '.
   $info['categname'], 'Description');
}
//**************************************************************************************************




if (isset($_GET['page']) || isset($_GET['filter'])){
  Yii::app()->clientScript->registerMetaTag('noindex,follow','robots');
}



?>






<div style="padding-top:10px;">
<?
$params['category']='main';
$this->widget('application.components.frontend.UserTopMenu',array('params'=>$params)); 
?>
<div>



<div style="padding-top:0px;">
  <?=$info['topcontent']?>
</div>

<div class="stxt">
<?
if (strlen($info['page_seotext'])){
   echo $info['page_seotext'];
   }elseif(!$info['id']){
   }else{
$uid =$info['id'].$info['categname'].$info['id_parent'].strlen($info['categname']);
$st = '
<p>
Мы рады {приветствовать|видеть} вас на страницах {самой|наиболее} {удобной|информативной} {онлайн|} {газеты|брошюры} {бесплатных|} объявлений Кривого Рога. Рекомендуем {Вам|} {обратить внимание|взглянуть} на категорию
'.$info['categname'].' '.($info['id_parent']?(' - '.$info['parent_category_name']):'').' в Кривом Роге.
</p>
<h2>'.$info['categname'].' в Кривом Роге</h2>
<p>{В этой|На этой} {категории|рубрике|странице} {приведен|отображен|показан} {список|перечень} {бесплатных|} объявлений "'.$info['categname'].' в Кривом Роге". {Для вашего удобства|Также|Еще} мы {сделали|создали} {разделение|разграничение} объявлений "'.$info['categname'].'" по району города. Для того чтобы {отфильтровать|отсортировать} по району - выберите соответствующий в выпадающем списке.
</p>';
if($info['id_parent']){
$st.=' Все {представленные|показанные} объявления {расположены|находятся} в родительской {категории|рубрике} "'.$info['parent_category_name'].' в Кривом Роге".';
}
echo fn__get_generated_text_by_uid($st,$uid);
   }
?>
</div>
