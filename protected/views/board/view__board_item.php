<?

$this->pageTitle=$info['name'].' Кривой Рог';

$description = mb_substr($info['about'],0,140,'utf-8').'...';
$description = str_replace("\n", "",$description);
$description = str_replace("\r", "",$description);
$description = str_replace('  ',' ',$description);
$description=fn__get_category_first_name($info['id_category']).' в Кривом Роге: '.$description;
Yii::app()->clientScript->registerMetaTag($description, 'Description');


?>

<script type="text/javascript" src="/themes/board/js/board_item.js"></script>  




<div style="padding:3px; padding-top:10px;">

  <div id="brd_item_breadcrumbs">
    <table>
      <tr>
        <td style="width:80px; padding-right:8px; border-right:1px #3465a4 solid;">
          <a href="<?=$_SERVER['HTTP_REFERER']?>">&#8656; <span>Назад</span></a>
        </td>
        <td style="padding-left:10px;">
          <a href="/cat">Объявления в Кривом Роге</a> → 
          <?=fn__get_rubric_selector($info['id_category'],false,true)?>
        </td>
      </tr>
    </table>
  </div>


  


  <div style=" padding-top:5px; padding-bottom:5px;">
    <table style="width:100%;">
      <tr>
        <td style="vertical-align:top;">
        
          <div id="obj_item_alert_container"></div>
        
          <h1 style="font-size: 23px; line-height: 40px; font-weight: bold; margin: 0px;">
            <?=$info['name'];?>
          </h1>
          <div style="font-size:13px;">
            <b style="border-right: 1px #d3d7cf solid; padding-right:20px;">
              <img src="/themes/board/img/ico/mapicon_32.png" style="width:16px; height:16px;"> 
              Кривой Рог
            </b> 
            <span style="padding-left:20px; color:#888a85;">
              Добавлено: <?=$info['createdon'];?>, номер объявления <?=$info['id'];?>
            </span>
          </div>
          
					<?if(fn__is_user_obj_author($info['id'])):?>
					<div class="alert alert-warning alert-dismissible" role="alert" style="margin-top:10px;">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<strong>Внимание!</strong> Вам, как автору этого объявления рекомендуем воспользоваться кнопками социальных сетей. Для того чтобы как можно больше людей узнало о вашем объявлении.
						
						<div style="height:40px; margin-top:5px; text-align:center;">
             <div class="shareblock"></div>
             <script src="/themes/board/components/share/share.js"></script> 
						</div>
					</div>
					<?endif;?>
          
          
          
          
          <?if(strlen(fn__get_album_mainimg_src($info['id_album']))){?>
           <div style="text-align: center; padding-top: 10px;">
             <img src="/<?=fn__get_album_mainimg_src($info['id_album'])?>" 
                  style="max-width:650px;">
           </div>
          <?}?>

        
          <? if (count($info['attributes'])){?><div style="padding: 5px;">
          <table style="width: 100%;" class="table_options"><tr><?}?>
          <?
          $i=1;
          foreach ($info['attributes'] as $key){
          ?>
          <td style="width:25%; padding:5px;">
            <div><?=$key[0]?>:</div>
            <div><b><?=$key[1]?></b></div>
          </td>
          <?
          if (($i%3)==0){echo "\n</tr>\n<tr>";}
          $i++;
          }?>
          <? if (count($info['attributes'])){?></tr></table></div><?}?>
          
          
          
          <div id="obj_content" style="padding-top:10px; font-size: 14px;">
            <?=$info['about'];?>
          </div>
          
          <div>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90, создано 31.08.10 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9808838827380357"
     data-ad-slot="5365470580"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
          </div>
          










          <div>
          <?if (fn__get_album_images_count($info['id_album'])){?>
             <?=fn__get_album_html($info['id_album'],htmlspecialchars($info['name'],ENT_QUOTES))?>
          <?}?>
          </div>


				<?if (count($info['latest_viewed_items'])>3){?>
<div class="panel panel-default" style="margin-top:10px;">
  <div class="panel-heading">Просмотренные вами объявления</div>
  <div class="panel-body">
		<div class="row">
			<?foreach ($info['latest_viewed_items'] as $item){?>
			<div class="col-md-3">
				<a href="/board/<?=$item['id']?>" class="thumbnail">
					<img src="<?
					$src=fn__get_album_mainimg_src($item['id_album']);
					if (strlen($src))
					{
						echo $src;
					}
					else
					{
						echo '/themes/board/img/content/no-photobig.jpg';
					}
					?>" 
					     style="width:130px; height:130px;">
				</a>
				<a href="/board/<?=$item['id']?>">
					<?=$item['name']?>
				</a>
			</div>
			<?}?>
		</div>
  </div>
</div>
				<?}?>




					<? if (count(fn__get_author_another_obj_id($info['id']))): ?>
					<div style="margin-top:10px">
						<div style="font-size:15px; font-weight:bold;">
							Другие объявления автора <?=fn__get_user_info($info['id_user'],'username');?>
						</div>
						<div>
						  <?=fn__obj_block(fn__get_author_another_obj_id($info['id']))?>
						</div>
					</div>
					<? endif; ?>
          
        </td>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <td style="width:250px; vertical-align:top;">
        
          <?if ($info['price']){?>
          <div class="priceblock">
            <?=$info['price'];?>
          </div>
          <?}?>
          
          <div style="font-size: 18px; text-align: center; padding-top: 10px; padding-bottom: 10px;">
            Связаться с автором
          </div>
          
          
<div class="panel panel-default">
  <div class="panel-body">
      <a class="btn btn-primary btn-lg btn-block" 
         data-toggle="modal" 
         data-target="#myModal2" 
         onclick="$('#message_alert').html(''); $('#message_alert').hide();">
         <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> 
         Написать автору
      </a>

      <a class="btn btn-primary btn-lg btn-block" 
         onclick="var phone = B64.decode('<?=$info['phone_encoded'];?>'); $('phone').html(phone);"
         title="Нажмите, чтобы показать номер телефона"
         >
         <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> 
         <phone><?=$info['phone_hidden'];?>
         <br><small style=" font-size:11px; border-bottom: 1px white dotted;">Показать телефон</small>
         </phone>
      </a>
      
  </div>
</div>
          
          
          
          
          
          
<div class="panel panel-default">
  <div class="panel-body">
            <table style="width:100%;">
              <tr>
                <td>
                  <img src="/themes/board/img/ico/map.png" style="max-width:40px">
                </td>
                <td style="padding:8px; font-size:14px; ">
                  Днепропетровская область, Кривой Рог, <?=fn__get_correct_district_name($info['id_district'])?>
                </td>
              </tr>
            </table>
  </div>
</div>



<div class="panel panel-default">
  <div class="panel-body">
    <ul class="nav nav-pills nav-stacked">
      <?if(fn__is_user_obj_author($info['id'])):?>
      <li><a href="/login?ret=board/edit/25870">Редактировать объявление</a></li>
      <?endif;?>
      <li>
        <a href="javascript:void(0);" 
           class="label label-danger btn_obj_abuse" 
           data-toggle="dropdown" 
           aria-haspopup="true" 
           aria-expanded="false">Пожаловаться</a>
        
        <ul class="dropdown-menu" style="width: 210px;">
          <li><a href="javascript:void(0);" idx="1" class="btn_obj_abuse_item">Товар продан</a></li>
          <li><a href="javascript:void(0);" idx="2" class="btn_obj_abuse_item">Неверная цена</a></li>
          <li><a href="javascript:void(0);" idx="3" class="btn_obj_abuse_item">Не дозвониться</a></li>
          <li><a href="javascript:void(0);" idx="4" class="btn_obj_abuse_item">Ссылки в описании</a></li>
          <li><a href="javascript:void(0);" idx="5" class="btn_obj_abuse_item">Другая причина</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>






        </td>
      </tr>
    </table>

  </div>

</div>


























<div class="modal fade" id="myModal2">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Написать автору объявления</h4>
      </div>
      <div class="modal-body">

        <div class="alert alert-info" role="alert" id="message_alert" 
             style="font-size:13px; display:none; padding:5px; margin-bottom: 5px;">
          
        </div>
        <form method="POST" id="frm__send_message_to_user">
          <input name="frm__send_message_to_user" type="hidden" value="1">
          <input name="id" type="hidden" value="<?=$info['id'];?>">
          <div style="<?=(fn__get_user_id()?'display:none;':'')?>">
            <div style="padding: 2px; font-size: 15px; color: rgba(108, 108, 108, 1);">Имя</div>
            <div><input name="name" type="text" id="inp__obj_send_name" 
                        class="form-control" 
                        style="margin-bottom: 2px;"></div>
          </div>
          
          <div style="<?=(fn__get_user_id()?'display:none;':'')?>">
            <div style="padding: 2px; font-size: 15px; color: rgba(108, 108, 108, 1);">Email</div>
            <div><input name="email" type="text" id="inp__obj_send_email" 
                        class="form-control" 
                        style="margin-bottom: 2px;"></div>
          </div>
          
          <div style="padding: 2px; font-size: 15px; color: rgba(108, 108, 108, 1);">Сообщение</div>
          <div><textarea id="inp__obj_send_message" name="message" 
                         class="form-control" 
                         style="resize: none; height:130px; margin-bottom: 2px;"></textarea></div>
        </form>


      </div>
      <div class="modal-footer">
        <a class="btn btn-primary" href="javascript:void(0)" style="color:#fff;" 
           id="board_item_send_message">
         Отправить</a>        
        <a class="btn btn-default" data-dismiss="modal" href="javascript:void(0)">Закрыть</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




























<div class="modal fade" id="modal_obj_abuse">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Пожаловаться на объявление</h4>
      </div>
      <div class="modal-body">

        <div class="alert alert-danger" role="alert" id="board_item_abuse_alert" 
             style="font-size:13px; display:none; padding:5px; margin-bottom: 5px;">
          
        </div>
        <form id="frm__send_abuse">
          <input id="inp__obj_abuse_id"
                 name="id"  
                 type="hidden" value="<?=$info['id'];?>">
          <input name="action_obj_abuse"
                 type="hidden" value="1">
                 
          <div style="<?=(fn__get_user_id()?'display:none;':'')?>">
            <div style="padding: 2px; font-size: 15px; color: rgba(108, 108, 108, 1);">Имя</div>
            <div><input type="text" 
                        id="inp__obj_abuse_name"
                        name="name"  
                        class="form-control" 
                        style="margin-bottom: 2px;"></div>
          </div>
          
          <div style="<?=(fn__get_user_id()?'display:none;':'')?>">
            <div style="padding: 2px; font-size: 15px; color: rgba(108, 108, 108, 1);">Email</div>
            <div><input type="email" 
                        id="inp__obj_abuse_email"
                        name="email"  
                        class="form-control" 
                        style="margin-bottom: 2px;"></div>
          </div>
          
          <div style="padding: 2px; font-size: 15px; color: rgba(108, 108, 108, 1);">Текст жалобы</div>
          <div><textarea id="inp__obj_abuse_message"
                         name="message"
                         class="form-control" 
                         style="resize: none; height:130px; margin-bottom: 2px;" required></textarea></div>
        </form>


      </div>
      <div class="modal-footer">
        <a class="btn btn-primary" href="javascript:void(0)" style="color:#fff;" 
           id="board_item_send_abuse">
         Отправить жалобу</a>        
        <a class="btn btn-default" data-dismiss="modal" href="javascript:void(0)">Закрыть</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<?if ($info['price']):?>
<div itemtype="http://schema.org/Product" itemscope>
  <meta itemprop="name" content="<?php echo htmlspecialchars(strip_tags($info['name'])); ?>">
  <meta itemprop="description" content="<?php echo htmlspecialchars(strip_tags(mb_substr($info['about'],0,140,'utf-8'))); ?>">
  <meta itemprop="sku" content="<?php echo $info['id']; ?>">
  <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
    <meta itemprop="price" content="<?php echo preg_replace("/\D/","",$info['price']); ?>" />
    <meta itemprop="priceCurrency" content="UAH" />
    <link itemprop="availability" href="http://schema.org/InStock" />
  </div>
</div>
<?endif;?>


