<?
$cs = Yii::app()->clientScript;
//$cs->registerScriptFile('/themes/board/js/board_add_fltr.js');




$this->pageTitle='Разместить бесплатное объявление на сайте KRGazeta Кривой Рог - разместить объявление в Кривом Роге без регистрации';
Yii::app()->clientScript->registerMetaTag('Разместить бесплатное объявление на сайте KRGazeta – разместите объявление без регистрации и его увидят жители Кривого Рога.', 'Description');


?>


<script type="text/javascript" src="/themes/board/js/board_add.js"></script>  

<form method="POST" enctype="multipart/form-data">
<div style="padding:5px;">
<table style="width:100%; margin-top:20px;">
  <tr>
    <td colspan="3" style="padding-bottom:5px;">
      <div  style="border-bottom:1px #d3d7cf solid; height:23px; padding:2px;">
        <h1 style="font-size:20px; line-height:17px;">
          Подать бесплатное объявление на KRGazeta
        </h1>
      </div>
    </td>
  </tr>
  










<!--                          Заголовок объявления                           -->
<!-- *********************************************************************** -->
  <tr>
    <td class="b_add_legend">
      Заголовок<span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input">
      <div class="form-group">
      <div class="b_add_legend_bottom popover_hover" 
           data-title="Напишите заголовок" 
           data-content="Введите подробный и понятный заголовок. Избегайте слов с ЗАГЛАВНЫМИ БУКВАМИ. 
                         Чем больше тематических слов будет в заголовке, тем вероятнее смогут 
                         найти объявление те, кто в нем заинтересован." 
           >
        <input class="form-control" 
               name="formdata[name]" 
               type="text" 
               value="<?=$info['name']?>" 
               maxlength="70" 
               required>
             
        <div class="form_element_<?=((strlen($info['errform']['name']))?'error_message':'message')?>">
           <?=((strlen($info['errform']['name']))?$info['errform']['name']:
                                                  'Максимальное количество символов - 70')?>
        </div>
      </div>
      </div>
    </td>
  </tr>
<!-- *********************************************************************** -->
  
  
  
  
  

















<!-- *********************************************************************** -->
<!-- Modal -->
<div class="modal fade " id="id_category_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Выберите категорию</h4>
      </div>
      <div class="modal-body" style="min-height: 320px;">
        <p>
          <?=fn__get_select_categories()?>
        </p>
      </div>
    </div>
  </div>
</div>



  
  
  
  
  <tr>
    <td class="b_add_legend" style="padding-top:5px;">
      Рубрика<span title="Обязательное поле">*</span>
    </td>
    <td style="padding-top:5px;">
      <input type="hidden" 
             name="formdata[id_category]" 
             id="form_id_category" 
             value="<?=$info['id_category']?>">
             
      <div class="b_add_legend_bottom popover_hover" 
           data-title="Выберите рубрику" 
           data-content="Для добавления объявления - обязательнот выберите соответствующую рубрику. 
                         Это поможет покапателям/клиентам скорее с Вами связаться.">
        <div id="div_rubric">
         <?=((isset($info['id_category'])&&($info['id_category']!=0))?fn__get_rubric_selector($info['id_category']).'
         <script>
         $(function() {
   $(\'#other_options\').html(B64.decode(\''.base64_encode(fn__get_post_obj_options($info['id_category'])).'\'));  
          });
         </script>
         ':'
         ')?>

        </div>
        <div style="text-align:center;">
          <a id="btn__id_category_modal__show" class="btn btn-primary" 
             onclick="$('#id_category_modal').modal('show');">
             Выбрать категорию
          </a>
        </div>
      
        <div class="<?=((strlen($info['errform']['id_category']))?'form_element_error_message':
                                                            'form_element_message')?>"
             style="text-align:center;">
           <?=((strlen($info['errform']['id_category']))?$info['errform']['id_category']:'')?>
        </div>
      
      </div>
    </td>
    <td></td>
  </tr>
<!-- *********************************************************************** -->
















<tr>
  <td colspan="3" id="other_options">
  
  </td>
</tr>




















  <tr>
    <td class="b_add_legend" style="padding-top:5px;">
      Район<span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input" style="padding-top:5px;">
      <div class="b_add_legend_bottom popover_hover" 
           data-title="Выберите район" 
           data-content="Выберите район города, который соответствует вашему объявлению 
                         (в котором продается товар или предоставляется услуга).">
      
        <select class="form-control" 
                name="formdata[id_district]" 
                style="cursor: pointer !important;">
          <?=fn__get_select_districrs($info['id_district']);?>
        </select>

        <div class="form_element_<?=((strlen($info['errform']['id_district']))?'error_message':'message')?>">
          <?=((strlen($info['errform']['id_district']))?$info['errform']['id_district']:'')?>
        </div>
      
      </div>
    </td>
    <td>

    </td>
  </tr>
  






















  <tr>
    <td colspan="3" style="padding-bottom:5px;">
      <div  style="border-bottom:1px #d3d7cf solid; height:18px;">
      </div>
    </td>
  </tr>




















  <tr>
    <td class="b_add_legend" style="padding-top:5px;">
      Описание<span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input" style="padding-top:5px;">
      <div class="b_add_legend_bottom popover_hover" 
           data-title="Опишите подробно товар или услугу" 
           data-content="<ul class='b_add_tooltip_list'>
  <li>Сделайте ваше описание побуждающим к действию. Укажите преимущества вашего товара или услуги.</li>
  <li>Предоставьте как можно больше подробностей - так вы избежите лишних вопросов и вызовете больше доверия.</li>
  <li>Старайтесь писать грамотно и корректно. Избегайте неприличных слов.</li>
  <li>Избегайте слов с ЗАГЛАВНЫМИ БУКВАМИ</li>
  </ul>
  <ul class='suggestDefault hidden'>
  <li>Сделайте ваше описание побуждающим к действию. Укажите преимущества вашего товара или услуги.</li>
  <li>Предоставьте как можно больше подробностей - так вы избежите лишних вопросов и вызовете больше доверия.</li>
  <li>Старайтесь писать грамотно и корректно. Избегайте неприличных слов.</li>
  <li>Избегайте слов с ЗАГЛАВНЫМИ БУКВАМИ</li>
</ul>" id="b_add_form_name__popover_hover">
      <textarea class="form-control" 
                name="formdata[about]" 
                maxlength="6000"
                required><?=$info['about']?></textarea>


       


      <div class="form_element_<?=((strlen($info['errform']['about']))?'error_message':'message')?>">
         <?=((strlen($info['errform']['about']))?$info['errform']['about']:
                                                 'Максимальное количество символов - 6000')?>
      </div>
        
      
      </div>
    </td>
    <td>
    </td>
  </tr>




















  <tr>
    <td class="b_add_legend" style="padding-top:5px;">
      Цена<span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input" style="padding-top:5px;">
      <div class="b_add_legend_bottom popover_hover" 
           data-title="Укажите цену" 
           data-content="Установите корректную цену на ваш товар или услугу. <br> 
                         Оставьте поле пустым, если цена договорная.">
         
        <input class="form-control" 
               name="formdata[price]" 
               style="display: inline; width:200px !important;"
               id="b_add_form_price" 
               type="text" 
               value="<?=$info['price']?>" 
               maxlength="11" 
               style="width:150px !important;">

        <select style="width:100px !important; display: inline;" 
                class="form-control" 
                name="formdata[id_valuta]">
        <?=fn__get_select_by_sql_i_tpl('
            SELECT `id`,`shortname` FROM `xta_valuta`','
            <option value="+id+" style="padding: 4px; padding-left: 7px;" +default+>+shortname+</option>
            ',((isset($info['id_valuta']))?$info['id_valuta']:0) )?>
        </select>
        
        <div class="form_element_<?
             if ((strlen($info['errform']['price']))||(strlen($info['errform']['id_valuta']))){
                 echo 'error_message';
                 }else{
                 echo 'message';
                 }
                 ?>">
           <?if ((strlen($info['errform']['price']))||(strlen($info['errform']['id_valuta']))){
                echo '<div>'.$info['errform']['id_valuta'].'</div>';
                echo '<div>'.$info['errform']['price'].'</div>';
                }else{
                echo 'Вы можете оставить поле пустым или поставить 0 - это будет обозначать "Цена договорная"';
                }
           ?>
        </div>
      </div>
    </td>
    <td style="vertical-align:top;" style="padding-top:5px;">

    </td>
  </tr>





















  <tr>
    <td class="b_add_legend" style="padding-top:20px;">
      Изображения
    </td>
    <td>
    
<script src="/themes/board/components/html5-uploader/javascript.js"></script>
<link rel="stylesheet" type="text/css" href="/themes/board/components/html5-uploader/style.css" />
    

	<br>
	<!-- Область для перетаскивания -->
	<div id="drop-files" 
	     ondragover="return false" 
	     class="popover_hover" 
	     style="height:120px;"
	     data-title="Добавьте фотографии" 
	     data-content="Рекомендуем добавить фотографии чтобы потенциальным покупателям/клиентам было проще оценить преимущества вашего предложения">
		<p>Перетащите сюда или выберите изображение</p>
        	<input type="file" name="images" id="uploadbtn" style="text-align: center; font-size: 16px; margin: 0px auto;" multiple />
	</div>
    <!-- Область предпросмотра -->
	<div id="uploaded-holder"> 
		<div id="dropped-files">
        	<!-- Кнопки загрузить и удалить, а также количество файлов -->
        	<div id="upload-button">
            	<center>
                	<span>0 Файлов</span>
                	<div id="loading">
						<div id="loading-bar">
							<div class="loading-color"></div>
						</div>
						<div id="loading-content"></div>
					</div>
                </center>
			</div>  
        </div>
	</div>
	<!-- Список загруженных файлов -->
	<div id="file-name-holder">
		<ul id="uploaded-files">
			<h1>Загруженные файлы</h1>
		</ul>
	</div>
    
    
    

    </td>
    <td style="vertical-align:top;" style="padding-top:5px;">



<? if (strlen($info['script'])){?>
<script>
$(document).ready(function() {





	// Процедура добавления эскизов на страницу
	function addImage1(ind) {
		// Если индекс отрицательный значит выводим весь массив изображений
		if (ind < 0 ) { 
		start = 0; end = dataArray.length; 
		} else {
		// иначе только определенное изображение 
		start = ind; end = ind+1; } 
		// Оповещения о загруженных файлах
		if(dataArray.length == 0) {
			// Если пустой массив скрываем кнопки и всю область
			$('#upload-button').hide();
			$('#uploaded-holder').hide();
		} else if (dataArray.length == 1) {
			$('#upload-button span').html("Был выбран 1 файл");
		} else {
			$('#upload-button span').html(dataArray.length+" файлов были выбраны");
		}
		// Цикл для каждого элемента массива
		for (i = start; i < end; i++) {
			// размещаем загруженные изображения
			if($('#dropped-files > .image').length <= 7) { 
				$('#dropped-files').append('<div id="img-'+i+'" class="image ins_img_'+i+'" style="background: url('+dataArray[i].value+'); background-size: cover;"> <a href="javascript:void(0)" id="drop-'+i+'" class="drop-button">Удалить изображение</a><input type="hidden" name="img'+i+'" value="'+dataArray[i].value+'" style="display:none"></div>'); 
				
			}
		}
		return false;
	}










  $("#uploaded-holder").show();
 	$('#upload-button').show();
  var dataArray = [];
  <?=$info['script'];?>
  addImage1(-1)

});
</script>
<?}?>

    </td>
  </tr>












































  <tr>
    <td colspan="3" style="padding-bottom:5px; padding-top:20px;">
      <div style="border-bottom:1px #d3d7cf solid; height:18px; font-size:17px; line-height:17px; font-weight:bold;">
       Ваши контактные данные
      </div>
    </td>
  </tr>















  <tr>
    <td class="b_add_legend" style="padding-bottom:5px;">
      Номер телефона<span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input" style="padding-bottom:5px;">
      <div class="b_add_legend_bottom popover_hover" 
           data-title="Номер телефона" 
           data-content="Укажите контактный номер телефона" 
           style="width:315px;">
        <input class="form-control" 
               name="formdata[phone]" 
               type="text" 
               value="<?=$info['phone']?>" 
               maxlength="11" 
               style="width:300px !important;"
               required>

        <div class="form_element_<?=((strlen($info['errform']['phone']))?'error_message':'message')?>">
           <?=((strlen($info['errform']['phone']))?$info['errform']['phone']:
                                               'Только цифры (формат 0971234567)')?>
        </div>
      </div>
    </td>
    <td></td>
  </tr>



















  <tr>
    <td class="b_add_legend">
      Контактное лицо<span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input">
      <div class="b_add_legend_bottom popover_hover" 
           data-title="Контактное лицо" 
           data-content="Как к Вам обращаться?" 
           style="width:315px;">
        <input class="form-control" 
               name="formdata[username]" 
               type="text" 
               value="<?=$info['username']?>" 
               maxlength="40" 
               style="width:300px !important;"
               required>

        <div class="form_element_<?=((strlen($info['errform']['username']))?'error_message':'message')?>">
           <?=((strlen($info['errform']['username']))?$info['errform']['username']:
                                               'Максимальное количество символов - 40')?>
        </div>
      </div>
    </td>
    <td></td>
  </tr>























<?if(!fn__get_user_id()){?>
  <tr>
    <td class="b_add_legend" style="padding-top:5px;">
      Email-адрес<span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input" style="padding-top:5px;">
      <div class="b_add_legend_bottom popover_hover" 
           data-title="Email-адрес" 
           data-content="Укажите реальный email для получения уведомлений и управления объявлением" 
           style="width:315px;">
        <input class="form-control" 
               name="formdata[email]" 
               type="email"
               value="<?=$info['email']?>" 
               maxlength="30" 
               style="width:300px !important;" 
               <?=((fn__get_user_id())?'readonly="readonly"':'')?>
               required>

        <div class="form_element_<?=((strlen($info['errform']['email']))?'error_message':'message')?>">
           <?=((strlen($info['errform']['email']))?$info['errform']['email']:
                                               'Максимальное количество символов - 30')?>
        </div>
      </div>
    </td>
    <td></td>
  </tr>
<?}?>







  
  
  




















  <tr>
    <td class="b_add_legend" style="padding-top:5px;">
      Skype 
    </td>
    <td class="td_b_add_input" style="padding-top:5px;">
      <div class="b_add_legend_bottom popover_hover" 
           data-title="Skype" 
           data-content="Укажите skype, чтобы с Вами могли связаться" 
           style="width:315px;">
        <input class="form-control" 
               name="formdata[skype]" 
               type="text" 
               value="<?=$info['skype']?>" 
               maxlength="20" 
               style="width:300px !important;">

        <div class="form_element_<?=((strlen($info['errform']['skype']))?'error_message':'message')?>">
           <?=((strlen($info['errform']['skype']))?$info['errform']['skype']:
                                               'Максимальное количество символов - 20')?>
        </div>
      </div>
    </td>
    <td></td>
  </tr>








  <tr>
    <td></td>
    <td style="padding-top:20px;">
    <input type="submit" class="btn btn-primary btn-large" name="sbm" value="Опубликовать"> 
    </td>
    <td></td>
  </tr>
</table>
</div>
<br>
</form>
