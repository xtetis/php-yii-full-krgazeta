<?
$this->pageTitle='Авторизация - KrGazeta';
Yii::app()->clientScript->registerMetaTag('Авторизируйтесь на сайте - это позволит Вам управлять созданными объявлениями и настройками учетной записи.', 'Description');
?>










<div style="padding-top:100px;">
  <table style="border:0px; width:100%;">
    <tr>
      <td></td>
      <td style="width:300px;">
        <div style="width: 300px;">
          <?=(isset($info['error']))?'<div class="badge badge-warning" 
                                           style="margin:2px; background-color: #BA0000;">
                                      '.$info['error'].'</div>':'';?>
          <form class="well form-vertical" id="verticalForm" method="post" style="background-color: #E9ECEF; border: 1px solid #D4D4D4;">
          
            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" class="form-control" 
                     id="email" name="email" placeholder="Введите email" value="<?=$info['email']?>"
                     required>
            </div>
                    
            <div class="form-group">
              <label for="pass">Пароль</label>
              <input type="password" class="form-control" id="pass" name="pass" placeholder="Введите пароль"
                     required>
            </div>
  
	          <input name="ret" type="hidden" value="<?=$info['ret']?>">	
	
	          <div style="text-align:center;">
	            <button  class="btn btn-primary" type="submit">Войти</button>
	          </div>  
	          
	          <div style="padding-top:10px;">
	          <a href="/login/forgotpass">Забыли пароль?</a>
	          <a style="float:right;" href="/login/register">Регистрация</a>
	          </div>
          </form>
        </div>
      </td>
      <td></td>
    </tr>
  </table>
</div>
