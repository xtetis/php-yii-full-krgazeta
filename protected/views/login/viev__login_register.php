<?
$this->pageTitle='Регистрация пользователя - KrGazeta';
Yii::app()->clientScript->registerMetaTag('Зарегистрируйтесь на KrGazeta и размещайте бесплатно ваши объявления.', 'Description');
?>



<form method="POST" enctype="multipart/form-data">
<div style="padding:5px;">
<table style="width:100%; margin-top:20px;">
  <tr>
    <td colspan="3" style="padding-bottom:15px;">
      <div  style="border-bottom:1px #d3d7cf solid; height:21px;">
        <h1 style="font-size:20px; line-height:17px;">
          Регистрация нового пользователя
        </h1>
      </div>
    </td>
  </tr>






<!--                          Email                                          
   - *********************************************************************** -->
  <tr>
    <td class="b_add_legend">
      Email <span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input">
      <div class="form-group" style="">
        <input type="email" 
               class="form-control" 
               name="formdata[email]" 
               value="<?=$info['email']?>" 
               data-toggle="popover" 
               title="Email" 
               data-content="Укажите email. При регистрации Вы получите на этот email письмо с 
                             данными о регистрации."
               required>
        <div class="<?=((strlen($info['errform']['email']))?'form_element_error_message':
                                                            'form_element_message')?>">
           <?=((strlen($info['errform']['email']))?$info['errform']['email']:
                                                   'Минимальное количество символов - 7')?>
        </div>
      </div>
    </td>
  </tr>
<!-- *********************************************************************** -->
  
  


<!--                          Контактное лицо                                          
   - *********************************************************************** -->
  <tr>
    <td class="b_add_legend">
      Контактное лицо <span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input">
      <div class="form-group" style="">
        <input type="text" 
               class="form-control" 
               name="formdata[username]" 
               value="<?=$info['username']?>" 
               data-toggle="popover" 
               title="Контактное лицо" 
               data-content="Укажите имя, которое будет указано в ваших объявлениях. "
               required>
        <div class="<?=((strlen($info['errform']['username']))?'form_element_error_message':
                                                               'form_element_message')?>">
          <?=((strlen($info['errform']['username']))?$info['errform']['username']:
                                                     'Минимальное количество символов - 4')?>
        </div>
      </div>
    </td>
  </tr>
<!-- *********************************************************************** -->
  
  


<!--                          Телефон                                          
   - *********************************************************************** -->
  <tr>
    <td class="b_add_legend">
      Телефон <span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input">
      <div class="form-group" style="">
        <input type="phone" 
               class="form-control" 
               name="formdata[phone]" 
               value="<?=$info['phone']?>" 
               data-toggle="popover" 
               title="Телефон" 
               data-content="Укажите телефон, который будет использоваться при добавлении объявлений. "
               required>
        <div class="<?=((strlen($info['errform']['phone']))?'form_element_error_message':
                                                            'form_element_message')?>">
          <?=((strlen($info['errform']['phone']))?$info['errform']['phone']:
                                                  'Минимальное количество символов - 6')?>
        </div>
      </div>
    </td>
  </tr>
<!-- *********************************************************************** -->



<!--                          Пароль                                          
   - *********************************************************************** -->
  <tr>
    <td class="b_add_legend">
      Пароль <span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input">
      <div class="form-group" style="">
        <input type="password" 
               class="form-control" 
               name="formdata[pass]" 
               value="<?=$info['pass']?>" 
               data-toggle="popover" 
               title="Пароль" 
               data-content="Укажите пароль не меньше 6 символов."
               required>
        <div class="<?=((strlen($info['errform']['pass']))?'form_element_error_message':
                                                           'form_element_message')?>">
          <?=((strlen($info['errform']['pass']))?$info['errform']['pass']:'Длина пароля не меньше 6 символов')?>
        </div>
      </div>
    </td>
  </tr>
<!-- *********************************************************************** -->






<!--                          Подтверждение пароля                                          
   - *********************************************************************** -->
  <tr>
    <td class="b_add_legend">
      Подтверждение пароля <span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input">
      <div class="form-group" style="">
        <input type="password" 
               class="form-control" 
               name="formdata[passdouble]" 
               value="<?=$info['passdouble']?>"
               data-toggle="popover" 
               title="Подтверждение пароля" 
               data-content="Для регистрации повторите введите пароль"
               required>
        <div class="<?=((strlen($info['errform']['passdouble']))?'form_element_error_message':
                                                                 'form_element_message')?>">
          <?=((strlen($info['errform']['passdouble']))?$info['errform']['passdouble']:
                                                       'Введите пароль повторно')?>
        </div>
      </div>
    </td>
  </tr>
<!-- *********************************************************************** -->





<script>
$(function() {
  // Handler for .ready() called.
  $('[data-toggle="popover"]').popover({trigger : 'hover focus'});
});
</script>

















  
  
  
  <tr>
    <td></td>
    <td style="padding-top:20px;">
    <input type="submit" class="btn btn-primary btn-large" name="sbm" value="Регистрация"> 
    </td>
    <td></td>
  </tr>
</table>
</div>
<br>
<br>
<br>
<br>
</form>



