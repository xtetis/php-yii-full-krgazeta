<?
$this->pageTitle='Восстановление пароля - KrGazeta';
Yii::app()->clientScript->registerMetaTag('Если вы забыли пароль от Вашей учетной записи - рекомендуем восстановить пароль.', 'Description');
?>








<div style="padding-top:100px;">
  <table style="border:0px; width:100%;">
    <tr>
      <td></td>
      <td style="width:300px;">
        <div style="width: 300px;">
          
          <h1 style="text-align: center;">
            Восстановление пароля
          </h1>
          
          <div class="alert alert-info" role="alert" id="message_alert" 
               style="font-size:13px; display:none; padding:5px; margin-bottom: 5px;">
            
          </div>
          
          <?=(isset($info['error']))?'<div class="badge badge-warning" style="margin:2px;">'.$info['error'].'</div>':'';?>
          <form class="well form-vertical" id="verticalForm" method="post">
	          <label for="TestForm_textField">Email</label>
	          <input id="inp__forgotpass_email" class="span3" name="email" type="text" value="<?=(isset($_POST['email']))?$_POST['email']:'';?>">
	          
	          <input name="ret" type="hidden" value="<?=(isset($_GET['ret']))?$_GET['ret']:'/';?>">	
	
	          <div style="text-align:center;">
	            <a id="btn_forgotpass" class="btn" name="yt0">Восстановить</a>
	          </div>  
	          
	          <div style="padding-top:10px;">
	          <a href="/login">Авторизация</a>
	          <a style="float:right;" href="/login/register">Регистрация</a>
	          </div>
          </form>
        </div>
      </td>
      <td></td>
    </tr>
  </table>
</div>
