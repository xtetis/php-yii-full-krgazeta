<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <!-- ********** SEO ********** -->   
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <!-- ********** /SEO ********** -->   
  <base href="http://<?=$_SERVER['HTTP_HOST']?>/">
  <?=fn__get_setting('yandex_verification_meta')?>

  
  <link href="/themes/board/img/favicon/favicon_32_32.png" rel="shortcut icon" type="image/x-icon" />
  
  <?//php Yii::app()->bootstrap->register(); ?>	
  
  <script type="text/javascript" src="/themes/board/components/jquery/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="/themes/board/components/bootstrap/js/bootstrap.min.js"></script>  
  <link rel="stylesheet" href="/themes/board/components/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="_/themes/board/components/bootstrap/css/bootstrap-theme.min.css">


  <link rel="stylesheet" href="/themes/board/css/common.css">
  <link rel="stylesheet" href="/themes/board/css/struct.css">
  <link rel="stylesheet" href="/themes/board/css/content.css">
  <script type="text/javascript" src="/themes/board/js/popover_bootstrap.js"></script>
  
  <script type="text/javascript" src="__/themes/board/js/xtetis.js"></script>
  
  


  
<!--**************************************** Постраничная навигация **************************** -->
    <link rel="stylesheet" type="text/css" href="/themes/xtetis/components/paginator3000/paginator3000.css" />
    <script type="text/javascript" src="/themes/xtetis/components/paginator3000/paginator3000.js"></script>
<!--*************************************** /Постраничная навигация **************************** -->



  <!-- ************************* Библиотека для работы с шафрованием *************************** -->
    <script type="text/javascript" src="/themes/board/js/myencode.js"></script>
    <script type="text/javascript" src="/themes/board/js/base64v1_0.js"></script>
  <!-- ************************* /Библиотека для работы с шафрованием ************************** -->
  
  
  



</head>
<body>
  <script src="/themes/board/js/bootstrap_hover_dropdown.js"></script>
  <table class="maxtable struct_maintable">
    <tr>
      <td colspan="3" style="height:5px;"></td>
    </tr>    
    <tr>
      <td>&nbsp;</td>
      <td style="width:1000px;">
        <table class="maxtable struct_maintable_1">
          <tr>
            <td class="struct_maintable_1_header">
              <table class="maxtable">
                <tr>
                  <td class="td_logo">
                    <a href="/cat">
                      <div id="logodiv">
                        <img src="/themes/board/img/struct/logo/krgazeta.png" alt="KRGazeta">
                      </div>
                      <div id="logodivbottom">
                         <?if(fn__get_site_razdel()=='love'){?>
                           <span style="background-color:#a40000;">Знакомства в Кривом Роге</span>
                         <?}else{?>
                           <span>Объявления Кривого Рога</span>
                         <?}?>
                      </div>
                    </a>
                  </td>
                  <td style="padding-left:30px;">
                    <table class="maxtable">
                      <tr>
                        <td>
                        <?=fn__get_user_menu();?>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                      </tr>
                    </table>
                    



        
        
        
    
        
        
    
                       
                  </td>
                  <td style="width:270px;">
                   <a class="btn btn-primary btn-lg" style="font-weight:bold; color:#fff;" href="/board/add">+ Добавить объявление</a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="vertical-align:top;">




      <link href="/themes/board/components/perfect_scrollbar/perfect-scrollbar.css" rel="stylesheet">
      <script src="/themes/board/components/perfect_scrollbar/jquery.mousewheel.js"></script>
      <script src="/themes/board/components/perfect_scrollbar/perfect-scrollbar.js"></script>
      <style>
        .contentHolder { position:relative; margin:0px auto; padding:0px; width: 315px; height: 260px; overflow: hidden; }
        .contentHolder .content { background-image: url('./azusa.jpg'); width: 1280px; height: 720px; }
        .spacer { text-align:center }
      </style>
    <script>
      jQuery(document).ready(function ($) {
        "use strict";
        $('.contentHolder').perfectScrollbar();
      });
    </script>




   <?php echo $content; ?>
   
   

































   
   
   

            </td>
          </tr>
        </table>
      </td>
      <td>&nbsp;</td>
    </tr>    
    <tr>
      <td colspan="3" style="height:5px;"></td>
    </tr>    
  </table>
  <?=fn__get_setting('yandex_metrica_counter')?>
</body>
</html>


<!--
Для сайта <?=$_SERVER['HTTP_HOST']?>
 Время генерации страницы <?=sprintf('%0.5f',Yii::getLogger()->getExecutionTime())?> с. 
Использовано памяти: <?=round(memory_get_peak_usage()/(1024*1024),2)."MB"?>

-->
