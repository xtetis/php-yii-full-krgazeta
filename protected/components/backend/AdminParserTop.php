<?

	class AdminParserTop extends CWidget {
    public $params = array(
        'title' => 'Парсер аукциона доменов NIC.RU',
        'buttons' => array(
'<a class="btn btn-default" href="/admin/parser/nic_ru"><span class="glyphicon glyphicon-home"></span>Начало</a>',
'<a class="btn btn-default" href="/admin/parser/nic_ru?do=parse"><span class="glyphicon glyphicon-repeat"></span>Парсить</a>',
'<a class="btn btn-default" href="/admin/parser/nic_ru?do=clear"><span class="glyphicon glyphicon-trash"></span>Стереть данные парсинга</a>',
'<a class="btn btn-default" data-toggle="modal" data-target="#settings_modal"><span class="glyphicon glyphicon-wrench"></span>Настройки</a>',
                        ),
    );
    
		public function run() {
			$this->render('view_AdminParserTop', array('params' => $this->params));
		}
	}

?>

