<?

	class AdminDirectoryTop extends CWidget {
    public $params = array(
        'function_name'=>'',
        'show_new'=>true,
        'show_all'=>true,
        'show_view'=>false,
        'show_link'=>'/',
        'title' => 'Статьи',
    );
    
		public function run() {
		  if (!isset($this->params['show_new']))$this->params['show_new']=true;
		  if (!isset($this->params['show_all']))$this->params['show_all']=true;
			$this->render('view_AdminDirectoryTop', array('params' => $this->params));
		}
	}

?>

