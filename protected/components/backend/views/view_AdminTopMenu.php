    
    
    
<nav class="navbar navbar-default navbar-collapse" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/" target="frontend_window">
      <span class="glyphicon glyphicon-home"></span>
      <?=ucfirst($_SERVER['HTTP_HOST'])?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-globe"></span>
            Сайты 
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
						<?=$siteslist?>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-info-sign"></span>
            Справочники <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li class="dropdown-submenu">
              <a class="dropdown-toggle" data-toggle="dropdown">
                Административные единицы
              </a>
              <ul class="dropdown-menu" role="menu">
						    <li><a href="/admin/xta_country">Страны</a></li>
						    <li><a href="/admin/xta_region">Области</a></li>
						    <li><a href="/admin/xta_city">Города</a></li>
						    <li><a href="/admin/xta_district">Районы</a></li>
              </ul>
            </li>
            <li class="dropdown-submenu">
              <a class="dropdown-toggle" data-toggle="dropdown">
                Объявления
              </a>
              <ul class="dropdown-menu" role="menu">
						    <li><a href="/admin/xta_obj_category">Категории объявлений</a></li>
						    <li><a href="/admin/xta_obj_option">Опции объявлений</a></li>
              </ul>
            </li>
            
            <li class="divider"></li>
            <li><a href="/admin/xta_site">Сайты</a></li>
            <li class="divider"></li>
            <li><a href="/admin/xta_setting_category">Категории настроек</a></li>
            <li><a href="/admin/xta_help">Документация</a></li>
          </ul>
        </li>













        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-align-justify"></span>
            Содержимое <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/admin/xta_page">Страницы</a></li>
            <li><a href="/admin/xta_obj">Объявления</a></li>
          </ul>
        </li>













        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-user"></span>
            Пользователи <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/admin/xta_user">Список пользователей</a></li>
            <li><a href="/admin/xta_user_role">Роли пользователей</a></li>
            <li class="disabled"><a href="#">Права доступа</a></li>
          </ul>
        </li>








        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-circle-arrow-down"></span>
            Модули <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/admin/mod_sitemap">Карта сайта</a></li>
            <li class="dropdown-submenu">
              <a class="dropdown-toggle" data-toggle="dropdown">
                Парсеры
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="/admin/parser/odnagazeta_com">odnagazeta.com</a></li>
              </ul>
            </li>
          </ul>
        </li>
        
        
      </ul>











      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-cog"></span>
          Настройки <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/admin/xta_setting">Настройки сайта</a></li>
            <li class="disabled"><a href="#">Виджеты</a></li>
            <li><a href="/admin/clear/watermarked">Очистить Watermarked</a></li>
            <li><a href="/admin/clear/thumbs">Очистить Thumbs</a></li>
          </ul>
        </li>
        
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-hdd"></span>
          Система <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/admin/sysinfo">Информация о системе</a></li>
          </ul>
        </li>
        <li><a href="/login/logout?to=admin" onclick="return confirm('Вы уверены, что хотите завершить сессию?');">
        <span class="glyphicon glyphicon-log-out"></span>
        Выход</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    
    
    
    
    

    
    
    
    
