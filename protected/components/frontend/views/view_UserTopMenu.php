
<div class="togglable-tabs" id="yw42">
  <ul id="yw43" class="nav nav-tabs" style="margin-bottom:0px;">
    <li <?=(($info['category']=='main')?'class="active"':'')?>>
      <a href="/cat" style="padding: 7px 12px;">
        Объявления Кривого Рога
      </a>
    </li>
    
    <?if(fn__get_site_razdel()=='love'){?>
    <li class="active">
      <a href="/love" style="padding: 7px 12px;">
        Знакомства в Кривом Роге
      </a>
    </li>
    <?}?>
    
    <li <?=(($info['category']=='myobj')?'class="active"':'')?>>
      <a href="/account/obj" style="padding: 7px 12px;">
        Мои объявления 
      </a>
    </li>
    
    <li <?=(($info['category']=='messages')?'class="active"':'')?>>
      <a href="/account/messages<?=(intval($info['count_unreaded'])?'?unreaded':'')?>" style="padding: 7px 12px;">
        Мои сообщения <?=((intval($info['count_unreaded'])>0)?'('.$info['count_unreaded'].')':'')?>
      </a>
    </li>
    
    <li <?=((($info['category']=='settings_common')||
             ($info['category']=='settings_password'))?'class="active"':'')?>>
      <a href="/account/settings" style="padding: 7px 12px;">
        Мои настройки
      </a>
    </li>
  </ul>
  
  
  
  
  <?if($info['category']=='myobj'):?>
  <div class="tab-content">
    <div id="yw42_tab_1" class="tab-pane fade active in">
      <ul id="yw16" class="nav nav-pills" style="margin:0px;">
        <li class="active"><a href="/account/obj">Все (<?=$info['count_active_obj']?>)</a></li>
        <!-- <li><a href="#/account/archive">Неактивные (0)</a></li> -->
      </ul>
    </div>
  </div>
  <?endif;?>
  
  
  
  
  <?if($info['category']=='messages'):?>
  <div class="tab-content">
    <div class="tab-pane fade active in" style="padding: 7px;">
      <ul class="nav nav-pills" style="margin:0px;">
        <li <?=(isset($_GET['unreaded'])?'':'class="active"')?>><a href="/account/messages">Все</a></li>
        <?if($info['count_unreaded']):?>
          <li <?=(isset($_GET['unreaded'])?'class="active"':'')?>>
            <a href="/account/messages?unreaded">
             Непрочитанные (<?=$info['count_unreaded']?>)
            </a>
          </li>
        <?endif;?>
      </ul>
    </div>
  </div>
  <?endif;?>
  
  
  
  <?if(($info['category']=='settings_common')||($info['category']=='settings_password')):?>
  <div class="tab-content">
    <div class="tab-pane fade active in">
      <ul class="nav nav-pills" style="margin:0px;">
        <li <?=(($info['category']=='settings_common')?'class="active"':'')?>>
          <a href="/account/settings">Общие</a></li>
        <li <?=(($info['category']=='settings_password')?'class="active"':'')?>>
          <a href="/account/settings/password">Пароль</a></li>
        <!--
        <li><a href="/account/messages/readed">Прочитанные (0)</a></li>
        <li><a href="/account/messages/unreaded">Непрочитанные (0)</a></li>
        -->
      </ul>
    </div>
  </div>
  <?endif;?>
  
  
  
  
  <?if(fn__get_site_razdel()=='love'):?>
  <div class="tab-content">
    <div class="tab-pane fade active in" style="padding: 7px;">
      <ul class="nav nav-pills" style="margin:0px;">
        <li <?=(($info['category']=='love_index')?'class="active"':'')?>><a href="/love">Начало</a></li>
        <li <?=(($info['category']=='love_search')?'class="active"':'')?>><a href="/love/search">Поиск</a></li>
        <li <?=(($info['category']=='love_user')?'class="active"':'')?>>
           <a href="/love/profile">Моя анкета</a>
        </li>
      </ul>
    </div>
  </div>
  <?endif;?>
  
  
  
</div>
