<?php

class AccountController extends Controller {

  public $layout='//layouts/main';

  public function filters()   
  {   
    if (!fn__get_user_id()){
       header("Location: /login?ret=account/obj");
       exit();
       }

    Yii::app()->cache->flush();
  }
  
  

	public function actionIndex($id=NULL)
	{	
    header("Location: /account/obj");
    exit;
	}



	public function actionObj($id=NULL)
	{
	  if (intval($id)){
      include('account/obj_item.php');
	  }else{
      include('account/obj.php');
	  }
	  
	}





	public function actionMessages($command='all',$id=NULL)
	{
      include('account/messages.php');
	}
	
	public function actionSettings($command='settings_common',$id=NULL){
      include('account/settings.php');
	}
	
	
	
	
	public function actionMessagelist($command='all',$id=NULL){
    	$layout='//layouts/empty';
      include('account/messagelist.php');
	}


}
