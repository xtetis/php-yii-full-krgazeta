<?

if ($command=='settings_common')
{
  
  if (isset($_POST['sbm_settings_common']))
  {
		$params = array(
				            array('required_fields',
				                  array(
				                        'username'=>'Контактное лицо',
				                        'phone'=>'Телефон',
				                       )
				                ),
				           array('length',array('username'=>'Контактное лицо'),array('minlength'=>3)),
				           array('length',array('username'=>'Контактное лицо'),array('maxlength'=>40)),
				           array('nostopwords',array('username'=>'Контактное лицо')),
				           
				           array('number',array('phone'=>'Номер телефона')),
				           array('length',array('phone'=>'Номер телефона'),array('minlength'=>6)),
				           array('length',array('phone'=>'Номер телефона'),array('maxlength'=>12)),
				           
				           array('length',array('skype'=>'Skype'),array('maxlength'=>20)),
				           array('skype',array('skype'=>'Skype')),
				           array('nostopwords',array('skype'=>'Skype')),
				           );
		$result_validate = fn__validate_field_list($_POST['formdata'],$params);
		$data['info']=$_POST['formdata'];
		$data['info']['errform']=$result_validate['errors'];
		if (!count($result_validate['errors']))
		{
			$sql="UPDATE 
							`xta_user` 
						SET 
							`username`='".sql_valid($_POST['formdata']['username'])."',
							`phone`='".sql_valid($_POST['formdata']['phone'])."',
							`skype`='".sql_valid($_POST['formdata']['skype'])."'
						WHERE 
						`id`=".fn__get_user_id();
			Yii::app()->db->createCommand($sql)->execute();
			$data['info']['alert']='Данные успешно сохранены';
		}
	}
	else
	{
		$data['info']['username'] = fn__get_field_val_by_id('xta_user','username',fn__get_user_id());
		$data['info']['phone']    = fn__get_field_val_by_id('xta_user','phone',fn__get_user_id());
		$data['info']['skype']    = fn__get_field_val_by_id('xta_user','skype',fn__get_user_id());
	}
	$this->render('view__account_settings',$data);
}




if ($command=='password')
{
  if (isset($_POST['sbm_settings_password']))
  {
		$params = array(
				            array('required_fields',
				                  array(
				                        'current_password'=>'Текущий пароль',
				                        'new_password'=>'Новый пароль',
				                        'new_password_confirm'=>'Подтверждение нового пароля',
				                       )
				                ),
		                                   
				           array('user_password_correct',array('current_password'=>'Текущий пароль'),
				                                         array('id_user'=>fn__get_user_id())),
				           array('length',array('new_password'=>'Новый пароль'),array('minlength'=>6)),
				           array('length',array('new_password_confirm'=>'Подтверждение нового пароля'),
				                          array('minlength'=>6)),
				                          
		               array('equal',array('new_password'=>'Новый пароль',
		                                   'new_password_confirm'=>'Подтверждение нового пароля')),
				           );
		$data['info']=$_POST['formdata'];
		$result_validate = fn__validate_field_list($data['info'],$params);
		$data['info']['errform']=$result_validate['errors'];
		if (!count($result_validate['errors']))
		{
			$sql="UPDATE 
							`xta_user` 
						SET 
							`pass`='".sql_valid(md5(md5($data['info']['new_password'])))."'
						WHERE 
							`id`=".fn__get_user_id();
			Yii::app()->db->createCommand($sql)->execute();
			$data['info']['alert']='Пароль успешно изменен. Новый пароль для входа - '.$data['info']['new_password'];
			$data['info']['alert_class']='alert-success';
			$data['info']['current_password']='';
			$data['info']['new_password']='';
			$data['info']['new_password_confirm']='';
		}else{
			$data['info']['alert']='В форме присутствуют ошибки';
			$data['info']['alert_class']='alert-danger';
		}
	}
	$this->render('view__account_settings_password',$data);
}

