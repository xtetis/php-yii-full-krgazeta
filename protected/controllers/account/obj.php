<?php

// Если получена команда деактивировать объявление
//==============================================================================
if (isset($_GET['unpubobj'])){
     $id_obj = intval($_GET['unpubobj']);
     if (fn__is_user_obj_author($id_obj)){
        $sql = "UPDATE `xta_obj` SET `published` = 0 WHERE `id` = ".$id_obj;
        Yii::app()->db->createCommand($sql)->execute();
        fn__clear_cache();
        }
     header("Location: /account/obj"); 
     exit();
   }
//==============================================================================



// Если получена команда опубликовать объявление
//==============================================================================
if (isset($_GET['pubobj'])){
     $id_obj = intval($_GET['pubobj']);
     if (fn__is_user_obj_author($id_obj)){
        $sql = "UPDATE `xta_obj` SET `published` = 1 WHERE `id` = ".$id_obj;
        Yii::app()->db->createCommand($sql)->execute();
        fn__clear_cache();
        }
     header("Location: /account/obj"); 
     exit();
   }
//==============================================================================





$data['info']['content']=fn__get_user_account_obj();
$data['info']['count_active']= fn__get_count_by_where('xta_obj',"
   `id_user` = ".fn__get_user_id()." AND
   `id_city` = ".fn__get_field_val_by_id('xta_site','id_city',fn__get_site_id())
  );


$this->render('view__account_obj',$data);
