<?php

class LoveController extends Controller {



	public function actionIndex($command='',$id=NULL){
    include('love/index.php');
	}
	
	public function actionUser($command='',$id=NULL){
    include('love/user.php');
	}
	
	public function actionProfile($command='',$id=NULL){
    if (!fn__get_user_id()){
        header("Location: /login?ret=account/obj");
        exit();    
       }
    include('love/profile.php');
	}

	public function actionSearch($command='',$id=NULL){
    include('love/search.php');
	}
	
	public function actionTop($command='',$id=NULL){
    include('love/top.php');
	}


}
