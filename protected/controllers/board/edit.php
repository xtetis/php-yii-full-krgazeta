<?

  $id = intval($id);
  
  // Если пользователь не авторизирован - редаректим на форму авторизации
  if (!fn__is_user_obj_author($id)){ header("Location: /cat"); exit(); }
  
  
  $sql = "SELECT *
          FROM `xta_obj` 
          WHERE `id` = ".$id;
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row){
     $val['name'] = htmlspecialchars($row['name']);
     $val['about'] = $row['about'];
     $val['district'] = $row['id_district'];
     $val['phone'] = $row['phone'];
     $val['valuta'] = $row['id_valuta'];
     $val['price'] = $row['price'];
     $val['category'] = $row['id_category'];
     $val['username'] = $row['username'];
     $val['skype'] = $row['skype'];
    }
  
if (isset($_POST['sbm'])){
  $error=array();
  $val=array();
 
 
 
 
 
 
    
  //----------------------------------------------------------------------------  
  $name = isset($_POST['form']['name']) ? $_POST['form']['name'] : '';
  $val['name'] = mb_substr(htmlspecialchars(strip_tags($name)),0,70,'utf-8');
  
  //----------------------------------------------------------------------------
  if ((mb_strlen($name,'utf-8')<20)||(mb_strlen($name,'utf-8')>70)){
     $error['name'] = 'Длина заголовка объявления должна быть не короче 20 и не длиннее 70 символов';
     }
     
  // Проверка на стоп слова -----------------
  $stop_words=explode(',',fn__get_setting('obj_stop_words'));
  foreach ($stop_words as $item)
  {
    if (strpos($val['name'],trim($item))!==false){
     $error['name']='Вы использвали недопустимое слово - "'.trim($item).'"';
     break;
    }
  }
  //----------------------------------------------------------------------------
  
  
  
  
  
  
  




  //----------------------------------------------------------------------------  
  $phone = isset($_POST['form']['phone']) ? $_POST['form']['phone'] : '';
  $val['phone'] = fn__get_only_numbers($phone);
  
  //----------------------------------------------------------------------------
  if (!$val['phone']){
     $error['phone'] = 'Обязательно введите контактный номер телефона';
     $val['phone']='';
     }
  //----------------------------------------------------------------------------
  if ($val['phone']>99999999999){
     $error['phone'] = 'Максимальное количество символов 11';
     $val['phone']=intval(substr(fn__get_only_numbers($phone),0,11));
     }
  //----------------------------------------------------------------------------
  if (strlen(strval($val['phone']))<6){
     $error['phone'] = 'Минимальное количество символов 6';
     }
  //----------------------------------------------------------------------------

















  // Текст объявления
  //****************************************************************************
  $about = isset($_POST['form']['about']) ? $_POST['form']['about'] : '';
  $val['about'] = htmlspecialchars(strip_tags($about));
  
  //----------------------------------------------------------------------------
  if (!strlen($val['about'])){
     $error['about'] = 'Обязательно введите текст объявления';
     $val['about']='';
     }
  //----------------------------------------------------------------------------
  if (mb_strlen($val['about'],'utf-8')<100){
     $error['about'] = 'Минимальное количество символов 100';
     }
  //----------------------------------------------------------------------------
  if (mb_strlen($val['about'],'utf-8')>6000){
     $error['about'] = 'Максимальное количество символов 6000';
     $val['about'] = mb_substr($val['about'],0,6000,'utf-8');
     }
  // Проверка на стоп слова -----------------
  $stop_words=explode(',',fn__get_setting('obj_stop_words'));
  foreach ($stop_words as $item)
  {
    if (strpos($val['about'],trim($item))!==false){
     $error['about']='Вы использвали недопустимое слово - "'.trim($item).'"';
     break;
    }
  }
  //****************************************************************************






  // Район города
  //****************************************************************************
  $district = isset($_POST['form']['district']) ? $_POST['form']['district'] : '-1';
  $val['district'] = intval($district);
  
  //----------------------------------------------------------------------------
  if (!strlen($val['district'])){
     $error['district'] = 'Обязательно выберите район города';
     $val['district']=-1;
     }
  //----------------------------------------------------------------------------
  $id_city=fn__get_field_val_by_id('xta_site','id_city',fn__get_site_id());
  $count = fn__get_count_by_where('xta_district','`id` = '.$val['district'].' AND `id_city`='.$id_city,false);
  
  if ((!intval($count))&&($val['district']!=0)){
     $error['district'] = 'Выберите корректный район города';
     $val['district']=-1;
  }
  //****************************************************************************










  // Валюта
  //****************************************************************************
  $valuta = isset($_POST['form']['valuta']) ? $_POST['form']['valuta'] : '0';
  $val['valuta'] = intval($valuta);
  
  //----------------------------------------------------------------------------
  if (!strlen($val['valuta'])){
     $error['torg'] = 'Выберите корректную валюту';
     $val['valuta']=0;
     }
  //----------------------------------------------------------------------------
  $count = fn__get_count_by_where('xta_valuta','`id` = '.$val['valuta']);
  
  if (!intval($count)){
     $error['torg'] = 'Выберите корректную валюту';
     $val['valuta']=0;
  }
  //****************************************************************************














  //  Сумма
  //----------------------------------------------------------------------------  
  $price = isset($_POST['form']['price']) ? $_POST['form']['price'] : '';
  $val['price'] = intval(fn__get_only_numbers($price));
  
  //----------------------------------------------------------------------------
  if ($val['price']>99999999999){
     $error['torg'] = 'Слишком большая сумма';
     $val['price']=intval(substr(fn__get_only_numbers($price),0,11));
     }
  //----------------------------------------------------------------------------
  if (strlen(strval($val['price']))<0){
     $error['torg'] = 'Сумма не может быть отрицательной';
     $val['price']=0;
     }
  //----------------------------------------------------------------------------







  // Категория объявления
  //****************************************************************************
  $category = isset($_POST['form']['category']) ? $_POST['form']['category'] : '0';
  $val['category'] = intval($category);
  
  //----------------------------------------------------------------------------
  if (!$val['category']){
     $error['category'] = 'Обязательно выберите категорию объявления';
     $val['category']=0;
     }
  //----------------------------------------------------------------------------
  $count_parent = fn__get_count_by_where('xta_obj_category','`id_parent` = '.$val['category'],false);
  $count = fn__get_count_by_where('xta_obj_category','`id` = '.$val['category'],false);
  
  if (($count_parent)||(!$count)){
     $error['category'] = 'Выберите корректную категорию объявления';
     $val['category']=0;
  }
  //****************************************************************************












  // Имя пользователя
  //----------------------------------------------------------------------------  
  $username = isset($_POST['form']['username']) ? $_POST['form']['username'] : '';
  $val['username'] = mb_substr(htmlspecialchars(strip_tags(trim($username))),0,40,'utf-8');
  
  //----------------------------------------------------------------------------
  if ((mb_strlen($username,'utf-8')<3)||(mb_strlen($username,'utf-8')>40)){
     $error['username'] = 'Допускается не короче 3 и не длиннее 40 символов';
     }
  // Проверка на стоп слова -----------------
  $stop_words=explode(',',fn__get_setting('obj_stop_words'));
  foreach ($stop_words as $item)
  {
    if (strpos($val['username'],trim($item))!==false){
     $error['username']='Вы использвали недопустимое слово - "'.trim($item).'"';
     break;
    }
  }
  //----------------------------------------------------------------------------
  
  
  
  
  
  
  
  
  

  // Skype пользователя
  //----------------------------------------------------------------------------  
  $skype = isset($_POST['form']['skype']) ? $_POST['form']['skype'] : '';
  $val['skype'] = mb_substr(trim($skype),0,20,'utf-8');
  //----------------------------------------------------------------------------
  if (mb_strlen($val['skype'],'utf-8')>20){
     $error['skype'] = 'Допускается не длиннее 20 символов';
     }
  //----------------------------------------------------------------------------
  if ((!fn__check_skype($val['skype']))&&(strlen($val['skype']))){
     $error['skype'] = 'Некорректный Skype';
     }
  // Проверка на стоп слова -----------------
  $stop_words=explode(',',fn__get_setting('obj_stop_words'));
  foreach ($stop_words as $item)
  {
    if (strpos($val['skype'],trim($item))!==false){
     $error['skype']='Вы использвали недопустимое слово - "'.trim($item).'"';
     break;
    }
  }
  //----------------------------------------------------------------------------











  // Eсли ошибок не найдено - вставляем запись
  //---------------------------------------------------------------------------
  if (!count($error)){
      $error_insert = array();
      $message_insert = array();

      $sql ="
      UPDATE `xta_obj` SET 
        `id_district`=".$val['district'].",
        `id_category`=".$val['category'].",
        `id_valuta`=".$val['valuta'].",
        `username`='".sql_valid($val['username'])."',
        `name`='".sql_valid($val['name'])."',
        `about`='".sql_valid($val['about'])."',
        `phone`='".$val['phone']."',
        `skype`='".sql_valid($val['skype'])."',
        `price`=".sql_valid($val['price'])."
      WHERE 
        `id`=".$id;
      Yii::app()->db->createCommand($sql)->execute();
      header("Location: /account/obj"); 
      exit;
     }
  //---------------------------------------------------------------------------







}
  
  
  
  
  
  
  
  

  





$data['error']=$error;
$data['val']=$val;

$this->render('view__board_edit',$data);
?>
