<?php
global $error_img_insert;


$data['info']['id_district']=-1;
if (fn__get_user_id()){
   $data['info']['username']=fn__get_user_info(fn__get_user_id(),'username');
   $data['info']['email']=fn__get_user_info(fn__get_user_id(),'email');
   $data['info']['phone']=fn__get_user_info(fn__get_user_id(),'phone');
   $data['info']['skype']=fn__get_user_info(fn__get_user_id(),'skype');
   }



if (isset($_POST['formdata'])){
   $data['info']=$_POST['formdata'];

   // Картинки для объявления
   //----------------------------------------------------------------------------  
   $data['img'] = array(); $script='';
   for ($i = 0; $i < 7; $i++){
     if ((isset($_POST['img'.$i]))&&(strlen($_POST['img'.$i]))){
          $data['img'][] = $_POST['img'.$i];
          $script.='dataArray.push({name : "'.$i.'.jpg", 
                                    value : "'.$_POST['img'.$i].'"}); ';
        }
     }
   $data['info']['script'] = $script;
   //----------------------------------------------------------------------------




   $data['info']=fn__obj_add_prepare_post_data($data['info']);
   $result_validate = fn__obj_add_validate($data['info']);
   $data['info']['errform']=$result_validate['errors'];
   if (!count($result_validate['errors'])){
      $data['info'] = fn__obj_add_prepare_options($data['info']);
      $id_user = fn__get_user_id();
      $new_user=false;
      if (!$id_user){
         $new_user=true;
         $userinfo['id_district']=$data['info']['id_district'];
         $userinfo['email']=$data['info']['email'];
         $userinfo['pass']=substr(fn__get_random_hash(),0,7);
         $userinfo['username']=$data['info']['username'];
         $userinfo['phone']=$data['info']['phone'];
         $userinfo['skype']=$data['info']['skype'];
         $id_user = fn__create_user($userinfo);
         fn__set_login($userinfo['email'],$userinfo['pass']);
         }
      $id_obj = fn__create_obj($data['info'],$id_user);
      fn__clear_cache();

      
      if ($new_user){
          $text= '<div class="alert in alert-block fade alert-success" style=" color:#555753;">
                    <p>Ваше объявление успешно опубликовано и создана учетная запись:</p>
                    <ul style="list-style-type: none;">
                      <li>
                        <b>Логин</b> - '.$userinfo['email'].'
                      </li>
                      <li>
                        <b>Пароль</b> - '.$userinfo['pass'].'
                      </li>
                    </ul>
                    <br>
                    <p>Вы можете использовать эти данные для 
                       <a target="_blank" href="/login">авторизации на сайте</a>.</p>
                    <p>Регистрационные данные отправлены Вам на электронную почту 
                       <b>'.$userinfo['email'].'</b>.</p>
                    <p>Список ваших оъявлений расположен на странице 
                       "<a target="_blank" href="/account/obj">Мои объявления</a>".</p>
                    <p>Добавленное объявление расположено на странице 
                       "<a target="_blank" href="/board/'.$id_obj.'">
                       http://krgazeta.com/board/'.$id_obj.'</a>".</p>
                       
                  </div>';
          fn__go_to_message_page($text);
         }else{
         header("Location: /board/".$id_obj); 
         exit;
         }

      }




}


$this->render('view__board_add',$data);
