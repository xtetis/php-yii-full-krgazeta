<?

if (fn__get_user_id()){
   header("Location: /account/settings");
   exit();
   }
   
   
   
$data = array();

if (isset($_POST['formdata'])){
   $result_validate = fn__register_user_validate($_POST['formdata']);
   $data['info']=$_POST['formdata'];
   $data['info']['errform']=$result_validate['errors'];
   if (!count($result_validate['errors'])){
      fn__create_user($_POST['formdata']);
      extract($_POST['formdata'], EXTR_PREFIX_ALL,'formdata');
      fn__set_login($formdata_email,$formdata_pass);
      $text= '<div class="alert in alert-block fade alert-success" style=" color:#555753;">
                <p>Успешно создана учетная запись:</p>
                <ul style="list-style-type: none;">
                  <li>
                    <b>Логин</b> - '.$formdata_email.'
                  </li>
                  <li>
                    <b>Пароль</b> - '.$formdata_pass.'
                  </li>
                </ul>
                <br>
                <p>Вы можете использовать эти данные для <a href="/login">авторизации на сайте</a>.</p>
                <p>Регистрационные данные отправлены Вам на электронную почту.</p>
              </div>';
      fn__go_to_message_page($text);
      }
}


$this->render('viev__login_register',$data);
