<?

  $orig_id = $id;
  $id = intval($id);
  
  // ПРоверяем корректновть ввода ID категории
  //============================================================================
  $correct_id = fn__get_count_by_where('xta_obj_category','`id` = '.$id);
  if ($id){
     if (!$correct_id){
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: /cat");
        exit();  
        }  
    } 
  
  //============================================================================
  
  
  // Получаем данные о категории
  //============================================================================
  $data['info']['page_title']=fn__get_field_val_by_id('xta_obj_category','title',$id);
  $data['info']['page_description']=fn__get_field_val_by_id('xta_obj_category','description',$id);
  $data['info']['page_hone']=fn__get_field_val_by_id('xta_obj_category','hone',$id);
  $data['info']['page_seotext']=fn__get_field_val_by_id('xta_obj_category','seotext',$id);
  $data['info']['categname']=fn__get_field_val_by_id('xta_obj_category','name',$id);
  $data['info']['id']=$id;
  $data['info']['id_parent']=fn__get_field_val_by_id('xta_obj_category','id_parent',$id);
  $data['info']['parent_category_name']=fn__get_field_val_by_id('xta_obj_category','name',$data['info']['id_parent']);
  
  
  $data['info']['topcontent']=fn__get_category_block_in_cat($id);
  $data['info']['topcontent'].=fn__get_obj_list($id).'<br>';
  
  

  
  
$this->render('viev__cat_index',$data);
?>
