<?php
  $data["info"]["command"]=$command;
  
  
  
  
  
  
  

//Показываем список сайтов
//**************************************************************************************************
if ($command=='select'){
  
  $count_records = fn__get_count_by_where('xta_site');
  $maxpage=ceil($count_records/10);
  $page=fn__get_correct_page($maxpage);
  $data["info"]["pagination"].=fn__get_pagination('/admin/xta_site?page=', $maxpage, $page);
  
  $sql="SELECT `xta_site`.*, `xta_city`.`name` as 'cityname' FROM `xta_site` 
        LEFT JOIN `xta_city` on `xta_city`.`id` = `xta_site`.`id_city`
        LIMIT ".(10*($page-1)).",10";
	  $reader =Yii::app()->db->createCommand($sql)->query(); 
	  $data['info']['select_table']='<table class="table table-bordered table-hover" style="margin-bottom:0px;">
	  <tr style="background:#d3d7cf;">
	    <th style="width: 30px;">#</th>
	    <th>Хост</th>
	    <th>Категория</th>
	    <th style="width: 60px;"></th>
	  </tr>
	  ';
	  foreach ($reader as $row){
	  $data['info']['select_table'].='
	  <tr>
	    <td>'.$row['id'].'</td>
	    <td>'.$row['name'].'</td>
	    <td>'.$row['cityname'].'</td>
	    <td>
	      <div class="btn-group btn-group-sm btn-group-select_item">
	        <a href="/admin/xta_site/edit/'.$row['id'].'" 
	           class="btn btn-default"
	           data-toggle="tooltip" 
	           data-placement="top" 
	           title="Редактировать запись">
	          <span class="glyphicon glyphicon-edit"></span></a>
	        <a href="/admin/xta_site/delete/'.$row['id'].'" class="btn btn-default" 
	           onclick="return confirmDelete();"
	           data-toggle="tooltip" 
	           data-placement="top" 
	           title="Удалить запись">
	           <span class="glyphicon glyphicon-trash"></span></a>
	      </div>
	    </td>
	  </tr>
	  ';
	  }
	  $data['info']['select_table'].='</table>
	  <div style="text-align:right;">
 	  Показано '.($count_records?((10*($page-1))+1):$count_records).' - 
	  '.(($count_records>(10*($page))?(10*($page)):$count_records)).' 
	  из '.$count_records.' записей</div>';
}
//**************************************************************************************************


  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
// Создаем сайт
//**************************************************************************************************
if ($command=='create'){
   if(isset($_POST['sbm']))
     {
       $_name    = sql_valid(strip_tags($_POST['formdata']['name']));
       $_id_city = intval($_POST['formdata']['id_city']);
       $sql="INSERT INTO `xta_site`(
                `id_city`,
                `name`
             ) VALUES (
                ".$_id_city.",
                '".$_name."'
             )";
       Yii::app()->db->createCommand($sql)->execute(); 
       header("Location: /admin/xta_site/edit/".fn__get_max_table_id('xta_site'));
       exit();
    }
    
    $sql = 'SELECT * FROM `xta_city`';
    $tpl = '<option value="+id+" +default+>+name+</option>';
    $data['info']['id_city']=fn__get_select_by_sql_i_tpl($sql,$tpl);
}
//**************************************************************************************************




















// Редактируем сайт
//**************************************************************************************************
if ($command=='edit')
{
	if(isset($_POST['sbm']))
	{
		$_name    = sql_valid(strip_tags($_POST['formdata']['name']));
		$_id_city = intval($_POST['formdata']['id_city']);
		$sql="UPDATE `xta_site` SET 
		         `name`='".$_name."', 
		         `id_city`=".$_id_city."
		      WHERE 
		         `id`=".$id;
		Yii::app()->db->createCommand($sql)->execute(); 
	}

	$sql = "SELECT * FROM `xta_site` WHERE `id` = ".$id;
	$row = Yii::app()->db->createCommand($sql)->queryRow(); 
	foreach ($row as $key => $value)
	{
		$data['info'][$key] = $value;
	}

	$sql = 'SELECT * FROM `xta_city`';
	$tpl = '<option value="+id+" +default+>+name+</option>';
	$data['info']['id_city']=fn__get_select_by_sql_i_tpl($sql,$tpl,$data['info']['id_city']);
}
//**************************************************************************************************











// Удаляем сайт
//**************************************************************************************************
if ($command=='delete'){
  fn__del_record_by_id('xta_site',$id);
  header("Location: /admin/xta_site");
  exit();
}
//**************************************************************************************************









$this->render('view__xta_site',$data); 
