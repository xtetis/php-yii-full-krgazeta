<?php
$data["info"]["command"]=$command;
$table = 'xta_user';
$data["info"]["table"] = $table;
  
  
  
  
  
  
  

//Показываем список
//**************************************************************************************************
if ($command=='select'){
  
  $count_records = fn__get_count_by_where($table);
  $maxpage=ceil($count_records/10);
  $page=fn__get_correct_page($maxpage);
  $data["info"]["pagination"].=fn__get_pagination('/admin/'.$table.'?page=', $maxpage, $page);
  
  $sql="SELECT 
          xta_user.* ,
          xta_user_role.name 
        FROM ".$table." 
        LEFT JOIN
          xta_user_role on 
          xta_user_role.id = xta_user.id_user_role
        LIMIT 10 OFFSET ".(10*($page-1))." ";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $data['info']['select_table']='<table class="table table-bordered table-hover" style="margin-bottom:0px;">
  <tr style="background:#d3d7cf;">
    <th style="width: 30px;">#</th>
    <th>Email пользователя</th>
    <th>Роль пользователя</th>
    <th style="width: 60px;"></th>
  </tr>
  ';
  foreach ($reader as $row){
  $data['info']['select_table'].='
  <tr>
    <td>'.$row['id'].'</td>
    <td>'.$row['email'].'</td>
    <td>'.$row['name'].'</td>
    <td>
      <div class="btn-group btn-group-sm btn-group-select_item">
        <a href="/admin/'.$table.'/edit/'.$row['id'].'" 
           class="btn btn-default"
           data-toggle="tooltip" 
           data-placement="top" 
           title="Редактировать запись">
          <span class="glyphicon glyphicon-edit"></span></a>
        <a href="/admin/'.$table.'/delete/'.$row['id'].'" class="btn btn-default" 
           onclick="return confirmDelete();"
           data-toggle="tooltip" 
           data-placement="top" 
           title="Удалить запись">
           <span class="glyphicon glyphicon-trash"></span></a>
      </div>
    </td>
  </tr>
  ';
  }
  $data['info']['select_table'].='</table>
  <div style="text-align:right;">
  Показано '.($count_records?((10*($page-1))+1):$count_records).' - 
  '.(($count_records>(10*($page))?(10*($page)):$count_records)).' 
  из '.$count_records.' записей</div>';
}
//**************************************************************************************************


  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
// Создаем запись
//**************************************************************************************************
if ($command=='create'){
   if(isset($_POST['sbm']))
     {
       $_email = sql_valid(strip_tags($_POST['formdata']['email']));
       $_id_user_role = intval($_POST['formdata']['id_user_role']);
       $sql="INSERT INTO `".$table."` (`id_user_role,email`) 
             VALUES (".$_id_user_role.",'".$_email."')";
       Yii::app()->db->createCommand($sql)->execute(); 
       header("Location: /admin/".$table."/edit/".fn__get_max_table_id($table));
       exit();
    }
    
  $data['info']['id_user_role']=fn__get_select_by_sql_i_tpl(
      'SELECT * FROM `xta_user_role`','<option value="+id+" +default+>+name+</option>');
}
//**************************************************************************************************




















// Редактируем
//**************************************************************************************************
if ($command=='edit')
{

	if(isset($_POST['sbm']))
	{
		$_email = sql_valid(strip_tags($_POST['formdata']['email']));
		$_id_user_role = intval($_POST['formdata']['id_user_role']);
		$sql="UPDATE `".$table."` SET 
		        `email`='".$_email."',
		        `id_user_role`='".$_id_user_role."'
		      WHERE id=".$id;
		Yii::app()->db->createCommand($sql)->execute(); 

		if (strlen($_POST['formdata']['pass']))
		{
			$_pass = md5(md5($_POST['formdata']['pass']));
			$sql="UPDATE `".$table."` SET 
				      `pass`='".$_pass."'
				    WHERE `id`=".$id;
			Yii::app()->db->createCommand($sql)->execute(); 
		}
	}
   

   $sql="SELECT * FROM `".$table."` WHERE `id` = ".$id;
   $row =Yii::app()->db->createCommand($sql)->queryRow(); 
   $data['info']['email']=$row['email'];
   $data['info']['id_user_role']=fn__get_select_by_sql_i_tpl(
      'SELECT * FROM `xta_user_role`','<option value="+id+" +default+>+name+</option>',$row['id_user_role']);
}
//**************************************************************************************************











// Удаляем сайт
//**************************************************************************************************
if ($command=='delete'){
  fn__del_record_by_id($table,$id);
  header("Location: /admin/".$table);
  exit();
}
//**************************************************************************************************









echo $this->render('view__'.$table,$data); 
