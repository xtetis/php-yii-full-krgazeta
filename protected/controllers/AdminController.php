<?php

class AdminController extends Controller {

  public $layout='//layouts/admin';
  

  public function filters()   
  {   
    if (!fn__get_user_id()){
        header("Location: /login?ret=admin");
        exit();    
    }
    
    if (fn__get_user_role()!='admin'){
        header("Location: /");
        exit();   
    }
  }




	public function actionIndex($id=NULL){
	  $this->render('view__index');
	}



	public function actionXta_country($command='select',$id=NULL)
	{
	  include('admin/xta_country.php');
	}




	public function actionXta_setting($command='select',$id=NULL){
	  include('admin/xta_setting.php');
	}


	public function actionXta_setting_category($command='select',$id=NULL){
	  include('admin/xta_setting_category.php');
	}


	public function actionXta_region($command='select',$id=NULL){
	  include('admin/xta_region.php');
	}


	public function actionXta_city($command='select',$id=NULL){
	  include('admin/xta_city.php');
	}





	public function actionXta_district($command='select',$id=NULL){
	  include('admin/xta_district.php');
	}




	public function actionXta_obj_category($command='select',$id=NULL){
	  include('admin/xta_obj_category.php');
	}






	public function actionXta_obj_option($command='select',$id=NULL){
	  include('admin/xta_obj_option.php');
	}





//******************************************************************************
	public function actionMod_cache($command=''){
	  include('admin/mod_cache.php');
  	}
//******************************************************************************



	public function actionMod_sitemap($command='',$id=NULL)
	{
		include('admin/mod_sitemap.php');
	}




	public function actionParser($command='odnagazeta_com',$id=NULL){
	  include('admin/parser/'.$command.'.php');
	}


	public function actionXta_user_role($command='select',$id=NULL)
	{
		include('admin/xta_user_role.php');
	}


	public function actionXta_user($command='select',$id=NULL)
	{
		include('admin/xta_user.php');
	}


	public function actionXta_site($command='select',$id=NULL)
	{
		include('admin/xta_site.php');
	}
}
