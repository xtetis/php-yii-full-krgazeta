<?php 
// Функция возвращает ID альбома достопримечательности (или создает пустой)
//function fn__get_story_album($id=0,$albumname=''){ 

// Функция максимальный ID новости
// function fn__get_max_story(){




// Функция возвращает ID альбома достопримечательности (или создает пустой)
//****************************************************************************** 
function fn__get_story_album($id=0,$albumname=''){ 
  $albumname = mb_substr($albumname,0,100,'utf-8');
  $id = intval($id);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_story` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $sql = "SELECT `id_album` FROM `xta_story` WHERE `id` = ".$id;
     $row1 =Yii::app()->db->createCommand($sql)->queryRow();
     $id_album=intval($row1['id_album']);
     
     $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id` = ".$id_album;
     $row1 =Yii::app()->db->createCommand($sql)->queryRow();
     if ($row1['count']){
       return $id_album;
     }else{
       $sql = "INSERT INTO `xta_albums`(`name`, `description`) VALUES ('".$albumname."','');";
       Yii::app()->db->createCommand($sql)->execute();
       $sql = "SELECT MAX(`id`) as 'max' FROM `xta_albums`";
       $row2 =Yii::app()->db->createCommand($sql)->queryRow();
       $id_album=intval($row2['max']);
       $sql = "UPDATE `xta_story` SET `id_album`=".$row2['max']." WHERE `id`=".$id;
       Yii::app()->db->createCommand($sql)->execute();
       return $id_album;
     }
  }
  return false;
}
//****************************************************************************** 










// Функция максимальный ID новости
//****************************************************************************** 
function fn__get_max_story(){
   $sql="SELECT MAX(`id`) as 'max' FROM `xta_story`";
   $row =Yii::app()->db->createCommand($sql)->queryRow(); 
   return $row['max'];
}
//****************************************************************************** 















// Функция возвращает: опубликован ли рассказ
//****************************************************************************** 
function fn__is_story_published($id=0){
  $id = intval($id);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_story` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $sql = "SELECT `published` FROM `xta_story` WHERE `id` = ".$id;
     $row =Yii::app()->db->createCommand($sql)->queryRow();
     return intval($row['published']);
   }
  return false;  
}
//****************************************************************************** 














// Функция публикует и снимает с публикации новость
//****************************************************************************** 
function fn__publish_story($id=0,$flag=true){
  $flag=intval($flag);
  $id = intval($id);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_story` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $val=0;
     if ($flag){$val=1;};
     $sql = "UPDATE `xta_story` SET `published`=".$val." WHERE `id`=".$id;
     Yii::app()->db->createCommand($sql)->execute();
     return true;
   }
  return false;  
}
//****************************************************************************** 

























// Функция возвращает количество опубликованных историй
// Входные параметры
// id_city - ID города (необязательный параметр)
// $id_category = ID категории 
//****************************************************************************** 
function fn__get_count_published_story($id_city=0, $id_category=0){ 

  $id_city = intval($id_city);
  $id_category = intval($id_category);  

  //----------------------------------------------------
  $id_cache='fn__get_count_published_story_'.$id_city."_".$id_category."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  



  
  $sql = "SELECT count(`id`) as 'count' 
          FROM `xta_story` 
          WHERE `published` = 1 AND `id_site`=".fn__get_site_id();
  if ($id_city){
    $sql.=' AND `id_city` = '.$id_city.' ';
  }
  
  if ($id_category){
    $sql.=' AND `id_category` = '.$id_category.' ';
  }  
  

  $row =Yii::app()->db->createCommand($sql)->queryRow();
  $ret=intval($row['count']);
  

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));
  return $ret;
}
//******************************************************************************















// Функция возвращает имя категории истории по ID
//****************************************************************************** 
function fn__get_story_category_name($id=0){ 

  //----------------------------------------------------
  $id_cache='fn__get_story_category_name_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 
  
  $ret='';
  $sql = "SELECT `name` FROM `xta_newscategory` WHERE `id` = ".intval($id);
  $row =Yii::app()->db->createCommand($sql)->queryRow();   
  $ret=$row['name']; 
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;   
}
//******************************************************************************



































// Функция возвращает список достопримечательностей 
// Входные параметры
// id_city - Фильтр по городу (необязательный)
//****************************************************************************** 
function fn__get_story_list($page=1,$id_city=0,$id_category=0){ 

  $id_city = intval($id_city);
  $id_category = intval($id_category);
  $page=intval($page);

  //----------------------------------------------------
  $id_cache='fn__get_story_list_'.$page."_".$id_city."_".$id_category."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  


  if ($id_city){
    $cityfilter=' AND `xta_story`.`id_city` = '.$id_city." "; 
  } 
  
  $category_filter='';
  if ($id_category){
    $category_filter=' AND `xta_story`.`id_category` = '.intval($id_category)." "; 
  }
  
  $sql = "SELECT 
          `xta_story`.*, 
          `xta_city`.`name` as 'cityname',
          `xta_region`.`name` as 'regionname',
          `xta_newscategory`.`name` as 'categoryname'
          FROM `xta_story`
          LEFT JOIN `xta_city` on
             `xta_city`.`id` = `xta_story`.`id_city`
          LEFT JOIN `xta_region` on
             `xta_region`.`id` = `xta_city`.`region_id`
          LEFT JOIN `xta_newscategory` on
             `xta_newscategory`.`id` = `xta_story`.`id_category`
          WHERE
            `xta_story`.`published` = 1 
            AND `xta_story`.`id_site`=".fn__get_site_id()."
            ".$cityfilter.$category_filter."  
          ORDER BY `xta_story`.`editedon` DESC   
          LIMIT ".(($page-1)*10)." , 10
          ";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row)
    {
   
      $imgsrc=fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album']),'thumb_story_'.$row['id'],100, 100);
      
      $ret.='
<div style="padding-bottom:15px;">
  <table style="height:10px;">
    <tr> 
      <td style="width: 100px; vertical-align:top; padding: 5px;"> 
        <img src="'.$imgsrc.'" alt="'.$row['name'].'" title="'.$row['name'].'" class="corner iradius15" style="width:100px; height:100px;">
      </td> 
      <td style="vertical-align:top; padding: 5px;">
        <div style="font-size:14px; font-weight:bold; text-align:left;"> 
          <a style="color: #111; font-weight:bold;" title="Нажмите, чтоб прочитать всю историю" href="/story/'.$row['id'].'"> 
            '.$row['name'].'
          </a>
        </div>
        <div style="text-align:left; margin-top:5px; font-size:12px; color: #222222;">
         '.mb_substr(strip_tags($row['about']),0,500,'utf-8').'...
        </div>
        </td>
    </tr>
    <tr>
      <td colspan="2">
      
      
      
      
      
        <table style="height:10px;">
         <tr>
          <td style="padding-right:10px;">
          <ul class="breadcrumbs breadcrumb" style="margin:0px; padding-top:2px; padding-bottom:2px;">
            <li>
              <a title="Перейти к просмотру истории" href="/story/'.$row['id'].'" style="font-size:13px;">Читать полностью ...</a>
              <span class="divider">/</span>
            </li>
            <li>
              <a rel="tooltip" title="Показать все истории города &quot;'.$row['cityname'].'&quot;" href="/story?city='.$row['id_city'].'" style="font-size:13px;">'.$row['cityname'].'</a>
              <span class="divider">/</span>
            </li>
            <li>
              <a title="Показать все истории &quot;'.$row['categoryname'].'&quot;" href="/story?category='.$row['id_category'].'" style="font-size:13px;">
                '.$row['categoryname'].'
              </a>
              <span class="divider">/</span>
            </li>
            <li class="active" style="font-size:13px;">
              '. date("d:m:Y",strtotime($row['editedon'])).'
            </li>
          </ul>
          </td>
          <td style="width:100px;">
          <a style="color:#fff; padding-left:15px; padding-right:15px;" href="/story/'.$row['id'].'" class="btn btn-primary btn-mini" title="'.$row['name'].' - прочитать подробнее">Подробнее</a>
          </td>
         </tr>
        </table>
              
      
      
      
      

        
      </td>
    </tr>
  </table>
  


</div>
      ';
    }
    
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret; 
}
//******************************************************************************

































// Функция возвращает список категорий новостей для опубликованных новостей
//****************************************************************************** 
function fn__get_list_actual_story_category($selected_category=0){ 

  //----------------------------------------------------
  $id_cache='fn__get_list_actual_story_category_'.$selected_category."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  


  $selected_category = intval($selected_category);
  $sql = "SELECT `id`,`name` FROM `xta_newscategory` WHERE `id` in (
            SELECT DISTINCT `id_category` FROM `xta_story` 
            WHERE `xta_story`.`published` = 1 AND `xta_story`.`id_site`=".fn__get_site_id()."
          )";
  $reader =Yii::app()->db->createCommand($sql)->query();   
          
  foreach ($reader as $row){
    $style='';
    if ($selected_category==$row['id']){
         $style=' style="background-image: url(\'/themes/ukrturne/img/ico/bullet_go_red.png\');"';
       }
    $ret.='
     <li>
       <a title="Показать все истории категории &quot;'.mb_strtolower($row['name'],"utf-8").'&quot;" href="/story?category='.$row['id'].'"'.$style.'>                                     
        '.$row['name'].'
       </a>
     </li>';
  }

  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret; 
}
//******************************************************************************




























// Функция возвращает корректен ли ID  истории
// Входные параметры
// id - ID истории
//****************************************************************************** 
function fn__is_correct_story_id($id=0){ 

  $id = intval($id);

  //----------------------------------------------------
  $id_cache='fn__is_correct_story_id_'.$id."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $ret=0;
  $sql = "SELECT count(`id`) as 'count' 
          FROM `xta_story` 
          WHERE `id` = ".$id." AND `id_site`=".fn__get_site_id();
  $row =Yii::app()->db->createCommand($sql)->queryRow();   
  if ($row['count']){
     $ret=$id;
   }
    
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret;  
}
//******************************************************************************








































// Функция возвращает истории по фильтру (внизу страницы)
// Входные параметры
// id - ID истории
//****************************************************************************** 
function fn__get_bottom_list_story($id_city=0, $exclude_id=0){ 


  $id_city = intval($id_city);
  $exclude_id = intval($exclude_id);

  //----------------------------------------------------
  $id_cache='fn__get_bottom_list_story_'.$id_city."_".$exclude_id."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  


  
  if ($id_city){
    $sql_city=' AND `id_city` = '.$id_city.' ';
  }

  
  if ($exclude_id){
    $sql_exclude=' AND `id` <> '.$exclude_id.' ';
  }
  
  $sql = "SELECT * FROM `xta_story` 
          WHERE `published` =1 
          AND `id_site`=".fn__get_site_id()."
          ".$sql_city.$sql_exclude.' 
          order by rand() LIMIT 0 , 3';
  $reader =Yii::app()->db->createCommand($sql)->query();   

          
  $ret='
<div class="bottom_list_dostoprim">
<table>
<tr>
';
  foreach ($reader as $row){
      $imgsrc=fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album']),'thumb_story_'.$row['id'],150,150);
      
    $ret.='
<td>
  <a href="/story/'.$row['id'].'" title="'.htmlspecialchars($row['name'], ENT_QUOTES).'">
    <img src="'.$imgsrc.'" alt="'.htmlspecialchars($row['name'], ENT_QUOTES).'">
    <div>
      '.$row['name'].'
    </div>
  </a>
</td>
    ';
  }
  $ret.='
</tr>
</table>
';

  if (fn__get_count_published_story($id_city,0)>3){
    $ret.='
      <div style="padding-top:10px; text-align:center;">
        <a class="btn btn-primary btn-small" style="color:#fff;" href="/story?city='.$id_city.'">
         Посмотреть остальные истории '.fn__get_padeg(fn__get_city_name($id_city),1).'
        </a>
      </div> 
    ';  
  }
          
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret; 
}
//******************************************************************************


































// Функция возвращает блок новостей на главной
//****************************************************************************** 
function fn__get_main_story_block(){ 

  //----------------------------------------------------
  $id_cache='fn__get_main_story_block_'.fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  


  $sql = "SELECT * FROM `xta_story` 
          WHERE `published` =1  AND `id_site`=".fn__get_site_id()."
          ORDER BY `xta_story`.`createdon` 
          DESC LIMIT 0 , 6";
  $reader =Yii::app()->db->createCommand($sql)->query();   
  $ret='
<div>
  <div>
    <div>
  ';        
  foreach ($reader as $row){
    $ret.='
<div style="border-bottom:1px #E5E5E5 solid; padding-top:5px;">
  <table style="height:12px;">
    <tr>
      <td style="width:70px; padding:1px; vertical-align:top;">
        <img src="'.fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album']),'thumb_story_'.$row['id'],50,50).'" alt="'.htmlspecialchars($row['name'], ENT_QUOTES).'" style="width:50px; height:50px;">
      </td>
      <td style=" padding-bottom:6px;">
        <table>
           <tr>
             <td style="padding-bottom:4px">
                <a href="/story/'.$row['id'].'" title="Читать полностью историю &quot;'.htmlspecialchars($row['name'], ENT_QUOTES).'&quot;" style="font-size:12px;">'.$row['name'].'</a>
             </td>
           </tr>
           <tr>
             <td style="width:120px; font-size:12px; text-align:left; padding-bottom:2px">
               Рассказывает '.$row['user_name'].' об отдыхе в <a href="/story?city='.$row['id_city'].'" title="Рассказы туристов о '.fn__get_padeg(fn__get_city_name($row['id_city']),5).'">'.fn__get_padeg(fn__get_city_name($row['id_city']),5).'</a>
             </td>
           </tr>
        </table>
      </td>
    </tr>
  </table>
</div>
    ';
  }
  $ret.='
</div>
</div>
</div>
  ';

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));
  return $ret;
}
//******************************************************************************





























// Функция возвращает корректен ли ID  истории
//****************************************************************************** 
function fn__send_story(){ 
  if (isset($_POST['formdata']['sendstory'])){
    $_name=htmlspecialchars(mb_substr($_POST['formdata']['name'],0,250,'utf-8'), ENT_QUOTES);
    $_about=strip_tags($_POST['formdata']['about'],'<b><p><br><ul><li><ol><i>');
    $_email=htmlspecialchars(mb_substr($_POST['formdata']['email'],0,250,'utf-8'), ENT_QUOTES);
    $_username=htmlspecialchars(mb_substr($_POST['formdata']['username'],0,250,'utf-8'), ENT_QUOTES);
    
    
    $_body='
<h1>'.$_name.'</h1>
<br><br>
<div>'.$_about.'</div>
<br><br><br>
<div>Email пользователя: '.$_email.'</h3>
<br>
<div>Имя пользователя: '.$_username.'</div>
<div>Сайт: '.fn__get_setting('site_name').'</div>
    
    ';
    
    
    mail(fn__get_setting('admin_email'), "На сайт ".fn__get_setting('site_name')." добавлена история", $_body); 
    
    return '
<div class="alert in alert-block fade alert-info">
  <a class="close" data-dismiss="alert">×</a>
  Ваш рассказ отправлен на рассмотрение и в течение суток будет опубликован на сайте
</div>';
  }else{
    return '

    
<script type="text/javascript" src="/themes/ukrturne/components/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		editor_selector : "mceEditor",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,image,cleanup",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
	});
</script>








<form method="POST" action="/story" name="formaddstory">
<div aria-hidden="true" style="display: none;" id="myModal" class="modal hide fade">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <div style="font-weight:bold; font-size:17px; color#000;">Напишите свою историю об отдыхе</div>
  </div>

  <div class="modal-body">
      <label><a onClick="alert(123); frmSubmit(); alert(465); ">Заголовок</a></label>
      <input name="formdata[name]" type="text" style="width:100%; height:25px;">
      
      <label>Ваш рассказ</label>
      <textarea name="formdata[about]" class="mceEditor" style="width:100%; height:240px;"></textarea>
      
      <table style="height:10px;">
        <tr>
          <td style="width:50%; padding:10px">
            <label>Ваше имя</label>
            <input name="formdata[username]" type="text" style="width:100%; height:25px;">
          </td>
          <td style="width:50%; padding:10px">
            <label>Ваш email</label>
            <input name="formdata[email]" type="text" style="width:100%; height:25px;">
          </td>
        <tr>
      </table>
      <input type="hidden" name="formdata[sendstory]">
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" type="submit">Отправить</button>
    <a data-dismiss="modal" class="btn" href="#">Закрыть</a>    
  </div>
</div>
</form>







<script language="javascript">
function frmSubmit(){
  alert("start");
  
  $.post(
         "/story", 
         $("#formaddstory").serialize(),
         function(data){
                        alert("sended");
                       },
         "json"
        );
  alert($("#formaddstory").serialize());
  alert("end");
   }
</script>







<div style="float:right; margin-top:-10px;">
  <a data-toggle="modal" title="Добавьте рассказ о вашем путешествии" data-target="#myModal" class="btn btn-primary">
    Добавить рассказ
  </a>
</div>
';
  }
}
//******************************************************************************










?>
