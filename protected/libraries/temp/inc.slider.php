<?php 




// Функция возвращает ID альбома слайдера (или создает пустой)
//****************************************************************************** 
function fn__get_slider_album($id_slider=0,$albumname=''){ 
  $albumname = mb_substr($albumname,0,100,'utf-8');
  $id_slider = intval($id_slider);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_slider` WHERE `id` = ".$id_slider;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $sql = "SELECT `id_album` FROM `xta_slider` WHERE `id` = ".$id_slider;
     $row1 =Yii::app()->db->createCommand($sql)->queryRow();
     $id_album=intval($row1['id_album']);
     
     $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id` = ".$id_album;
     $row1 =Yii::app()->db->createCommand($sql)->queryRow();
     if ($row1['count']){
       return $id_album;
     }else{
       $sql = "INSERT INTO `xta_albums`(`name`, `description`) VALUES ('".$albumname."','');";
       Yii::app()->db->createCommand($sql)->execute();
       $sql = "SELECT MAX(`id`) as 'max' FROM `xta_albums`";
       $row2 =Yii::app()->db->createCommand($sql)->queryRow();
       $id_album=intval($row2['max']);
       $sql = "UPDATE `xta_slider` SET `id_album`=".$row2['max']." WHERE `id`=".$id_slider;
       Yii::app()->db->createCommand($sql)->execute();
       return $id_album;
     }
  }
  return false;
}
//****************************************************************************** 










// Функция возвращает слайдер для главной
//****************************************************************************** 
function fn__get_main_slider(){ 
  //----------------------------------------------------
  $id_cache='fn__get_main_slider_'.fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
  $ret='
<div id="yw0" class="carousel slide" style="margin:0px; height:300px;">
  <div class="carousel-inner">
  ';
  $sql = "SELECT * FROM `xta_slider` WHERE `id_site`=".fn__get_site_id();
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $i=0;
  foreach ($reader as $row)    
    {
      $i++;
      $active='';
      if ($i==1){$active=' active';}
      $ret.='
      <div class="item'.$active.'">
        <a href="'.$row['link'].'">
        <img src="'.fn__get_album_mainimg_src($row['id_album'],false).'" alt="" />
        <div class="carousel-caption">
         <h4 style="font-size:14px;">'.$row['name'].'</h4>
         <p style="font-size:13px;">'.$row['description'].'</p>
        </div>
        </a>
      </div>
      ';
      
      
    }
  $ret.='
  </div>
<a class="carousel-control left" href="#yw0" data-slide="prev">&lsaquo;</a>
<a class="carousel-control right" href="#yw0" data-slide="next">&rsaquo;</a>
</div>
<script>
$(".carousel").carousel({
interval: 4000
})
</script>
  ';
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 








?>
