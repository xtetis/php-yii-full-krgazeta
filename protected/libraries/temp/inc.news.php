<?php 

// Функция возвращает корректен ли ID новости
// fn__is_correct_news_id($id=0,$cache_request=true)

// Функция возвращает имя категории по ID
// function fn__get_newscategory_name($id=0,$cache_request=false){ 

// Функция возвращает список новостей, которые выводятся в самой новости
// function fn__get_news_list_in_newsitem($id_city=0,$cache_request=true){ 

// Функция возвращает количество опубликованных новостей для фильтра по городу
//function fn__get_news_filter_city(){ 

// Функция возвращает количество опубликованных новостей
// function fn__get_count_published_news($id_city=0, $id_newscategory=0, $cache_request=false){ 

// Функция возвращает блок новостей на главной
// function fn__get_main_news_block($cache_request=true){ 


// Функция возвращает список новостей 
// function fn__get_news_list($page=1,$id=0,$id_newscategory=0){ 







// Функция возвращает корректен ли ID новости
// Входные параметры
// id - ID новости
//****************************************************************************** 
function fn__is_correct_news_id($id=0){ 
  $id = intval($id);
  
  //----------------------------------------------------
  $id_cache='fn__is_correct_news_id_'.$id."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
  $ret=false;
  $sql = "SELECT count(`id`) as 'count' 
          FROM `xta_news` 
          WHERE `id` = ".$id." AND `id_site`=".fn__get_site_id();
  $row =Yii::app()->db->createCommand($sql)->queryRow();   
  if ($row['count']){
       $ret=$id;
     }
  
    
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret;   
}
//******************************************************************************












// Функция возвращает имя категории по ID
//****************************************************************************** 
function fn__get_newscategory_name($id=0){ 

  $id=intval($id);
  
  //----------------------------------------------------
  $id_cache='fn__get_newscategory_name_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $sql = "SELECT `name` FROM `xta_newscategory` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();   
  $ret=$row['name']; 
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;  
}
//******************************************************************************
























// Функция возвращает список новостей, которые выводятся в самой новости
//****************************************************************************** 
function fn__get_news_list_in_newsitem($id_city=0){ 

  $id_city = intval($id_city);
  
  //----------------------------------------------------
  $id_cache='fn__get_news_list_in_newsitem_'.$id_city."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  
  $sql = "SELECT `id`,`id_album`,`name` FROM `xta_news` 
          WHERE `published` = 1 AND `id_city` = ".$id_city." 
          AND `id_site`=".fn__get_site_id()."
          LIMIT 0 , 11";
  $reader =Yii::app()->db->createCommand($sql)->query();   
  $i=0;      
  $ret.='<table style="width:1px; height:10px;"><tr>';  
  foreach ($reader as $row){        
    if ($i==6){$ret.='</tr><tr>';}
    $ret.='
          <td class="tpl_gr_incity_dostoprim_item">
            <a href="/news/'.$row['id'].'" class="main_vert_subitem_a">
               <div style="text-align:center;">
                 <img height="100" width="100" style="height:100px; width:100px;" src="'.fn__get_album_mainimg_src($row['id_album']).'" class="corner iradius15"> 
               </div> 
              <div class="main_vert_subitem_pagetitle">
                 '.mb_substr($row['name'],0,35,'utf-8').'...
               </div>  
            </a>
          </td>
    ';
    if ($i==10){
      $ret.='
        <td class="incity_dostoprim_item_next">
          <a href="/news?city='.$id_city.'" title="Посмотреть все туристические новости '.fn__get_padeg(fn__get_city_name($id_city),1).'">
           <div style="height:100px; width:100%;">&nbsp;</div></a></td>';
    }
    $i++;
  }
  $ret.='</tr></table>';
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret; 
}
//******************************************************************************




// Функция возвращает количество опубликованных новостей для фильтра по городу
// Входные параметры
// id_city - ID города (необязательный параметр)
//****************************************************************************** 
function fn__get_news_filter_city(){ 
  $id_city = intval($id_city);
  if ($id_city){
    $sql = "SELECT count(`id`) as 'count' FROM `xta_city` 
            WHERE `id` = ".$id." AND `id_site`=".fn__get_site_id()."";
    $row =Yii::app()->db->cache(1000000)->createCommand($sql)->queryRow();
    if (!$row['count']){
      return false;
    }
  }
  
  $sql = "SELECT count(`id`) as 'count' FROM `xta_news` WHERE `published` = 1";
  if ($id_city){
    $sql.=' AND `id_city` = '.$id_city;
  }
    
  $dependency = new CDbCacheDependency('SELECT max(`editedon`) FROM `xta_news`'); 
  $row =Yii::app()->db->cache(1000000,$dependency)->createCommand($sql)->queryRow();
  return $row['count'];
}
//******************************************************************************
























// Функция возвращает количество опубликованных новостей
// Входные параметры
// id_city - ID города (необязательный параметр)
//****************************************************************************** 
function fn__get_count_published_news($id_city=0, $id_newscategory=0){ 
  $id_city = intval($id_city);
  $id_newscategory = intval($id_newscategory);
  
  //----------------------------------------------------
  $id_cache='fn__get_count_published_news_'.$id_city."_".$id_newscategory;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------    
  
  
  $sql = "SELECT count(`id`) as 'count' FROM `xta_news` 
          WHERE `published` = 1 AND `id_site`=".fn__get_site_id();

  if ($id_city){
    $sql.=' AND `id_city` = '.$id_city;
  }
  
  if ($id_newscategory){
    $sql.=' AND `id_newscategory` = '.$id_newscategory;
  }  
    
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  $ret=$row['count'];

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;  
}
//******************************************************************************



























// Функция возвращает блок новостей на главной
//****************************************************************************** 
function fn__get_main_news_block(){ 

  //----------------------------------------------------
  $id_cache='fn__get_main_news_block_'.fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
 

  $sql = "SELECT `xta_news`.* FROM `xta_news` 
          WHERE `xta_news`.`published` =1 AND `id_site`=".fn__get_site_id()."
          ORDER BY `xta_news`.`createdon` DESC LIMIT 0 , 6";
  $reader =Yii::app()->db->createCommand($sql)->query();   
          
  $ret='
<!-- ************** fn__get_main_news_block ******************* -->
<div>
  <div>
    <div style="text-align:center; padding-top:3px;">
    <a class="btn btn-info" href="/news" style="width:200px; color:#fff;">Новости туризма</a>
    </div>  

    <div>
  ';        
  foreach ($reader as $row){
    $ret.='
<div style="border-bottom:1px #E5E5E5 solid; padding-top:5px;">
  <table style="height:2px;">
    <tr>
      <td style="width:70px; padding:1px; vertical-align:top;">
        <img src="'.fn__get_img_thumb(fn__correct_img_src(fn__get_album_mainimg_src($row['id_album'])),'news'.$row['id'],68,50).'" alt="'.htmlspecialchars($row['name'], ENT_QUOTES).'" style="width:68px; height:50px;">
      </td>
      <td style=" padding-bottom:6px;">
        <table>
           <tr>
             <td style="padding-bottom:4px">
                <a href="/news/'.$row['id'].'" title="'.htmlspecialchars($row['name'], ENT_QUOTES).'" style="font-size:12px;">'.$row['name'].'</a>
             </td>
           </tr>
           <tr>
             <td style="width:120px; font-size:12px; text-align:left; padding-bottom:2px">
               '.$row['createdon'].'
             </td>
           </tr>
           <tr>
             <td style="font-size:12px; text-align:left;">
               <i>Город:</i>
               <a href="/news?city='.$row['id_city'].'" title="Новости '.fn__get_padeg(fn__get_city_name($row['id_city']),1).'">
               '.fn__get_city_name($row['id_city']).'</a>
             </td>
           </tr>
        </table>
      </td>
    </tr>
  </table>
</div>
    ';
  }
  $ret.='
</div>
</div>
</div>
<!-- ************** /fn__get_main_news_block ******************* -->
  ';

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));
  
  return $ret;
}
//******************************************************************************































// Функция возвращает список новостей 
// Входные параметры
// id_city - Фильтр по городу (необязательный)
//****************************************************************************** 
function fn__get_news_list($page=1,$id_city=0,$id_newscategory=0){ 

  $id_city = intval($id_city);
  $id_newscategory = intval($id_newscategory);
  $page = intval($page);

  //----------------------------------------------------
  $id_cache='fn__get_news_list_'.fn__get_site_id()."_".$page."_".$id_city."_".$id_newscategory;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  
  if ($id_city){
    $cityfilter=' AND `xta_news`.`id_city` = '.$id_city." "; 
  } 
  
  if ($id_newscategory){
    $newscategory_filter=' AND `xta_news`.`id_newscategory` = '.intval($id_newscategory)." "; 
  }
  
  $sql = "SELECT 
          `xta_news`.`id`, 
          `xta_news`.`id_album`, 
          `xta_news`.`id_newscategory`, 
          `xta_news`.`name`, 
          `xta_news`.`id_city`,
          `xta_news`.`text`, 
          `xta_news`.`createdon`,
          `xta_city`.`name` as 'cityname',
          `xta_region`.`name` as 'regionname',
          `xta_newscategory`.`name` as 'newscategoryname'
          FROM `xta_news`
          LEFT JOIN `xta_city` on
             `xta_city`.`id` = `xta_news`.`id_city`
          LEFT JOIN `xta_region` on
             `xta_region`.`id` = `xta_city`.`region_id`
          LEFT JOIN `xta_newscategory` on
             `xta_newscategory`.`id` = `xta_news`.`id_newscategory`
          WHERE
            `published` = 1 
            AND `xta_news`.`id_site`=".fn__get_site_id()."
          ".$cityfilter.$newscategory_filter."  
          ORDER BY `xta_news`.`createdon` DESC   
          LIMIT ".(($page-1)*10)." , 10
          ";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row)
    {
   
      $imgsrc='/themes/ukrturne/img/ico/empty.gif';
      if (fn__get_album_main_img($row['id_album'],true)){
        $imgsrc=fn__get_album_mainimg_src($row['id_album'],true);
      } 
      
      $ret.='
<div style="padding-bottom:15px;">
  <table style="height:10px;">
    <tr> 
      <td style="width: 100px; vertical-align:top; padding: 5px;"> 
        <img src="'.fn__get_img_thumb($imgsrc,'news_'.$row['id'],100,100).'" alt="'.$row['name'].'" title="'.$row['name'].'" class="corner iradius15" style="width:100px; height:100px;">
      </td> 
      <td style="vertical-align:top; padding: 5px;">
        <div style="font-size:14px; font-weight:bold; text-align:left;"> 
          <a style="color: #111; font-weight:bold;" title="Нажмите, чтоб прочитать всю новость" href="/news/'.$row['id'].'"> 
            '.$row['name'].'
          </a>
        </div>
        <div style="text-align:left; margin-top:5px; font-size:12px; color: #222222;">
         '.mb_substr(strip_tags($row['text']),0,500,'utf-8').'...
        </div>
        </td>
    </tr>
    <tr>
      <td colspan="2">
      
      
        <table style="height:10px;">
         <tr>
          <td style="padding-right:10px;">
        <ul class="breadcrumbs breadcrumb" style="margin:0px; padding-top:2px; padding-bottom:2px;">
          <li>
            <a title="Прочитать новость полностью" href="/news/'.$row['id'].'" style="font-size:13px;">Читать полностью ...</a>
            <span class="divider">/</span>
          </li>
          <li>
            <a rel="tooltip" title="Показать все новости города &quot;'.$row['cityname'].'&quot;" href="/news?city='.$row['id_city'].'" style="font-size:13px;">'.$row['cityname'].'</a>
            <span class="divider">/</span>
          </li>
          <li>
            <a title="Показать все новости рубрики &quot;'.$row['newscategoryname'].'&quot;" href="/news?category='.$row['id_newscategory'].'" style="font-size:13px;">
              '.$row['newscategoryname'].'
            </a>
            <span class="divider">/</span>
          </li>
          <li class="active" style="font-size:13px;">
            '. date("d:m:Y",strtotime($row['createdon'])).'
          </li>
        </ul>
          </td>
          <td style="width:100px;">
          <a style="color:#fff; padding-left:15px; padding-right:15px;" href="/news/'.$row['id'].'" class="btn btn-primary btn-mini" title="'.$row['name'].' - прочитать подробнее">Подробнее</a>
          </td>
         </tr>
        </table>
        
        
        
        

        
      </td>
    </tr>
  </table>
  


</div>
      ';
    }
    
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret;      
}
//******************************************************************************

























// Функция возвращает список категорий новостей как <option>
//****************************************************************************** 
function fn__get_newscategory_options($id_default=0){
  $id_default = intval($id_default);
  
  $result='';
  $sql = "SELECT `id`,`name` FROM `xta_newscategory`";
  $reader =Yii::app()->db->cache(50000)->createCommand($sql)->query(); 
  foreach ($reader as $row)    
    {
      if ($id_default==intval($row['id']))
         {
           $result.='
<option value="'.$row['id'].'" selected="selected">'.$row['name'].'</option>';
         }else{
                $result.='
<option value="'.$row['id'].'">'.$row['name'].'</option>';
              }           
    }
  return $result; 
}
//****************************************************************************** 


















// Функция максимальный ID новости
//****************************************************************************** 
function fn__get_max_news(){
   $sql="SELECT MAX(`id`) as 'max' FROM `xta_news` WHERE `xta_news`.`id_site`=".fn__get_site_id()."";
   $row =Yii::app()->db->createCommand($sql)->queryRow(); 
   return $row['max'];
}
//****************************************************************************** 



















// Функция возвращает ID альбома новости (или создает пустой)
//****************************************************************************** 
function fn__get_news_album($id_news=0,$albumname=''){ 
  $albumname = mb_substr($albumname,0,100,'utf-8');
  $id_news = intval($id_news);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_news` WHERE `id` = ".$id_news;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $sql = "SELECT `id_album` FROM `xta_news` WHERE `id` = ".$id_news;
     $row1 =Yii::app()->db->createCommand($sql)->queryRow();
     $id_album=intval($row1['id_album']);
     
     $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id` = ".$id_album;
     $row1 =Yii::app()->db->createCommand($sql)->queryRow();
     if ($row1['count']){
       return $id_album;
     }else{
       $sql = "INSERT INTO `xta_albums`(`name`, `description`) VALUES ('".$albumname."','');";
       Yii::app()->db->createCommand($sql)->execute();
       $sql = "SELECT MAX(`id`) as 'max' FROM `xta_albums`";
       $row2 =Yii::app()->db->createCommand($sql)->queryRow();
       $id_album=intval($row2['max']);
       $sql = "UPDATE `xta_news` SET `id_album`=".$row2['max']." WHERE `id`=".$id_news;
       Yii::app()->db->createCommand($sql)->execute();
       return $id_album;
     }
  }
  return false;
}
//****************************************************************************** 













// Функция публикует и снимает с публикации новость
//****************************************************************************** 
function fn__publish_news($id=0,$flag=true){
  $flag=intval($flag);
  $id = intval($id);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_news` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $val=0;
     if ($flag){$val=1;};
     $sql = "UPDATE `xta_news` SET `published`=".$val." WHERE `id`=".$id;
     Yii::app()->db->createCommand($sql)->execute();
     return true;
   }
  return false;  
}
//****************************************************************************** 


























// Функция возвращает: опубликована ли новость
//****************************************************************************** 
function fn__is_news_published($id=0){
  $id = intval($id);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_news` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $sql = "SELECT `published` FROM `xta_news` WHERE `id` = ".$id;
     $row =Yii::app()->db->createCommand($sql)->queryRow();
     return intval($row['published']);
   }
  return false;  
}
//****************************************************************************** 













// Функция возвращает корректен ли ID категории новости
// Входные параметры
// id - ID города
//****************************************************************************** 
function fn__is_correct_newscategory_id($id=0){ 

  $id = intval($id);
  //----------------------------------------------------
  $id_cache='fn__is_correct_newscategory_id_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $ret=0;
  $sql = "SELECT count(`id`) as 'count' FROM `xta_newscategory` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();   
  if ($row['count']){
       $ret=$id;
     }
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret;   
}
//******************************************************************************















// Функция возвращает список категорий новостей для опубликованных новостей
//****************************************************************************** 
function fn__get_list_actual_newscategory($selected_category=0){ 

  //----------------------------------------------------
  $id_cache='fn__get_list_actual_newscategory_'.fn__get_site_id()."_".$selected_category;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  


  $selected_category = intval($selected_category);
  $sql = "SELECT `id`,`name` FROM `xta_newscategory` WHERE `id` in (
             SELECT DISTINCT `id_newscategory` 
             FROM `xta_news` 
             WHERE 
               `published` = 1
                AND 
                `xta_news`.`id_site`=".fn__get_site_id()."
          )";
  $reader =Yii::app()->db->createCommand($sql)->query();   
  foreach ($reader as $row){
    $style='';
    if ($selected_category==$row['id']){
         $style=' style="background-image: url(\'/themes/ukrturne/img/ico/bullet_go_red.png\');"';
       }
    $ret.=' 
    <li>
      <a title="Показать все новости рубрики &quot;'.$row['name'].'&quot;" href="/news?category='.$row['id'].'"'.$style.'>                                     
       '.$row['name'].'
      </a>
    </li>';
  }
  
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));
  return $ret; 
}
//******************************************************************************




?>
