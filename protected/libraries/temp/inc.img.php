<?php 




// Функция возвращает количество изображений в альбоме
//******************************************************************************
function fn__get_album_images_count($id_album=0){ 


  //----------------------------------------------------
  $id_cache='fn__get_album_images_count_'.$id_album;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $id_album = intval($id_album);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id` = ".$id_album;
  $row =Yii::app()->db->createCommand($sql)->queryRow();   

  if (!$row['count']){
        $ret= false;
     }else{
        $sql = "SELECT count(`id`) as 'count' FROM `xta_images` WHERE `id_album` = ".$id_album;
        $row =Yii::app()->db->createCommand($sql)->queryRow();   
        $ret=intval($row['count']);
     }
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;     
}
//******************************************************************************





















// Функция возвращает первые 4 изображения для галлереи и кнопку просмотреть весь альбом
// id_album
// imgalt  - какие ALT добавлять в картинки  (котография *кого-чего* # )
// linkmoretext  - какой текст для кнопки "Смотреть весь альбом"
// linkmorehref - ссылка на полный альбом
function fn__get_preview_for_album($id_album=0, $imgalt='',$linkmorehref='/',$linkmoretext='Посмотреть остальные фотографии'){ 

  $id_album = intval($id_album);
  
  //----------------------------------------------------
  $id_cache='fn__get_preview_for_album_'.$id_album."_".$imgalt."_".$linkmorehref."_".$linkmoretext;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
  $sql = "SELECT * FROM `xta_images` WHERE `id_album` = ".$id_album." LIMIT 0 , 4";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $i=0;
  $ret='
  <table style="height:10px;">
    <tr>
  ';
  foreach ($reader as $row){       
      $ret.='
        <td style="padding:5px; text-align:center;">
          <a class="grouped_elements" title="Фотография '.$imgalt.' '.$i.'" style="text-decoration:none;"  rel="group1" href="'.$row['src'].'">
             <img src="'.$row['src'].'" alt="Фотография '.$imgalt.' #'.$i.'" title="Фотография '.$imgalt.' #'.$i.'" class="imgalbumpreview">
          </a>   
        </td>
      ';
      if ($i==1){$ret.='</tr><tr>';}
      $i++;  
  }
  $ret.='
          </tr></table>  

        <script type="text/javascript">
          $(document).ready(function() {
            $("a.grouped_elements").fancybox({
                maxWidth    : 800,
                maxHeight   : 600,
                fitToView   : true,
                width       : \'70%\',
                height      : \'70%\',
                autoSize    : true,
                closeClick  : false,
                openEffect  : \'none\',
                closeEffect : \'none\',
               overlayShow :	true,
               cyclic      : true,
               titleShow   : true,
               titlePosition: \'over\',
               hideOnContentClick : true    
            });
          });
        </script>';
  
  if (fn__get_album_images_count($id_album)){
    $ret.='
      <div style="padding-top:5px; text-align:center;">
        <a class="btn btn-primary btn-small" style="color:#fff;" href="'.$linkmorehref.'">
          '.$linkmoretext.'</a>
      </div> 
    ';
  }
 
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;  
}
//******************************************************************************



























// Функция возвращает SRC уменьшенного изображения
//****************************************************************************** 
function fn__get_img_thumb($src='',$nameprefix='thumb',$width=1, $height=1,$defaultimg='themes/ukrturne/img/ico/empty.gif'){ 
  $width = intval($width);
  $height = intval($height);
  if (file_exists($src))
  {
    $newname = 'images/thumbs/'.$nameprefix.'_w'.strval($width).'_h'.strval($height).'_'.md5($src).'.jpg';
    if (file_exists($newname)){
      return $newname;
    }
	  $thumb=Yii::app()->phpThumb->create($src);
    
	  $thumb->resize($width,$height);
	  $thumb->save($newname);
    return $newname;
  }else{
    return fn__get_img_thumb($defaultimg,'thumb',$width,$height);
  }
}
//******************************************************************************


































// Функция возвращает изображения для галлереи
// id_album
// imgalt  - какие ALT добавлять в картинки  (котография *кого-чего* # )
function fn__get_album_html($id_album=0, $imgalt='',$candelete=false, $retpath=''){ 
  
  // Удаляем изображение
  //----------------------------------------------------------------------------
  if (($candelete) && (isset($_GET['delimg']))){
       $id_img = intval($_GET['delimg']);
       $sql = "SELECT count(`id`) as 'count' FROM `xta_images` WHERE `id_album` = ".$id_album." AND `id`=".$id_img;
       $row =Yii::app()->db->createCommand($sql)->queryRow(); 
       if ($row['count']){
            fn__del_img($id_img);
            Yii::app()->cache->flush();
            header("Location: ".$retpath);
            exit;
          }
     }
  
  //----------------------------------------------------------------------------

  


  //----------------------------------------------------
  $id_cache='fn__get_album_html_'.$id_album.$imgalt.$candelete.$retpath;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 



  $id_album = intval($id_album);
  $sql = "SELECT * FROM `xta_images` WHERE `id_album` = ".$id_album."";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $i=0;
  $ret='
  <table style="height:10px;">
    <tr>
  ';
  foreach ($reader as $row){  
  
    if ($candelete){
     $_candel='
     <div style="text-align:center; padding-top:2px;">
       <a href="'.$_SERVER['REQUEST_URI'].'?delimg='.$row["id"].'" onclick="return confirm(\'Вы уверены, что хотите удалить изображение?\');" title="Удалить изображение">
         <img src="/themes/ukrturne/img/ico/remove_1180.png" style="width:21px; height:21px;">
         Удалить изображение
       </a>  
     </div>';
    }
        
      $ret.='
<td style="padding:5px; text-align:center;">
  <a class="grouped_elements" title="Фотография '.$imgalt.' #'.$i.'" style="text-decoration:none;" rel="group1" href="'.fn__get_watermerked_img_src($row['src']).'">
     <img src="'.fn__get_watermerked_img_src($row['src']).'" alt="Фотография '.$imgalt.' #'.$i.'" title="Фотография '.$imgalt.' #'.$i.'" class="imgalbumpreview">
  </a>   
  '.$_candel.'
</td>
      ';
      if ($i%2==1){$ret.='</tr><tr>';}
      $i++;  
  }
  $ret.='
  </tr></table>  

<script type="text/javascript">
  $(document).ready(function() {
    $("a.grouped_elements").fancybox({
        maxWidth    : 800,
        maxHeight   : 600,
        fitToView   : true,
        width       : \'70%\',
        height      : \'70%\',
        autoSize    : true,
        closeClick  : false,
        openEffect  : \'none\',
        closeEffect : \'none\',
       overlayShow :	true,
       cyclic      : true,
       titleShow   : true,
       titlePosition: \'over\',
       hideOnContentClick : true    
    });
  });
</script>     
  
  ';
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;     
}
//******************************************************************************







































// Функция загружает изображение в альбом
//****************************************************************************** 
function fn__post_upload_img($album_id,$img_inputname,$alt=''){ 
   Yii::app()->cache->flush();

   $alt=mb_substr($alt,0,200,'utf-8');
   $album_id = intval($album_id);
   $uploaddir='images/gallery/'.$album_id.'/';
   
   if (!is_dir($uploaddir)){
     if (!mkdir($uploaddir)) {
       return false;
     }
   }
   

   $res=true;
   for ($i = 0; $i < count($_FILES[$img_inputname]['tmp_name']); $i++)
   {
       $apend=$album_id.'_'.strval(md5(date('YmdHis').rand(100,10000))).'.jpg'; 
       $uploadfile = $uploaddir.$apend; 
       
       if (($_FILES[$img_inputname]['size'][$i]!=0)&&($_FILES[$img_inputname]['size'][$i]<=1024000))
           {          
           if (move_uploaded_file($_FILES[$img_inputname]['tmp_name'][$i], $uploadfile))
              {          
                $size = getimagesize($uploadfile); 
                if ($size[0] < 1500 && $size[1]<1500)
                   {   
                    $sql = "INSERT INTO `xta_images` (`id_album`, `src`, `alt`) 
                            VALUES (".$album_id.",'".$uploadfile."','".$alt."')";     
                    Yii::app()->db->createCommand($sql)->execute();
                   
                     $sql = "SELECT count(`id`) as 'count' 
                             FROM `xta_images` WHERE `id` in
                             (
                               SELECT `id_mainimg` 
                               FROM `xta_albums` WHERE `id` = ".$album_id."
                             )";
                     $row2 =Yii::app()->db->createCommand($sql)->queryRow(); 
                     if (!intval($row2['count']))
                        {
                          $sql = "SELECT MAX(`id`) as 'max' 
                                  FROM `xta_images` WHERE `id_album` =".$album_id;
                          $row2 =Yii::app()->db->createCommand($sql)->queryRow(); 
                       
                          $sql = "UPDATE `xta_albums` 
                                  SET `id_mainimg`= ".intval($row2['max'])." 
                                  WHERE `id` = ".$album_id;
                          Yii::app()->db->createCommand($sql)->execute(); 
                        }
                     $res = $res &&  true;
                  } else {
                          $res = false;
                          unlink($uploadfile);
                         }
             }else{
                    $res = false;
                  }
          }else{
                 $res = false;
               }
   }    
   return $res;
}
//****************************************************************************** 










































// Функция возвращает HTML альбома в админке
//****************************************************************************** 
function fn__get_admin_album($id=0){ 

  
  
  $id = intval($id);
  $rurl=$_SERVER['REQUEST_URI'];
  $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id` = ".intval($id);
  $row =Yii::app()->db->cache(50000)->createCommand($sql)->queryRow(); 
  if ($row['count']){ 
  
     file_put_contents ('protected/libraries/albumimgdir/'.md5($_SERVER['SERVER_NAME'].$_SERVER['REMOTE_ADDR']).".txt" ,'/images/gallery/'.$id);     
  
     $sql = "SELECT `id_mainimg` FROM `xta_albums` WHERE `id` = ".intval($id);
     $row =Yii::app()->db->createCommand($sql)->queryRow(); 
     $mainimg = intval($row['id_mainimg']);
     
     $result='<table style="height:10px;"><tr>';

     $sql="SELECT `id`, `src` FROM `xta_images` WHERE `id_album` = ".$id." ORDER BY `id` DESC";
     $reader =Yii::app()->db->createCommand($sql)->query(); 
     $i=1;
     foreach ($reader as $row2){
         $_type="primary";
         $_title_anchor="Сделать титулом";
         $_hrefdef='<a style="color:#fff;" 
                       href="'.$rurl."&def=".$row2['id'].'" 
                       class="btn btn-'.$_type.' btn-mini">
                       '.$_title_anchor.'</a>';
         $_hrefdel='<a style="color:#fff;" 
                       href="'.$rurl."&del=".$row2['id'].'" 
                       class="btn btn-danger btn-mini" 
                       onClick="return confirm(\'Вы хотите удалить изображение?\')">Удалить</a>';

         if ($mainimg==intval($row2['id'])){
              $_type="success";
              $_title_anchor="Титульное изображение";
              $_hrefdef='<a style="color:#fff;" class="btn btn-success btn-mini">Заглавная</a>';
            }
         $result.='
                    <td style="padding-top:10px;">
                     <div style="width:200px;">
                       <a href="#" class="thumbnail" rel="tooltip" 
                         data-title="'.$row2['id'].'" 
                         style="max-width:200px; max-height:200px;">
                         <img src="/'.$row2['src'].'" style="max-width:190px; max-height:190px;">
                       </a>
                     </div>
                     <div style="padding:5px; text-align:center; width:200px;">
                       '.$_hrefdef.'
                       '.$_hrefdel.'
                     </div>
                   </td>';
         if ($i%4==0)
            {
               $result.='</tr><tr>';
            }
         $i++;
       }  
       $result.='</tr></table>';
       return $result;
  }
  return false;
}
//****************************************************************************** 











// Функция устанавливает изображение, главным для альбома
//****************************************************************************** 
function fn__set_main_img_to_album($id_img=0,$id_album=0){ 
   $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id` = ".intval($id_album);
   $row =Yii::app()->db->cache(50000)->createCommand($sql)->queryRow(); 
   if ($row['count']){
     $sql = "SELECT count(`id`) as 'count' FROM `xta_images` WHERE `id` = ".intval($id_img);
     $row1 =Yii::app()->db->cache(50000)->createCommand($sql)->queryRow();
     if ($row1['count']){
       $sql = "SELECT count(`id`) as 'count' 
               FROM `xta_images` 
               WHERE `id` = ".intval($id_img)." and `id_album` = ".intval($id_album);  
       $row2 =Yii::app()->db->cache(50000)->createCommand($sql)->queryRow(); 
       if ($row2['count']){
          $sql = "UPDATE `xta_albums` 
                  SET `id_mainimg`=".intval($id_img)." 
                  WHERE `id` = ".intval($id_album);
          Yii::app()->db->createCommand($sql)->execute();  
          return true;          
       }    
       return false;  
     }
     return false;
   }
   return false;
}
//****************************************************************************** 








// Функция проверяет существование в изображения
//****************************************************************************** 
// Первый параметр - ID изображения, второй параметр - необходимость проверки
// существования файла
function fn__image_exists($id_img=0,$file_exists=false){ 
  $sql = "SELECT count(`id`) as 'count' FROM `xta_images` WHERE `id` = ".intval($id_img);
  $row1 =Yii::app()->db->cache(50000)->createCommand($sql)->queryRow();
  if ($row1['count']){
    return true; 
  }
  return false;
}
//****************************************************************************** 













// Функция возвращает ID главного изображения альбома
//****************************************************************************** 
function fn__get_album_main_img($id_album=0,$cache_request=false){ 

  //----------------------------------------------------
  $id_cache='fn__get_album_main_img_'.$id_album.$cache_request;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $id_album = intval($id_album);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id` = ".intval($id_album);
  $row =Yii::app()->db->createCommand($sql)->queryRow();    

  if ($row['count']){
      $sql = "SELECT `id_mainimg` FROM `xta_albums` WHERE `id` = ".intval($id_album);
      $row1 =Yii::app()->db->createCommand($sql)->queryRow();    
      $id_main_img=intval($row1['id_mainimg']);
      
      $sql = "SELECT count(`id`) as 'count' FROM `xta_images` WHERE `id` = ".intval($id_main_img);
      $row1 =Yii::app()->db->createCommand($sql)->queryRow();    
      if ($row1['count']){
         $ret=$id_main_img; 
         

      }else{
         $sql = "SELECT count(`id`) as 'count' FROM `xta_images` WHERE `id_album` = ".$id_album;
         $row =Yii::app()->db->createCommand($sql)->queryRow();    
         if ($row['count']){
           $sql = "SELECT `id` FROM `xta_images` WHERE `id_album` = ".$id_album." LIMIT 0,1";
           $row =Yii::app()->db->createCommand($sql)->queryRow();    
           $id_img = intval($row['id']);
               
           $sql = "UPDATE `xta_albums` SET `id_mainimg`=".$id_img." WHERE `id`=".$id_album;
           Yii::app()->db->createCommand($sql)->execute();  
           $ret= $id_img;
         }else{
           $sql = "UPDATE `xta_albums` SET `id_mainimg`=0 WHERE `id`=".intval($id_album);
           Yii::app()->db->createCommand($sql)->execute();
           $ret= false;
         }
      }
   }
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;   
}
//****************************************************************************** 

















// Функция возвращает главное изображение альбома или false
// Входные параметры
// id - ID альбома
//****************************************************************************** 
function fn__get_album_mainimg_src($id=0,$watermarked=true){ 

  //----------------------------------------------------
  $id_cache='fn__get_album_mainimg_src_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

   $id = intval($id);
   $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id` = ".$id;
   $row1 =Yii::app()->db->createCommand($sql)->queryRow();

   if ($row1['count']){
       $sql = "SELECT `id_mainimg` FROM `xta_albums` WHERE `id` = ".$id;
       $row =Yii::app()->db->createCommand($sql)->queryRow();
       $id_mainimg = $row['id_mainimg'];
   
       $sql = "SELECT count(`id`) as 'count' 
       FROM `xta_images` WHERE `id`= ".$id_mainimg;
       $row =Yii::app()->db->createCommand($sql)->queryRow();   
       
       if ($row['count'])
          {
            $sql = "SELECT `src` FROM `xta_images` WHERE `id`= ".$id_mainimg;
            $row =Yii::app()->db->createCommand($sql)->queryRow();  
            $ret=$row['src'];
            
            if ($watermarked){
              $ret = fn__get_watermerked_img_src($ret);
            }
            

          }else{
                 $sql = "SELECT count(`id`) as 'count' 
                         FROM `xta_images` WHERE `id_album`= ".$id;
                 $row =Yii::app()->db->createCommand($sql)->queryRow();
                 if ($row['count']){
                      $sql = "SELECT `id` FROM `xta_images` WHERE `id_album`=".$id." LIMIT 0,1";
                      $row =Yii::app()->db->createCommand($sql)->queryRow();
                      $sql = "UPDATE `xta_albums` SET `id_mainimg`=".$row['id']." WHERE `id`=".$id;
                      Yii::app()->db->createCommand($sql)->execute(); 
                      $ret=fn__get_album_mainimg_src($id);
                    }else{
                      $ret= false;
                    }
               }
     }else{   
       $ret=false;
     }
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;       
}
//****************************************************************************** 

































// Функция удаляет изображение
//****************************************************************************** 
function fn__del_img($id_img=0){ 
   $sql = "SELECT count(`id`) as 'count' FROM `xta_images` WHERE `id` = ".intval($id_img);
   $row =Yii::app()->db->createCommand($sql)->queryRow(); 
   if ($row['count']){
      
      $sql = "SELECT `src` FROM `xta_images` WHERE `id` = ".intval($id_img);
      $row1 =Yii::app()->db->createCommand($sql)->queryRow(); 
      $src=$row1['src'];
      
      if (!file_exists($src)) {
          return false;
         }
      
      if (unlink($src)){
        $sql = "DELETE FROM `xta_images` WHERE `id`=".intval($id_img);
        Yii::app()->db->createCommand($sql)->execute();
        
        $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id_mainimg`=".intval($id_img);
        $row1 =Yii::app()->db->createCommand($sql)->queryRow(); 
        if ($row1['count']){
          $sql = "SELECT `id` FROM `xta_albums` WHERE `id_mainimg`=".intval($id_img);
          $row1 =Yii::app()->db->createCommand($sql)->queryRow(); 
          $album_id=intval($row1['id']);
          
          $sql = "SELECT count(`id`) as 'count' FROM `xta_images` WHERE `id_album` =".$album_id;
          $row1 =Yii::app()->db->createCommand($sql)->queryRow();
          if ($row1['count']){
            $sql = "SELECT MAX(`id`) as 'id' FROM `xta_images` WHERE `id_album` = ".$album_id;
            $row1 =Yii::app()->db->createCommand($sql)->queryRow();
            
            $sql = "UPDATE `xta_albums` SET `id_mainimg`=".$row1['id']." WHERE `id` = ".$album_id;
            Yii::app()->db->createCommand($sql)->execute();
            return true; 
          }else{
            $sql = "UPDATE `xta_albums` SET `id_mainimg`=0 WHERE `id` = ".$album_id;
            Yii::app()->db->createCommand($sql)->execute();
            return true; 
          }
        }
        return true; 
      }
      return false;
   }
  return false;
}
//****************************************************************************** 



















// Функция возврашает watermark изображение по его оригинальному
//****************************************************************************** 
function fn__get_watermerked_img_src($original_src='',$watermark_img=''){ 
$original_img=$_SERVER['DOCUMENT_ROOT'].'/'.$original_src;
$watermarked_img=$_SERVER['DOCUMENT_ROOT'].'/images/watermarked/wm_'.md5($original_img).'.jpg';
$otnosit_watermarked_img='images/watermarked/wm_'.md5($original_img).'.jpg';
if (file_exists($watermarked_img)){
  return $otnosit_watermarked_img;
}

if (!file_exists($original_img)) {
    return $original_src;
   }else{
    $watermark_img=$_SERVER['DOCUMENT_ROOT'].fn__get_setting('watermark_img_path');
    
    
    
    
    Yii::app()->ih->load($original_img)->watermark($watermark_img, 0,0,CImageHandler::CORNER_CENTER, 0.8)->save($watermarked_img);     
    return $otnosit_watermarked_img;
   }
}
//****************************************************************************** 











// Функция удаляет альбом со всеми изображениям 
// $id - ID альбома, который нужно удалить
//****************************************************************************** 
function fn__del_album($id){ 
  $id = intval($id);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_images` WHERE `id_album` = ".$id;
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row){
    fn__del_img($row['id']);
  }
  
  $sql = "DELETE FROM `xta_albums` WHERE `id` = ".$id;
  Yii::app()->db->createCommand($sql)->execute();
  return true;
}
//****************************************************************************** 





// Функция создает изображение из текста (создано для генерации телефона)
//****************************************************************************** 
function fn__get_text_img_src($text='Testing...',$width=180,$height=25,$font_size=14){ 

$filename=md5($text."_".$width."_".$height."_".$font_size).'.png';
if (file_exists($_SERVER['DOCUMENT_ROOT'].'/images/textpng/'.$filename)){
  return '/images/textpng/'.$filename;
}

// Create the image
$im = imagecreatetruecolor($width, $height);

// Create some colors
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);
imagefilledrectangle($im, 0, 0, $width-1, $height-1, $white);

// Replace path by your own font path
$font = $_SERVER['DOCUMENT_ROOT'].'/protected/libraries/fonts/arial.ttf';

// Add some shadow to the text
//imagettftext($im, 20, 0, 11, 21, $grey, $font, $text);

// Add the text
imagettftext($im, $font_size, 0, 5, 20, $black, $font, $text);

// Using imagepng() results in clearer text compared with imagejpeg()
imagepng($im,$_SERVER['DOCUMENT_ROOT'].'/images/textpng/'.$filename);
imagedestroy($im);
return '/images/textpng/'.$filename;
}
//****************************************************************************** 








?>
