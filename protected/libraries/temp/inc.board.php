<?php 




// Функция возвращает список объявлений на главной странице
//****************************************************************************** 
function fn__main_obj_list_block($page=0,$count=10,$filter_city=0,$filter_type_house=0,$filter_type_obj=0,$sortby='timestamp', $sortdir='DESC',$main=true,$cache_request=true){ 
  
  //----------------------------------------------------  
  $id_cache='fn__main_obj_list_block_'.$page."_".$count."_".$filter_city."_".$filter_type_house."_".$filter_type_obj."_".$sortby."_".$sortdir."_".$main."_".$cache_request."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------      
  
  $ret='';
  
  $filter_type_obj=intval($filter_type_obj);
  $filter_type_house=intval($filter_type_house);
  $filter_city=intval($filter_city);
  $page=intval($page);
  $count=intval($count);



  if ($_sortdir==' ASC'){
      }else{
            $_sortdir=' DESC';
           }     



  
  
  //------------------------------------------------------------------------            
  //Получаем количество материалов
   $where_i_type_obj = ''; 
   if ($filter_type_obj){
       $where_i_type_obj = ' AND `xta_obj`.`type_obj` = '.$filter_type_obj;
      }            
   //.......................................................................               
   $where_i_tipe_house = ''; 
   if ($filter_type_house){
       $where_i_tipe_house = ' AND `type_house` = '.$filter_type_house;
      }
   //.......................................................................                 
   $where_i_id_city = '';
   if ($filter_city){
       $where_i_id_city = ' AND `id_city` = '.$filter_city;
      }
   //.......................................................................
   $_sortby=' ORDER BY `timestamp` ';
   if ($sortby=='price'){
       $_sortby=' ORDER BY `price` ';
      }
   //.......................................................................
   $_sortby.=$_sortdir;
   
   

$sql="
      SELECT `xta_obj`.*, 
             `xta_valuta`.`name` as 'valutaname' ,
             `xta_city`.`name` as 'cityname',
             `xta_type_house`.`name` as 'typehousename',
             `xta_type_obj`.`name` as 'typeobjname',
             `xta_payby`.`name` as 'paybyname',
             `xta_pay_period`.`name` as 'payperiodname',
             `xta_pay_period`.`shortname` as 'payperiodshortname'
      FROM `xta_obj` 
      LEFT JOIN 
           `xta_valuta` 
           on 
           `xta_valuta`.`id` = `xta_obj`.`price_valut`
      LEFT JOIN
           `xta_city`     
           on
           `xta_city`.`id` = `xta_obj`.`id_city`
      LEFT JOIN
           `xta_type_house`     
           on
           `xta_type_house`.`id` = `xta_obj`.`type_house`  
      LEFT JOIN
           `xta_type_obj`     
           on
           `xta_type_obj`.`id` = `xta_obj`.`type_obj`   
      LEFT JOIN
           `xta_pay_period`     
           on
           `xta_pay_period`.`id` = `xta_obj`.`id_pay_pariod`                        
      LEFT JOIN
           `xta_payby`     
           on
           `xta_payby`.`id` = `xta_obj`.`price_comment`  
      WHERE 1=1  AND `xta_obj`.`id_site`=".fn__get_site_id()."
      ".$where_i_tipe_house.$where_i_id_city.$where_i_type_obj.$_sortby." LIMIT ".(($page-1)*10)." , ".$count;
      
  $reader =Yii::app()->db->createCommand($sql)->query();   
  foreach ($reader as $row){        
     $tpl = file_get_contents('protected/views/chunks/board/tpl_main_board_list_item.php');
     $tpl = str_replace('[[+name]]',$row['name'],$tpl);
     $tpl = str_replace('[[+user_name]]',$row['user_name'],$tpl);
     $tpl = str_replace('[[+id]]',$row['id'],$tpl);
     $tpl = str_replace('[[+timestamp]]',$row['timestamp'],$tpl);
     $tpl = str_replace('[[+cityname]]',$row['cityname'],$tpl);
     $tpl = str_replace('[[+cityname_padeg_1]]',fn__get_padeg($row['cityname'],1),$tpl);
     $tpl = str_replace('[[+payperiodname]]',$row['payperiodname'],$tpl);
     $tpl = str_replace('[[+id_pay_pariod]]',$row['id_pay_pariod'],$tpl);
     $tpl = str_replace('[[+payperiodshortname]]',$row['payperiodshortname'],$tpl);
     $tpl = str_replace('[[+payperiodshortname_first]]',mb_ucfirst($row['payperiodshortname'],'utf-8'),$tpl);
     
     
     
     $tpl = str_replace('[[+cityname_padeg_5]]',fn__get_padeg($row['cityname'],5),$tpl);
     $tpl = str_replace('[[+typehousename_padeg_3]]',mb_strtolower(fn__get_padeg($row['typehousename'],3),'utf-8'),$tpl);
     $tpl = str_replace('[[+id_city]]',$row['id_city'],$tpl);
     if ($main){
     $tpl = str_replace('[[+text]]',mb_substr(strip_tags($row['text']),0,250,'UTF-8').'...',$tpl);      
        }else{
     $tpl = str_replace('[[+text]]',mb_substr(strip_tags($row['text']),0,500,'UTF-8').'...',$tpl);        
        }

     $tpl = str_replace('[[+type_house]]',$row['type_house'],$tpl);
     $tpl = str_replace('[[+typehousename]]',$row['typehousename'],$tpl);
     $tpl = str_replace('[[+typeobjname]]',$row['typeobjname'],$tpl);
     $tpl = str_replace('[[+type_obj]]',$row['type_obj'],$tpl);
           
     $_price='';
     if (intval($row['price'])){               
       $_price='               
          <div style="text-align:center;">
          <span style="color:#007F00; font-weight:bold; font-size:20px;">
          '.intval($row['price']).' '.$row['valutaname'].'
          </span>
          <br>
          <span style="">
          '.$row['paybyname'].'
          </span>  </div>                 
       ';
       }else{
          $_price='<div style="text-align:center; color: #669999; font-weight:bold;">Цена <br> договорная</div>';
            }
     $tpl = str_replace('[[+price]]',$_price,$tpl);

     $tpl = str_replace('[[+imgsrc]]',fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album'],true),'thumb_obj_id_'.$row['id'],110,110),$tpl);     

           
     $ret.=$tpl;
  }          
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  
  return $ret;
}
//******************************************************************************

































//******************************************************************************   
function fn__get_typehouse_name($id=0){   

  //----------------------------------------------------
  $id_cache='fn__get_typehouse_name_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
  
  $id = intval($id);
  
  $sql = "SELECT count(`id`) as 'count' FROM `xta_type_house` WHERE `id` = ".$id;
  $row=Yii::app()->db->createCommand($sql)->queryRow();
  if (!intval($row['count'])){
       $ret=false;	  
     }else{
       $sql = "SELECT `name` FROM `xta_type_house` WHERE `id` = ".$id;
       $row=Yii::app()->db->createCommand($sql)->queryRow();
       $ret=$row['name'];
     }  
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 

  return $ret;
}
//******************************************************************************   
   
  



















// Футкция возвращает имя типа объявления
//****************************************************************************** 
function fn__get_typeobj_name($id=0){

  //----------------------------------------------------
  $id_cache='fn__get_typeobj_name_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
     $id = intval($id);
     $sql="SELECT count(`id`) as 'count' FROM `xta_type_obj` WHERE `id` = ".$id;
     $row=Yii::app()->db->cache(1000000)->createCommand($sql)->queryRow();
     if (!intval($row['count'])){
          $ret=false;	  
        }else{
               $sql="SELECT `name` FROM `xta_type_obj` WHERE `id` = ".$id;
               $row=Yii::app()->db->cache(1000000)->createCommand($sql)->queryRow();
               $ret=$row['name'];
     }  
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;      
}
//******************************************************************************  

 






















// Футкция возвращает имя типа жилья
//****************************************************************************** 
function fn__get_type_house_name($id=0){

  //----------------------------------------------------
  $id_cache='fn__get_type_house_name_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
     $id = intval($id);
     $sql="SELECT count(`id`) as 'count' FROM `xta_type_house` WHERE `id` = ".$id;
     $row=Yii::app()->db->cache(10000000)->createCommand($sql)->queryRow();
     if (!intval($row['count'])){
          $ret=false;	  
        }else{
               $sql="SELECT `name` FROM `xta_type_house` WHERE `id` = ".$id;
               $row=Yii::app()->db->cache(10000000)->createCommand($sql)->queryRow();
               $ret=$row['name'];
     }  
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;      
}
//******************************************************************************  

  











// Функция возвращает список ID регионов для списка городов
//****************************************************************************** 
function fn__get_board_typehpuse_list($cache_request=true){ 

  //----------------------------------------------------
  $id_cache='fn__get_board_typehpuse_list_'.$cache_request;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 
  
  $sql = "SELECT `id`,`name` FROM `xta_type_house`";
  if ($cache_request){
       $reader =Yii::app()->db->cache(10000000)->createCommand($sql)->query();   
     }else{
           $reader =Yii::app()->db->createCommand($sql)->query();   
          }
  foreach ($reader as $row){        
    $ret.='<li><a href="/board?house='.$row['id'].'">'.$row['name'].'</a></li>';
  }

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret; 

}
//******************************************************************************


























// Функция возвращает список типов жилья
//****************************************************************************** 
function fn__get_type_house_options(){ 

  //----------------------------------------------------
  $id_cache='fn__get_type_house_options';
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 
  
  $sql = "SELECT `id`,`name` FROM `xta_type_house`";
  $reader =Yii::app()->db->createCommand($sql)->query();   
  foreach ($reader as $row){        
    $ret.='<option style="padding-left: 10px;" value="'.$row['id'].'">'.$row['name'].'</option>';
  }

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret; 

}
//******************************************************************************


























// Функция возвращает список типов жилья
//****************************************************************************** 
function fn__get_type_obj_options(){ 

  //----------------------------------------------------
  $id_cache='fn__get_type_obj_options';
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 
  
  $sql = "SELECT `id`,`name` FROM `xta_type_obj`";
  $reader =Yii::app()->db->createCommand($sql)->query();   
  foreach ($reader as $row){        
    $ret.='<option style="padding-left: 10px;" value="'.$row['id'].'">'.$row['name'].'</option>';
  }

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret; 

}
//******************************************************************************





















// Футкция возвращает имя типа жилья
//****************************************************************************** 
function fn__get_type_obj_name($id=0){

  //----------------------------------------------------
  $id_cache='fn__get_type_obj_name_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
     $id = intval($id);
     $sql="SELECT count(`id`) as 'count' FROM `xta_type_obj` WHERE `id` = ".$id;
     $row=Yii::app()->db->cache(10000000)->createCommand($sql)->queryRow();
     if (!intval($row['count'])){
          $ret=false;	  
        }else{
               $sql="SELECT `name` FROM `xta_type_obj` WHERE `id` = ".$id;
               $row=Yii::app()->db->cache(10000000)->createCommand($sql)->queryRow();
               $ret=$row['name'];
     }  
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;      
}
//******************************************************************************  




















// Футкция возвращает количество объявлений по фильтру
//****************************************************************************** 
function fn__get_board_obj_count($obj=0,$house=0,$city=0,$period=0){

  //----------------------------------------------------
  $id_cache='fn__get_board_obj_count_'.$obj."_".$house."_".$city."_".$period."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $obj=intval($obj);
  $house=intval($house);
  $city=intval($city);


  if ($obj){
     $where_obj = ' AND `xta_obj`.`type_obj` = '.$obj;
    }            
  if ($house){
     $where_house = ' AND `type_house` = '.$house;
    }
  if ($city){
     $where_city = ' AND `id_city` = '.$city;
    }
  if ($period){
     $where_period = ' AND `id_pay_pariod` = '.$period;
    }

  $sql = "SELECT count(`id`) as 'count' FROM `xta_obj` WHERE 1=1 
          AND `xta_obj`.`id_site`=".fn__get_site_id()."
          ".$where_house.$where_city.$where_obj.$where_period;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  $ret = $row['count'];
  
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;      
}
//******************************************************************************  
  































// Футкция возвращает список объявлений по фильтру
//****************************************************************************** 
function fn__get_board_obj_list($page=1,$obj=0,$house=0,$city=0,$sortby='timestamp',$sortdir='DESC',$period=0){

  //----------------------------------------------------
  $id_cache='fn__get_board_obj_list_'.$page."_".$obj."_".$house."_".$city."_".$sortby."_".$sortdir."_".$period."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  


  $page=intval($page);
  $obj=intval($obj);
  $house=intval($house);
  $city=intval($city);
  $period=intval($period);
  

  if ($obj){
     $where_obj = ' AND `xta_obj`.`type_obj` = '.$obj;
    }            
  if ($house){
     $where_house = ' AND `type_house` = '.$house;
    }
  if ($city){
     $where_city = ' AND `id_city` = '.$city;
    }
  if ($period){
     $where_period = ' AND `id_pay_pariod` = '.$period;
    }
  $sortby=' ORDER BY `'.$sortby.'` '.$sortdir;  
    
    
  $sql = "
  SELECT 
    `xta_obj`.*, 
    `xta_valuta`.`name` as 'valutaname' ,
    `xta_city`.`name` as 'cityname',
    `xta_type_house`.`name` as 'typehousename',
    `xta_type_obj`.`name` as 'typeobjname',
    `xta_payby`.`name` as 'paybyname',
    `xta_pay_period`.`name` as 'payperiodname',
    `xta_pay_period`.`shortname` as 'payperiodshortname'
  FROM `xta_obj` 
  LEFT JOIN 
    `xta_valuta` on 
    `xta_valuta`.`id` = `xta_obj`.`price_valut`
  LEFT JOIN
    `xta_city` on
    `xta_city`.`id` = `xta_obj`.`id_city`
  LEFT JOIN
    `xta_type_house` on
    `xta_type_house`.`id` = `xta_obj`.`type_house`  
  LEFT JOIN
    `xta_type_obj` on
    `xta_type_obj`.`id` = `xta_obj`.`type_obj`
  LEFT JOIN
    `xta_pay_period` on
    `xta_pay_period`.`id` = `xta_obj`.`id_pay_pariod`
  LEFT JOIN
    `xta_payby` on
    `xta_payby`.`id` = `xta_obj`.`price_comment`    
  WHERE 
    1=1 
     AND `xta_obj`.`id_site`=".fn__get_site_id()."
    ".$where_house.$where_city.$where_obj.$where_period.$sortby." 
  LIMIT ".(($page-1)*10)." , 10";

  $reader =Yii::app()->db->createCommand($sql)->query();     
    
$i=0;
foreach ($reader as $row){
$i++;
  $tpl = file_get_contents('protected/views/chunks/board/tpl_board_list_item.php');
  $tpl = str_replace('[[+name]]',$row['name'],$tpl);
  $tpl = str_replace('[[+payperiodshortname]]',$row['payperiodshortname'],$tpl);
  $tpl = str_replace('[[+user_name]]',$row['user_name'],$tpl);
  $tpl = str_replace('[[+id]]',$row['id'],$tpl);
  $tpl = str_replace('[[+timestamp]]',$row['timestamp'],$tpl);
  $tpl = str_replace('[[+cityname]]',$row['cityname'],$tpl);
  $tpl = str_replace('[[+id_city]]',$row['id_city'],$tpl);
  $tpl = str_replace('[[+text]]',mb_substr(strip_tags($row['text']),0,400,'UTF-8').'...',$tpl);
  $tpl = str_replace('[[+type_house]]',$row['type_house'],$tpl);
  $tpl = str_replace('[[+typehousename]]',$row['typehousename'],$tpl);
  $tpl = str_replace('[[+typeobjname]]',$row['typeobjname'],$tpl);
  $tpl = str_replace('[[+type_obj]]',$row['type_obj'],$tpl);
  
  $tpl = str_replace('[[+cityname_padeg_1]]',fn__get_padeg($row['cityname'],1),$tpl);
  $tpl = str_replace('[[+cityname_padeg_5]]',fn__get_padeg($row['cityname'],5),$tpl);
  $tpl = str_replace('[[+typehousename_padeg_3]]',mb_strtolower(fn__get_padeg($row['typehousename'],3),'utf-8'),$tpl);
  
  
   
  if (intval($row['price'])){               
    $_price='               
      <div style="text-align:center;">
        <span style="color:#007F00; font-weight:bold; font-size:20px;">
         '.intval($row['price']).' '.$row['valutaname'].'
        </span>
        <br>
        <span style="">
          За '.$row['paybyname'].' в '.$row['payperiodname'].'
        </span>  
      </div>                 
       ';
       $tpl = str_replace('[[+periodinlist]]',' <a href="/board?city='.$row['id_city'].'&period='.$row['id_pay_pariod'].'">'.$row['payperiodshortname'].'</a>',$tpl);
       
      }else{
      $_price='<div style="text-align:center; color: #669999; font-weight:bold;">
                Цена <br> договорная
               </div>';
           }
  $_price = str_replace('[[++human++]]','человека',$_price);
  $_price = str_replace('[[++object++]]',mb_strtolower(fn__get_padeg($row['typehousename'],3),'utf-8'),$_price);
  
  $tpl = str_replace('[[+price]]',$_price,$tpl);
  $tpl = str_replace('[[+periodinlist]]','',$tpl);

  $tpl = str_replace('[[+imgsrc]]',fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album'],true),'thumb_obj_id_'.$row['id'],110,110),$tpl);

  $ret.=$tpl;
  
if (($i==2)||($i==5)){
    $ret.='<div style="width:720px; overflow:hidden;">';
    $ret.=file_get_contents('protected/views/chunks/ad/tpl_adsense_728_90.php');
    $ret.='</div>';
   }
}
    
    
  
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;      
}
//******************************************************************************  




















// Футкция проверяет корректен ли ID объявления (возвращает ID или False)
//****************************************************************************** 
function fn__get_correct_obj_id($id=0){

  //----------------------------------------------------
  $id_cache='fn__get_correct_obj_id_'.$id."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
     $id = intval($id);
     $sql="SELECT count(`id`) as 'count' FROM `xta_obj` WHERE `id` = ".$id." AND `xta_obj`.`id_site`=".fn__get_site_id()."";
     $row=Yii::app()->db->createCommand($sql)->queryRow();
     if (!intval($row['count'])){
          $ret=false;	  
        }else{
          $ret=$id;
        }  
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;      
}
//******************************************************************************  
























// Футкция проверяет является ли пользователь создателем объявления
//****************************************************************************** 
function fn__get_is_obj_creator($id){
  
  $ret=false;

  if ((isset($_COOKIE['obid']))&&(isset($_COOKIE['obid_key']))&&(intval($_COOKIE['obid'])==$id)){

      $obid_key=$_COOKIE['obid_key'];
      $id = intval($id);
      
      $sql = "SELECT `user_email` FROM `xta_obj` WHERE `id` = ".$id." AND                    
              `xta_obj`.`id_site`=".fn__get_site_id()."";
      $row=Yii::app()->db->cache(10000000)->createCommand($sql)->queryRow();
      
      $hash=md5($row['user_email'].$id);
      if ($hash==$obid_key){
          $ret=true;
      }
  }  
    
  return $ret;      
}
//******************************************************************************  



















// Футкция устанавливает владельца объявления (если правильный хеш)
//****************************************************************************** 
function fn__set_obj_creator($id){
  
  if (isset($_GET['obid_key'])){
      $obid_key=$_GET['obid_key'];
          
      $sql = "SELECT `user_email` FROM `xta_obj` WHERE `id` = ".$id."
              AND `xta_obj`.`id_site`=".fn__get_site_id()."";
      $row=Yii::app()->db->cache(10000000)->createCommand($sql)->queryRow();
      
      $hash=md5($row['user_email'].$id);
      if ($hash==$obid_key){
          SetCookie("obid",$id,time()+7200,'/');
          SetCookie("obid_key",$hash,time()+7200,'/');
          header("Location: /board/".$id);
          exit;
      }
  } 
}
//******************************************************************************  










// Функция возвращает список объявлений того же города
//****************************************************************************** 
function fn__get_related_obj_in_obj($id=0,$count=5){

  //----------------------------------------------------
  $id_cache='fn__get_related_obj_in_obj_'.$id."_".$count."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 
  
  $id=intval($id);
  $count = intval($count);
  
  $sql = "SELECT count(`id`) as 'count' FROM `xta_obj` WHERE `id` = ".$id."
          AND `xta_obj`.`id_site`=".fn__get_site_id()."";
  $row=Yii::app()->db->createCommand($sql)->queryRow();
  if ($row['count']<2){
       $sql = "SELECT `id`,`id_album`,`name`,`text` 
               FROM `xta_obj` 
               WHERE `id_city` in
               (SELECT `id_city` FROM `xta_obj` WHERE `id` = ".$id.")
               AND `id`<>".$id."
                AND `xta_obj`.`id_site`=".fn__get_site_id()."
                ORDER BY `id` DESC LIMIT 0,".$count;
       $reader=Yii::app()->db->createCommand($sql)->query();
       foreach ($reader as $row){
          $tpl='
           <table style="height:10px; margin-top:15px;">
             <tr>
               <td style=" text-align:left; padding-bottom:5px;">
               <a href="/board/'.$row['id'].'" style="font-size:14px;">
                 '.mb_substr($row['name'],0,50,'UTF-8').'
               </a>
               </td>
             </tr>
             <tr>
               <td style="padding-right:5px; vertical-align:top; 
                 width:55px; text-align:center;">
                 <a href="/board/'.$row['id'].'">
                  <img src="'.fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album'],true),'thumb_obj_id_'.$row['id'],200,200,'themes/ukrturne/img/content/domik.gif').'" style="width:200px; height:120px;" class="corner iradius4">
                 </a>                                                           
               </td>
             </tr>
             <tr>
               <td style="vertical-align:top; font-size:12px; border-bottom:1px gray dotted;">
                 '.mb_substr(strip_tags($row1['text']),0,100,'UTF-8').'...
               </td>
             </tr>
           </table>
              ';  

       $ret.=$tpl;           	 
      }
     }else{
       $ret = '';
     }    

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;    
}
//******************************************************************************  




















// Футкция возвращает ID альбома объявления
//****************************************************************************** 
function fn__get_obj_album($id){
  
  //----------------------------------------------------
  $id_cache='fn__get_obj_album_'.$id."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------    
  
  
  $id = intval($id);

  $sql = "SELECT count(`id`) as 'count' FROM `xta_obj` WHERE `id` = ".$id." AND `xta_obj`.`id_site`=".fn__get_site_id()."";
  $row=Yii::app()->db->createCommand($sql)->queryRow();  
  if ($row['count']){
       $sql = "SELECT `id_album` FROM `xta_obj` WHERE `id` = ".$id;
       $row=Yii::app()->db->createCommand($sql)->queryRow();
       $id_album=$row['id_album'];
       $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id` = ".$id_album;
       $row=Yii::app()->db->createCommand($sql)->queryRow();
       if ($row['count']){
            $ret=$id_album;
          }else{
            $sql = "INSERT INTO `xta_albums`(`name`, `description`, `id_mainimg`) 
                    VALUES ('obj_".$id."','',0)";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "SELECT MAX(`id`) as 'id' FROM `xta_albums`";
            $row =Yii::app()->db->createCommand($sql)->queryRow();
            $max_album = $row['id'];
            $sql = "UPDATE `xta_obj` SET `id_album`=".$max_album." WHERE `id` = ".$id;
            Yii::app()->db->createCommand($sql)->execute();
            $ret=$max_album;
            Yii::app()->cache->flush();
          }
       
     }else{
       $ret=false;
     }
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;      
}
//******************************************************************************  























// Футкция возвращает имя периода оплаты
//****************************************************************************** 
function fn__get_pay_period_name($id=0,$field='name'){

  $id = intval($id);

  //----------------------------------------------------
  $id_cache='fn__get_pay_period_name_'.$id."_".$field;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

     
     $sql="SELECT count(`id`) as 'count' FROM `xta_pay_period` WHERE `id` = ".$id;
     $row=Yii::app()->db->createCommand($sql)->queryRow();
     if (!intval($row['count'])){
          $ret=false;	  
        }else{
          $sql="SELECT `".$field."` FROM `xta_pay_period` WHERE `id` = ".$id;
          $row=Yii::app()->db->createCommand($sql)->queryRow();
          $ret=$row[$field];
        }  
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;      
}
//******************************************************************************  

  






















// Функция возвращает список периодов оплаты
//****************************************************************************** 
function fn__get_pay_period_options($field='name'){ 



  //----------------------------------------------------
  $id_cache='fn__get_pay_period_options_'.$field;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 
  
  $sql = "SELECT `id`,`".$field."` FROM `xta_pay_period`";
  $reader =Yii::app()->db->createCommand($sql)->query();   
  foreach ($reader as $row){        
    $ret.='<option style="padding-left: 10px;" value="'.$row['id'].'">'.mb_ucfirst($row[$field],'utf-8').'</option>';
  }

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret; 

}
//******************************************************************************





// Функция возвращает список типов валюты
//****************************************************************************** 
function fn__get_valuta_options($id_defailt=0){ 

  $id_defailt = intval($id_defailt);

  //----------------------------------------------------
  $id_cache='fn__get_valuta_options_'.$id_defailt;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 
  
  $sql = "SELECT `id`,`name` FROM `xta_valuta`";
  $reader =Yii::app()->db->createCommand($sql)->query();   
  foreach ($reader as $row){  
    $selected=($id_defailt==$row['id'])?' selected="selected"':'';
    $ret.='<option value="'.$row['id'].'"'.$selected.'>'.$row['name'].'</option>';
  }

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret; 

}
//******************************************************************************

















// Функция возвращает название валюты
//****************************************************************************** 
function fn__get_valuta_name($id=0){ 

  $id = intval($id);

  //----------------------------------------------------
  $id_cache='fn__get_valuta_name_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 
  
  
  $sql = "SELECT count(`id`) as 'count' FROM `xta_valuta` WHERE `id` = ".$id;
  $row=Yii::app()->db->createCommand($sql)->queryRow();
  if (!intval($row['count'])){
       $ret='';	  
     }else{
       $sql = "SELECT `name` FROM `xta_valuta` WHERE `id` = ".$id;
       $row=Yii::app()->db->createCommand($sql)->queryRow();
       $ret=$row['name'];
     }    

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret; 

}
//******************************************************************************











// Функция удаляет объявление
//****************************************************************************** 
function fn__delete_board_item($id=0){ 
  if ((isset($_GET['do']))&&($_GET['do']=='delobj')){
    $id = intval($id);
    if (fn__get_is_obj_creator($id)){
      $sql = "SELECT count(`id`) as 'count' FROM `xta_obj` WHERE `id` = ".$id;
      $row=Yii::app()->db->createCommand($sql)->queryRow();
      if (intval($row['count'])){
        fn__del_album(fn__get_obj_album($id));
        $sql = "DELETE FROM `xta_obj` WHERE `id` = ".$id;
        Yii::app()->db->createCommand($sql)->execute();
        Yii::app()->cache->flush();
        header("Location: /board/");
        exit;      
      }
    }
  }
}
//******************************************************************************













// Функция возвращает список опций для формы вставки и редактирования опций
//****************************************************************************** 

function fn__get_add_obj_options($objid=0){ 
  
  $sql = "SELECT DISTINCT `id_category` FROM `xta_obj_options`";
  $reader =Yii::app()->db->createCommand($sql)->query();   
  foreach ($reader as $row){  
    $active_categs[]=$row['id_category'];
  }
  $active_categs=implode(',',$active_categs);


  $sql = "SELECT * FROM `xta_obj_options_category` WHERE `id` in (".$active_categs.")";
  $reader =Yii::app()->db->createCommand($sql)->query();   
  $ret='<table style="border:1px gray solid; " class="table-striped table-bordered">';
  foreach ($reader as $row){  
    $ret.='<tr>
           <td colspan="3" class="add_option_td_group" style="font-size:14px; padding:6px; background-color: #d3d7cf; ">
            '.$row['name'].'
           </td></tr>';

		$sql = "SELECT * FROM `xta_obj_options` WHERE `id_category` = ".$row['id']." ORDER BY `xta_obj_options`.`type` ASC ";
		$reader1 =Yii::app()->db->createCommand($sql)->query();   
		foreach ($reader1 as $row1){  
		  $ret.='<tr>
             <td rel="tooltip" title="'.$row1['description'].'" style="width:28px; padding:3px;">
							<img src="'.$row1['img'].'" style="width:28px; height:28px;">							
							</td><td style="padding:5px; font-size:13px; vertical-align:middle;  width:250px;">
							'.$row1['name'].'
							<br> 
							<span style="color:gray;">'.$row1['description'].'</span>
							
							
             </td><td style="padding-left:10px; vertical-align:middle;">';


 			$input_name='obj_option_'.$row1['id'];

			$input_value_checkbox='';
			$input_value_select=-1;
			$input_value_text='';
      if ((isset($_POST[$input_name]))||(fn_get_count_by_where('xta_obj_options_values','`id_obj` = '.$objid))){

        //echo  $input_name.' - '.$_POST[$input_name].'<br>';
       // echo $input_name.' - '.intval(fn__get_only_numbers(fn_get_fieldval_by_where('xta_obj_options_values','value' ,'`id_obj` = '.$objid.' AND `id_option` = '.$row1['id']))).'<br>';
        
        if ($row1['type']==0){ 
            $input_value = intval(fn__get_only_numbers(fn_get_fieldval_by_where('xta_obj_options_values','value' ,'
                           `id_obj` = '.$objid.' AND `id_option` = '.$row1['id'])));
            if (isset($_POST[$input_name])){
               $input_value=intval(fn__get_only_numbers($_POST[$input_name])); 
               }
            if ($input_value){$input_value_checkbox=' checked="checked" ';}
           }

        if ($row1['type']==1){ 
            $input_value_select=intval(fn__get_only_numbers(fn_get_fieldval_by_where('xta_obj_options_values','value' ,'`id_obj` = '.$objid.' AND `id_option` = '.$row1['id']))); 
            if (isset($_POST[$input_name])){
               $input_value_select=intval(fn__get_only_numbers($_POST[$input_name])); 
               }
           }

        if ($row1['type']==2){ 
            
            $input_value_select=fn_get_fieldval_by_where('xta_obj_options_values','value' ,'`id_obj` = '.$objid.' AND `id_option` = '.$row1['id']);
            if (isset($_POST[$input_name])){
               $input_value_text=mb_substr($_POST[$input_name],0,40,'utf-8'); 
               }
           }        
 			}

      $input="\n".'<input type="checkbox" name="'.$input_name.'" value="1" '.$input_value_checkbox.' />';
      if ($row1['type']==1) {
           $options = explode('|',$row1['values']);
           $i=0;
					 $_options=array();
					 foreach ($options as $item)
					 {
            $_options.='<option value="'.$i.'" '.(($i==$input_value_select)?' selected="selected"':'').'>
                        '.$item.'</option>';
            $i++;
					 }
           $input="\n".'<select name="'.$input_name.'" style="margin-top:3px; width:250px;">'.$_options.'</select>';
         }
      if ($row1['type']==2) {
           $input='<input type="text" maxlength="40" style="width:200px;" value="'.$input_value_text.'" name="'.$input_name.'">';
         }

      $ret.= $input.'</td></tr>';
		}
  }
	$ret.='</table>';


return $ret;

}
//******************************************************************************





//******************************************************************************
function fn__insert_obj_options($objid=0){


		$sql = "SELECT * FROM `xta_obj_options` ";
		$reader =Yii::app()->db->createCommand($sql)->query();   
		foreach ($reader as $row){  
			$input_name='obj_option_'.$row['id'];


      if ((isset($_POST['i_useoptions']))&&($_POST['i_useoptions']==1)&&(isset($_POST[$input_name]))){

//echo $input_name.'<br>';

				$value=intval(fn__get_only_numbers($_POST[$input_name]));
        $sql = "SELECT count(`id`) as 'count' FROM `xta_obj_options_values` 
								WHERE `id_obj` = ".$objid." AND `id_option` = ".$row['id'];
		    $row1=Yii::app()->db->createCommand($sql)->queryRow();
		    if (!intval($row1['count'])){
						$sql = "INSERT INTO `xta_obj_options_values`(`id_obj`, `id_option`, `value`) 
										VALUES (".$objid.",".$row['id'].",".$value.")";
					 }else{
				    $sql = "SELECT `id` FROM `xta_obj_options_values` 
										WHERE `id_obj` = ".$objid." AND `id_option` = ".$row['id'];
						$row1=Yii::app()->db->createCommand($sql)->queryRow();
            $optvalid = $row1['id'];
						$sql = "
						UPDATE `xta_obj_options_values` SET 
							`value`=".$value."
						WHERE 
							`id`=".$optvalid;
					 }
				Yii::app()->db->createCommand($sql)->execute();         
      }
		}
}
//******************************************************************************















//******************************************************************************
function fn__get_obj_options_in_obj($objid=0){

  //----------------------------------------------------
  $id_cache=__FUNCTION__.'_'.$objid;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 

  $count = fn_get_count_by_where('xta_obj_options_values',' `id_obj` = '.$objid);
  if ($count){
			$sql = "SELECT * FROM `xta_obj_options_values` WHERE `id_obj` = ".$objid;
			$reader =Yii::app()->db->createCommand($sql)->query();   
      $id_options=array();
			foreach ($reader as $row){  
          $id_options[]=$row['id_option'];
				}
			$id_options = implode(',',$id_options);


      $sql = "
			SELECT * FROM `xta_obj_options_category` WHERE `id` in (
			SELECT DISTINCT `id_category` FROM `xta_obj_options` WHERE `id` in (".$id_options.")
			)";
			$reader =Yii::app()->db->createCommand($sql)->query();   
			$i=0;
			foreach ($reader as $row){
       $ret_tabs_head.='
        <li class="'.(($i==0)?'active':'').'">
          <a data-toggle="tab" href="#yw42_tab_'.$row['id'].'">'.$row['name'].'</a>
        </li>
       ';
       $i++;
			 $sql = "SELECT * FROM `xta_obj_options` 
						WHERE `id_category` =".$row['id']." AND 
						`id` in (".$id_options.")";
			$reader1 =Yii::app()->db->createCommand($sql)->query();   
			$ret_tabs_content.='
			<div id="yw42_tab_'.$row['id'].'" class="tab-pane fade '.(($i==1)?' active in':'').'">
			<table style="border:1px gray solid; border-top:0px; border-color: rgb(221, 221, 221) rgb(221, 221, 221) transparent; " class="table-striped table-bordered">';
			foreach ($reader1 as $row1){
           $ret_tabs_content.='
               <tr><td style="padding:3px; width:38px;">
			  		   <img src="'.$row1['img'].'" style="width:32px; height:32px;">
			  		   </td><td style="padding:2px; padding-left:10px;">
  					  '.$row1['name'].'<br><span style="color:gray;">'.$row1['description'].'</span></td>';
           $sql="SELECT * FROM `xta_obj_options_values` 
                 WHERE `id_obj`=".$objid." AND `id_option` = ".$row1['id'];
			  $row2=Yii::app()->db->createCommand($sql)->queryRow();
				  
           $value = $row2['value'];
           $type= $row1['type'];
              
           if ($type==0){
             $_value = ($value)?'Есть':'Нет';
             }
           if ($type==2){
             $_value = $value;
             }
           if ($type==1){
             $_value = explode('|',fn_get_field_val_by_id('xta_obj_options','values',$row1['id']));
             if (count($_value)>$value){
                $_value = $_value[$value];
                }
              //$_value =  $row2['value']; 
             }
           
              
              $ret_tabs_content.='<td style="padding-left:10px;">'.$_value.'</td></tr>';
						}
						$ret_tabs_content.='</table></div>';
				}
      
      
      $ret='
        <div class="togglable-tabs" id="yw42">
          <ul id="yw43" class="nav nav-tabs" style="margin-bottom:0px;">
            '.$ret_tabs_head.'
          </ul>
          <div class="tab-content">
            '.$ret_tabs_content.'
          </div>
        </div>
      ';

     }
     
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;
}
//******************************************************************************






?>
