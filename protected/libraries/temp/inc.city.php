<?php 



//****************************************************************************** 
function fn__get_city_name($id=0){

  $id = intval($id);
  
  //----------------------------------------------------
  $id_cache='fn__get_city_name_'.$id."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
     
  $sql="
SELECT count(`xta_city`.`id`) as 'count' 
FROM `xta_city` 
LEFT JOIN `xta_region` on
  `xta_region`.`id` = `xta_city`.`region_id`
LEFT JOIN `xta_country` on
  `xta_country`.`id` = `xta_region`.`id_country`
LEFT JOIN `xta_sites` on
  `xta_country`.`id` = `xta_sites`.`id_country`
WHERE 
    `xta_city`.`id` = ".$id." 
  AND 
    `xta_sites`.`id`=".fn__get_site_id();
  $row=Yii::app()->db->createCommand($sql)->queryRow();
  if (!intval($row['count'])){
      $ret=false;	  
    }else{
           $sql="
SELECT `xta_city`.`name` 
FROM `xta_city` 
LEFT JOIN `xta_region` on
  `xta_region`.`id` = `xta_city`.`region_id`
LEFT JOIN `xta_country` on
  `xta_country`.`id` = `xta_region`.`id_country`
LEFT JOIN `xta_sites` on
  `xta_country`.`id` = `xta_sites`.`id_country`
WHERE 
    `xta_city`.`id` = ".$id." 
  AND 
    `xta_sites`.`id`=".fn__get_site_id();
           $row=Yii::app()->db->createCommand($sql)->queryRow();
           $ret=$row['name'];
  }  
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;      
}
//******************************************************************************  











// Функция возвращает ID региона по городу
//****************************************************************************** 
function fn__get_city_region($id_city=0){ 

  //----------------------------------------------------
  $id_cache='fn__get_city_region_'.$id_city;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 
   
   $id_city = intval($id_city);
   $sql = "
SELECT 
  count(`xta_city`.`id`) as 'count' 
FROM 
  `xta_city` 
LEFT JOIN `xta_region` on
  `xta_region`.`id` = `xta_city`.`region_id`
LEFT JOIN `xta_country` on
  `xta_country`.`id` = `xta_region`.`id_country`
LEFT JOIN `xta_sites` on
  `xta_country`.`id` = `xta_sites`.`id_country`
WHERE 
    `xta_city`.`id` = ".$id_city." 
  AND 
    `xta_sites`.`id`=".fn__get_site_id();
   $row =Yii::app()->db->createCommand($sql)->queryRow(); 
   if ($row['count']){
   $sql = "
SELECT 
  `xta_city`.`region_id`
FROM 
  `xta_city` 
LEFT JOIN `xta_region` on
  `xta_region`.`id` = `xta_city`.`region_id`
LEFT JOIN `xta_country` on
  `xta_country`.`id` = `xta_region`.`id_country`
LEFT JOIN `xta_sites` on
  `xta_country`.`id` = `xta_sites`.`id_country`
WHERE 
    `xta_city`.`id` = ".$id_city." 
  AND 
    `xta_sites`.`id`=".fn__get_site_id();


         $row =Yii::app()->db->createCommand($sql)->queryRow(); 
         $ret=intval($row['region_id']);
      }else{
         $ret=false;
      }
   
   // Кeшируем результат функции
   Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));   
   return $ret;
}
//****************************************************************************** 





















// Функция список регионов как <option>
//****************************************************************************** 
function fn__get_region_options($id_default=0,$allowregion=''){ 

  $id_default = intval($id_default); 

  //----------------------------------------------------
  $id_cache='fn__get_region_options_'.$id_default."_".$allowregion;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  
  if ($allowregion!=''){
       $sqlwhere=' AND `xta_region`.`id` in ('.$allowregion.')';
     }
  $ret='';
  $sql = "
SELECT 
  `xta_region`.`id`,
  `xta_region`.`name` 
FROM 
  `xta_region` 
LEFT JOIN `xta_country` on
  `xta_country`.`id` = `xta_region`.`id_country`
LEFT JOIN `xta_sites` on
  `xta_country`.`id` = `xta_sites`.`id_country`
WHERE 
  `xta_sites`.`id`=".fn__get_site_id()." ".$sqlwhere."
ORDER BY 
  `xta_region`.`name` ASC 
                 ";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row)    
    {
      $_selected='';
      if ($id_default==intval($row['id']))
         {
           $_selected=' selected="selected"';
           
         }
      $ret.='<option value="'.$row['id'].'"'.$_selected.'>
              '.$row['name'].'
             </option>';            
    }


  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;
}
//****************************************************************************** 




















// Функция список городов как <option>
//****************************************************************************** 
function fn__get_city_options($id_region=0,$id_default=0){ 
  $id_default = intval($id_default);
  $id_region = intval($id_region);
  
  //----------------------------------------------------
  $id_cache='fn__get_city_options_'.$id_region."_".$id_default."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------    
  
  $ret='';
  $sql = "
SELECT 
  `xta_city`.`id`,
  `xta_city`.`name`
FROM 
  `xta_city` 
LEFT JOIN `xta_region` on
  `xta_region`.`id` = `xta_city`.`region_id`
LEFT JOIN `xta_country` on
  `xta_country`.`id` = `xta_region`.`id_country`
LEFT JOIN `xta_sites` on
  `xta_country`.`id` = `xta_sites`.`id_country`
WHERE 
    `xta_city`.`region_id` = ".$id_region." 
  AND 
    `xta_sites`.`id`=".fn__get_site_id();;


  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row)    
    {
      $selected=($id_default==intval($row['id']))?' selected="selected"':'';
      $ret.='<option value="'.$row['id'].'"'.$selected.'>'.$row['name'].'</option>';        
    }
    

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;    
}
//****************************************************************************** 


















// Функция возвращает список ID регионов для списка городов
//****************************************************************************** 
function fn__get_actual_region_filter($id_list=array()){ 

  //----------------------------------------------------
  $id_cache='fn__get_actual_region_filter_'.serialize($id_list)."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  


  if (count($id_list)){
        $id_arr = implode(',',$id_list);
        $sql = "SELECT DISTINCT `region_id` FROM `xta_city` WHERE `id` in (".$id_arr.")";
        $reader =Yii::app()->db->createCommand($sql)->query();   
        foreach ($reader as $row){        
            $res[]=$row['region_id'];
          }
        $ret = $res; 
     }else{
         $ret = array();
       }
       
       
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;        
}
//******************************************************************************



























//****************************************************************************** 
function fn__get_region_name($id=0){

  //----------------------------------------------------
  $id_cache='fn__get_region_name_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
     $id = intval($id);
     $sql="
SELECT 
  count(`xta_region`.`id`) as 'count' 
FROM 
  `xta_region` 
LEFT JOIN `xta_country` on
  `xta_country`.`id` = `xta_region`.`id_country`
LEFT JOIN `xta_sites` on
  `xta_country`.`id` = `xta_sites`.`id_country`
WHERE 
    `xta_region`.`id` = ".$id." 
  AND 
    `xta_sites`.`id`=".fn__get_site_id();
     $row=Yii::app()->db->createCommand($sql)->queryRow();
     if (!intval($row['count'])){
          $ret=false;	  
        }else{
               $sql="SELECT `name` FROM `xta_region` WHERE `id` = ".$id;
               $row=Yii::app()->db->createCommand($sql)->queryRow();
               $ret=$row['name'];
     }  
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;      
}
//******************************************************************************  





















// Функция возвращает список городов актуальных для (новостей, достопримечательностей и т.д.)
//****************************************************************************** 
function fn__get_actual_city_filter($filter='news'){ 

  //----------------------------------------------------
  $id_cache='fn__get_actual_city_filter_'.$filter."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 

  if ($filter=='news'){
      $sql = "SELECT DISTINCT(`id_city`) FROM `xta_news` 
              WHERE `published` = 1 AND `id_site`=".fn__get_site_id();
     }
     
  if ($filter=='obj'){
      $sql = "SELECT DISTINCT(`id_city`) FROM `xta_obj`
              WHERE `id_site`=".fn__get_site_id();
     }
     
  if ($filter=='dostoprim'){
      $sql = "SELECT DISTINCT(`id_city`) FROM `xta_dostoprim` 
              WHERE `published` = 1 AND `id_site`=".fn__get_site_id();
     }    
     
  if ($filter=='story'){
      $sql = "SELECT DISTINCT(`id_city`) FROM `xta_story` 
              WHERE `published` = 1 AND `id_site`=".fn__get_site_id();
     }     
  
  if ($filter=='sale'){
      $sql = "SELECT DISTINCT(`id_city`) FROM `xta_sale` 
              WHERE `published` = 1 AND `id_site`=".fn__get_site_id();
     }   
  
  $reader =Yii::app()->db->createCommand($sql)->query();   
  $ret=array();
  foreach ($reader as $row){        
    $ret[]=$row['id_city'];
  }
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;   
}
//******************************************************************************


















// Функция возвращает корректен ли ID города
//****************************************************************************** 
function fn__is_correct_city_id($id_city=0){ 

  $id_city = intval($id_city);
  //----------------------------------------------------
  $id_cache='fn__is_correct_city_id_'.$id_city."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $ret=0;
  $sql = "SELECT count(`id`) as 'count' FROM `xta_city` 
          WHERE `id` = ".$id_city." AND `id_site`=".fn__get_site_id();
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if ($row['count']){
       $ret=$id_city;
     }
    
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret; 
}
//******************************************************************************


















// Функция возвращает список областей для текущей страны
//****************************************************************************** 
function fn__get_main_menu_region_list(){ 

  $id_city = intval($id_city);
  //----------------------------------------------------
  $id_cache='fn__get_main_menu_region_list_'.fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $ret='';
  $sql = "
SELECT `xta_region`.`id`,
       `xta_region`.`name` 
FROM `xta_region` 
LEFT JOIN `xta_country` on `xta_region`.`id_country` = `xta_country`.`id`
LEFT JOIN `xta_sites` on `xta_country`.`id` = `xta_sites`.`id_country`
WHERE `xta_sites`.`id` =".fn__get_site_id();
  $reader =Yii::app()->db->createCommand($sql)->query();   
  $i=0;
  foreach ($reader as $row){          
       $ret.='
        <td class="main_menu_div_items_block">          
          <div class="main_menu_div_item">              
            <a href="/region/'.$row['id'].'" >              
              <nobr>'.$row['name'].'
              </nobr></a>          
          </div> 
          </td>
       ';
       if ($i%3==2){$ret.='</tr><tr>';}
       $i++;
     }
    
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret; 
}
//******************************************************************************

















// Функция отправляет рассказ о городе
//****************************************************************************** 
function fn__send_city_about(){ 
  if (isset($_POST['formdata']['sendstory'])){
    $_about=strip_tags($_POST['formdata']['about'],'<b><p><br><ul><li><ol><i>');
    $_email=htmlspecialchars(mb_substr($_POST['formdata']['email'],0,250,'utf-8'), ENT_QUOTES);
    $_username=htmlspecialchars(mb_substr($_POST['formdata']['username'],0,250,'utf-8'), ENT_QUOTES);
    $_rurl=htmlspecialchars(mb_substr($_POST['formdata']['rurl'],0,250,'utf-8'), ENT_QUOTES);
    
    
    $_body='
-------------------------------------------------------
'.$_about.'
-------------------------------------------------------
Email пользователя: '.$_email.'
-------------------------------------------------------
Имя пользователя: '.$_username.'
-------------------------------------------------------
Сайт: '.fn__get_setting('site_name').'
-------------------------------------------------------
Город: '.$_rurl.'
-------------------------------------------------------

    ';
    
    
    mail(fn__get_setting('admin_email'), "На сайт ".fn__get_setting('site_name')." добавлено описание города", $_body); 
    
    return '
<div class="alert in alert-block fade alert-info">
  <a class="close" data-dismiss="alert">×</a>
  Ваше описание отправлено и в течение суток будет опубликовано на сайте
  <br>
  <span style="font-weight:bold;">Спасибо за то, что поделились ценной информацией</span>
</div>';
  }else{
    return '

    
<script type="text/javascript" src="/themes/ukrturne/components/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		editor_selector : "mceEditor",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,image,cleanup",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
	});
</script>








<form method="POST" action="'.$_SERVER['REQUEST_URI'].'" name="formaddstory">
<div aria-hidden="true" style="display: none;" id="myModal" class="modal hide fade">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <div style="font-weight:bold; font-size:17px; color#000;">Напишите свое описание населенного пункта</div>
  </div>

  <div class="modal-body">      
      <textarea name="formdata[about]" class="mceEditor" style="width:100%; height:240px;"></textarea>
      
      <table style="height:10px;">
        <tr>
          <td style="width:50%; padding:10px">
            <label>Ваше имя (необязательно)</label>
            <input name="formdata[username]" type="text" style="width:100%; height:25px;">
            <input name="formdata[rurl]" type="hidden" value="'.$_SERVER['REQUEST_URI'].'">
          </td>
          <td style="width:50%; padding:10px">
            <label>Ваш email (необязательно)</label>
            <input name="formdata[email]" type="text" style="width:100%; height:25px;">
          </td>
        <tr>
      </table>
      <input type="hidden" name="formdata[sendstory]">
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" type="submit">Отправить</button>
    <a data-dismiss="modal" class="btn" href="#">Закрыть</a>    
  </div>
</div>
</form>







<script language="javascript">
function frmSubmit(){
  alert("start");
  
  $.post(
         "/story", 
         $("#formaddstory").serialize(),
         function(data){
                        alert("sended");
                       },
         "json"
        );
  alert($("#formaddstory").serialize());
  alert("end");
   }
</script>







<div style="text-align:center;">
  <a data-toggle="modal" title="Добавьте описание населенного пункта" data-target="#myModal" class="btn btn-primary">
    Добавить описание
  </a>
</div>
';
  }
}
//******************************************************************************
























// Функция возвращает ID альбома региона (или создает пустой)
//****************************************************************************** 
function fn__get_region_album($id_news=0,$albumname=''){ 
  $albumname = mb_substr($albumname,0,100,'utf-8');
  $id_news = intval($id_news);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_region` WHERE `id` = ".$id_news;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $sql = "SELECT `id_album` FROM `xta_region` WHERE `id` = ".$id_news;
     $row1 =Yii::app()->db->createCommand($sql)->queryRow();
     $id_album=intval($row1['id_album']);
     
     $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id` = ".$id_album;
     $row1 =Yii::app()->db->createCommand($sql)->queryRow();
     if ($row1['count']){
       return $id_album;
     }else{
       $sql = "INSERT INTO `xta_albums`(`name`, `description`) VALUES ('".$albumname."','');";
       Yii::app()->db->createCommand($sql)->execute();
       $sql = "SELECT MAX(`id`) as 'max' FROM `xta_albums`";
       $row2 =Yii::app()->db->createCommand($sql)->queryRow();
       $id_album=intval($row2['max']);
       $sql = "UPDATE `xta_region` SET `id_album`=".$row2['max']." WHERE `id`=".$id_news;
       Yii::app()->db->createCommand($sql)->execute();
       return $id_album;
     }
  }
  return false;
}
//****************************************************************************** 

















// Функция возвращает название страны, которая привязана к сайту
//****************************************************************************** 
function fn__get_country_name(){ 

  $id_city = intval($id_city);
  //----------------------------------------------------
  $id_cache='fn__get_country_name_'.fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $ret='';
  
  $id_country = fn_get_field_val_by_id('xta_sites','id_country',fn__get_site_id());
  $ret = fn_get_field_val_by_id('xta_country','name',$id_country);

    
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret; 
}
//******************************************************************************









// Функция список стран для списка в меню
//****************************************************************************** 
function fn__get_country_list_menu(){ 
  //----------------------------------------------------
  $id_cache='fn__get_country_list_menu_'.fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

   $ret_css='
select.countyselect_'.fn__get_site_id().'{
  background:transparent url(/public/sites/flags/flag_'.fn__get_site_id().'.jpg);
  background-repeat: no-repeat;
  background-position: 2% center;
}
';
   $ret='
<select class="countyselect countyselect_'.fn__get_site_id().'" onchange="window.location.href=this.value" title="Выберите страну">';
   $sql="SELECT DISTINCT * FROM `xta_country` WHERE `id` in (
	 SELECT `id_country` FROM `xta_sites`)";
   $reader =Yii::app()->db->createCommand($sql)->query(); 
   foreach ($reader as $row){
     $site_id = fn_get_fieldval_by_where('xta_sites', 'id' ,'`id_country` = '.$row['id']);
     $site_host = fn_get_field_val_by_id('xta_sites', 'name' ,$site_id);

   $ret_css.='
select.countyselect option.country_'.$row['id'].'{
  background-image: url(/public/sites/flags/flag_'.$row['id'].'.jpg);
}';
   $ret.='
     <option '.((fn__get_site_id() == $site_id)?' selected="selected"':"").' value="http://'.$site_host.'/" class="country_'.$row['id'].'">'.$row['name'].'</option>';


   }
  $ret.='</select>';


$ret = '<style>'.$ret_css.'</style>'.$ret;


 
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret; 
}
//******************************************************************************

?>
