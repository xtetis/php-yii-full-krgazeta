<?php 



// Функция возвращает форму отправки комментария
//****************************************************************************** 
function fn__get_comments_form($key=''){ 
  fn__del_comment();
  $hash=md5($key.date("m.d.y").'dedaba1f1713e7af35783919d41082dd');
  
  if (isset($_POST['sbm_comment'])){

    $_name=htmlspecialchars(strip_tags($_POST['formdata']['name']), ENT_QUOTES);
    $_email=htmlspecialchars(strip_tags($_POST['formdata']['email']), ENT_QUOTES);
    $_about=htmlspecialchars(strip_tags($_POST['formdata']['about']), ENT_QUOTES);
    $_about_normal=$_POST['formdata']['about'];
    $_hash_post=trim($_POST['formdata']['captcha']);
    



    $_err='';
    if ($_hash_post!=$hash){
      $_err='Это спам (или у Вас отключен JavaScript)';
    }
    if ($_about==''){
      $_err='Введите сообщение';
    }
    if (strpos($_about_normal,'http')!==false){
      $_err='В сообщении недопустимы ссылки';
    }
    if (strpos($_about_normal,'HREF')!==false){
      $_err='В сообщении недопустимы ссылки';
    }
    if (strpos($_about_normal,'href')!==false){
      $_err='В сообщении недопустимы ссылки';
    }    
    if (strlen($_about)<50){
      $_err='Сообщение не может быть короче 50 символов';
    }
    if ($_name==''){
      $_err='Введите имя';
    }
    if (strlen($_name)<3){
      $_err='Имя не может быть короче 3 символов';
    }
    if ($_email==''){
      $_err='Введите email';
    }
    if (strlen($_email)<6){
      $_err='Email не может быть короче 6 символов';
    }
    if (!checkEmail($_email)){
      $_err='Введен некорректный email';
    }    
    
    
    
    
    // Проверка на стоп слова -----------------
    $stop_words=explode(',',fn__get_setting('comments_stop_words'));
    foreach ($stop_words as $item)
    {
      if (strpos($_about,trim($item))!==false){
       $_err='Вы тексте Вы использвали недопустимое слово - "'.trim($item).'"';
       break;
      }
    }
    
    // ----------------------------------------    
    
    
    if ($_err!=''){
      $_err_message='
      <div class="alert in alert-block fade alert-error">
        <a class="close" data-dismiss="alert">×</a>
        <span style="font-weight:bold;">Ошибка при добавлении комментария:</span> '.$_err.'</div>';
    }else{
      $_err_message='
      <div class="alert in alert-block fade alert-success">
        <a class="close" data-dismiss="alert">×</a>
        <span style="font-weight:bold;">Комментарий успешно добавлен</span> '.$_err.'</div>';    

      $sql="INSERT INTO `xta_comments`(
            `key`, 
            `about`, 
            `username`, 
            `email`, 
            `createdon`, 
            `editedon`, 
            `published`) 
            VALUES (
            '".$key."',
            '".$_about."',
            '".$_name."',
            '".$_email."',
            CURRENT_TIMESTAMP, 
            CURRENT_TIMESTAMP, 
            1
            ) ";   
      Yii::app()->db->createCommand($sql)->execute();  
      Yii::app()->cache->flush();     
      
      
  // Отправка email администратору
  //----------------------------------------------------------------------
  $to  = fn__get_setting('admin_email');
  $subject = "Новый комментарий на сайте ".ucfirst($_SERVER['HTTP_HOST']);
  $message= fn__get_setting('tpl_email_template_about_create_comment');
  $message = str_replace('++sitename++',ucfirst($_SERVER['HTTP_HOST']),$message);
  $message = str_replace('++siteurl++',$_SERVER['HTTP_HOST'],$message);
  $message = str_replace('++rurl++',$_SERVER['REQUEST_URI'],$message);
  $message = str_replace('++about++',$_about,$message);
  $message = str_replace('++username++',$_name,$message);
  $message = str_replace('++email++',$_email,$message);
  $message = str_replace('++time++', date("H:i:s  d.m.Y"),$message);
  $message = str_replace('++hash++', md5($_email.fn__get_max_table_id('xta_comments').$_name),$message);
  $message = str_replace('++idcomment++', fn__get_max_table_id('xta_comments'),$message);
  
  

  
  fn__send_email($to,$subject, $message);
  //----------------------------------------------------------------------       
      
          
      $_name='';
      $_email='';
      $_about='';
    }
  }
  
  $ret='
  
  <a name="postcomment"></a>
  '.$_err_message.'
  <form method="post" action="'.$_SERVER['REQUEST_URI'].'#postcomment">
  <p class="well">
    <span style="padding-left:8px; padding-top:5px; font-size:17px; font-weight:bold; display:block;">Оставьте свой отзыв</span>
    <input type="hidden" name="key" value="'.$key.'" />
    <table style="height:10px;">
      <tr>
        <td style="padding:8px;">
          <label for="formdata[name]" style="font-weight:bold; font-size:13px;">Имя</label>
          <input style="width:100%; padding:3px; font-size:13px; height:30px;" maxlength="50" name="formdata[name]" type="text" value="'.$_name.'" />	
        </td>
        <td style="padding:8px;">
          <label for="formdata[email]" style="font-weight:bold; font-size:13px;">Email</label>
          <input style="width:100%; padding:3px; font-size:13px; height:30px;" maxlength="50" name="formdata[email]" type="text" value="'.$_email.'" />	
        </td>
      </tr>
      <tr>
        <td colspan="2" style="padding:8px; padding-top:0px;">
          <label for="formdata[about]" style="font-weight:bold; font-size:13px;">Сообщение</label>
          <textarea style="width:100%; font-size:13px; padding:3px;" id="comment_about" name="formdata[about]">'.$_about.'</textarea>
         <span style="font-size:12px;">Всего символов: <span id="textarea_comment_strlen">'.mb_strlen ($_about,'utf-8').'</span> должно быть не меньше 50-ти</span> 
        </td>
      </tr>

      <tr id="id_tr_comment_captcha">
        <td colspan="2" style="padding:8px; padding-top:5px;">
          <label style="font-weight:bold; font-size:13px;">
          Подтвердите Ваш комментарий, нажав на красную область</label>
          <input id="captcha" style="width:30px; height:30px; color:red; border:0px; background:red;" name="formdata[captcha]" type="text" readonly="readonly" onClick="setTimeout(decodeBase64(\''.base64_encode(' $("#captcha").val("'.$hash.'"); $(\'#captcha\').css( \'color\',\'green\'); $(\'#captcha\').css( \'background\',\'green\'); $("#id__comment_sbm").show(); ').'\'), 1)" />	
        </td>
      </tr>

      <tr>
        <td colspan="2" style="padding:8px; padding-top:5px;">
          <input style="display:none;" class="btn btn-primary btn-small" type="submit" name="sbm_comment" value="Отправить комментарий" id="id__comment_sbm" />
        </td>
      </tr>
    </table>
    <script>
      
      $("#comment_about").keyup(function()
        {
          var curLength = $("#comment_about").val().length; 
          $("#textarea_comment_strlen").html(curLength);
        })
    </script>
  </p>
  </form>
  
  ';


  return $ret; 
}
//******************************************************************************

















// Функция возвращает омментарии для определенного ключа
//****************************************************************************** 
function fn__get_comments_list($key=''){ 

  //----------------------------------------------------
  $id_cache='fn__get_comments_list'.$key;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  


  $sql = "SELECT * FROM `xta_comments`WHERE `published` =1
          AND `key` = '".$key."' ORDER BY `createdon` ASC";
  $reader =Yii::app()->db->createCommand($sql)->query();
  $ret='';
  foreach ($reader as $row){   
    $emailhash = md5(strtolower(trim($row['email'])));
    $imgsrc="http://www.gravatar.com/avatar/".$emailhash."?d=http://ru.gravatar.com/userimage/4012651/c52ab4a1910f5df39f8a0dfedb85ab1a.png";
    $ret.='
    <div class="alert in alert-block fade alert-warning" style="padding:8px;">
      <table style="height:10px;">
        <tr>
          <td style="width:50px; padding-right:5px;"> 
            <img alt="Аватар пользователя" src="'.$imgsrc.'">
          </td>
          <td style="vertical-align:top; padding-left:5px;">
            <div style="font-size:12px;">
              '.$row['about'].'
            </div>
            <div style="font-size:12px; color:#888a85; padding-top:7px;">
              Отправлено пользователем 
              <span style="font-weight:bold;">'.$row['username'].'</span> в '.$row['createdon'].'
            </div>
          </td>
        </tr>
      </table>
    </div>
    ';
  }
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;  
}
//******************************************************************************













// Функция удаляет комментарий
//****************************************************************************** 
function fn__del_comment(){ 
  if ((isset($_GET['do']))&&(isset($_GET['hash']))&&(isset($_GET['comment']))&&($_GET['do']=='delcomment')){
    $id = intval($_GET['comment']);
    $sql="SELECT count(`id`) as 'count' FROM `xta_comments` WHERE `id`=".$id;
    $row =Yii::app()->db->createCommand($sql)->queryRow(); 
    if ($row['count']){
      $sql="SELECT * FROM `xta_comments` WHERE `id`=".$id;
      $row =Yii::app()->db->createCommand($sql)->queryRow(); 
      if (md5($row['email'].$id.$row['username'])==$_GET['hash']){
        fn__del_record_by_id('xta_comments',$id);
        Yii::app()->cache->flush();
        echo '
        <a name="delcomment"></a>
        <div class="alert in alert-block fade alert-success">
        <a class="close" data-dismiss="alert">×</a>
        <span style="font-weight:bold;">Комментарий успешно удален</span></div>
        ';  
        //header("Location: /article/".$row['id_article']);
        //exit();
      }
    }
  }
}
//******************************************************************************

?>
