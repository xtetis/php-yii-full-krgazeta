<?php 







// Функция возвращает корректен ли ID категории достопримечательности
// Входные параметры
// id - ID города
//****************************************************************************** 
function fn__is_correct_typedostoprim_id($id=0){ 

  $id = intval($id);
  //----------------------------------------------------
  $id_cache='fn__is_correct_typedostoprim_id_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
  $ret=0;
  $sql = "SELECT count(`id`) as 'count' FROM `xta_type_dostoprim` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();   
  if ($row['count']){
       $ret=$id; 
     }
  
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret;   
}
//******************************************************************************












// Функция возвращает количество опубликованных достопримечательностей
// Входные параметры
// id_city - ID города (необязательный параметр)
// $id_category = ID категории достопримечатености
//****************************************************************************** 
function fn__get_count_published_dostoprim($id_city=0, $id_category=0){ 
  $id_city = intval($id_city);
  $id_category = intval($id_category);
  
  //----------------------------------------------------
  $id_cache='fn__get_count_published_dostoprim_'.$id_city."_".$id_category."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------    
  
  
  $sql = "SELECT count(`id`) as 'count' FROM `xta_dostoprim` 
          WHERE `published` = 1 AND `id_site`=".fn__get_site_id()." ";
  if ($id_city){
    $sql.=' AND `id_city` = '.$id_city;
  }
  
  if ($id_category){
    $sql.=' AND `id_type_dostoprim` = '.$id_category;
  }  

  $row =Yii::app()->db->createCommand($sql)->queryRow();
  $ret=$row['count'];

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;     
}
//******************************************************************************























// Функция возвращает имя категории достопримечательности по ID
//****************************************************************************** 
function fn__get_dostoprimcategory_name($id=0){ 
  
  $id=intval($id);
  
  //----------------------------------------------------
  $id_cache='fn__get_dostoprimcategory_name_'.$id."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 

  $ret='';
  $sql = "SELECT `name` FROM `xta_type_dostoprim` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();   
  $ret=$row['name']; 
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));   
  return $ret;
}
//******************************************************************************

























// Функция возвращает список достопримечательностей 
// Входные параметры
// id_city - Фильтр по городу (необязательный)
//****************************************************************************** 
function fn__get_dostoprim_list($page=1,$id_city=0,$id_category=0){ 
  $id_city = intval($id_city);
  $id_category = intval($id_category);
  $page  = intval($page);
  
  //----------------------------------------------------
  $id_cache='fn__get_dostoprim_list_'.$page."_".$id_city."_".$id_category."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 
  
  if ($id_city){
        $cityfilter=' AND `xta_dostoprim`.`id_city` = '.$id_city." "; 
     } 
  
  if ($id_category){
        $category_filter=' AND `xta_dostoprim`.`id_type_dostoprim` = '.intval($id_category)." "; 
     }
  
  $sql = "SELECT 
          `xta_dostoprim`.*, 
          `xta_city`.`name` as 'cityname',
          `xta_region`.`name` as 'regionname',
          `xta_type_dostoprim`.`name` as 'categoryname'
          FROM `xta_dostoprim`
          LEFT JOIN `xta_city` on
             `xta_city`.`id` = `xta_dostoprim`.`id_city`
          LEFT JOIN `xta_region` on
             `xta_region`.`id` = `xta_city`.`region_id`
          LEFT JOIN `xta_type_dostoprim` on
             `xta_type_dostoprim`.`id` = `xta_dostoprim`.`id_type_dostoprim`
          WHERE
            `xta_dostoprim`.`published` = 1 
            AND 
            `xta_dostoprim`.`id_site`=".fn__get_site_id()."
            ".$cityfilter.$category_filter."  
          ORDER BY `xta_dostoprim`.`editedon` DESC   
          LIMIT ".(($page-1)*10)." , 10
          ";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row)
    {
      $imgsrc=fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album']),'thumb_dostoprim_'.$row['id'],100, 100);
      
      $ret.='
<div style="padding-bottom:15px;">
  <table style="height:10px;">
    <tr> 
      <td style="width: 100px; vertical-align:top; padding: 5px;"> 
        <img src="'.$imgsrc.'" alt="'.$row['name'].'" title="'.$row['name'].'" class="corner iradius15" style="width:100px; height:100px;">
      </td> 
      <td style="vertical-align:top; padding: 5px;">
        <div style="font-size:14px; font-weight:bold; text-align:left;"> 
          <a style="color: #111; font-weight:bold;" title="Нажмите, чтоб ознакомиться с достопримечательностью" href="/dostoprim/'.$row['id'].'"> 
            '.$row['name'].'
          </a>
        </div>
        <div style="text-align:left; margin-top:5px; font-size:12px; color: #222222;">
         '.mb_substr(strip_tags($row['about']),0,500,'utf-8').'...
        </div>
        </td>
    </tr>
    <tr>
      <td colspan="2">
        <table style="height:10px;">
         <tr>
          <td style="padding-right:10px;">
            <ul class="breadcrumbs breadcrumb" style="margin:0px; padding-top:2px; padding-bottom:2px;">
              <li>
                <a title="Перейти к просмотру достопримечательности" href="/dostoprim/'.$row['id'].'" style="font-size:13px;">Читать полностью ...</a>
                <span class="divider">/</span>
              </li>
              <li>
                <a rel="tooltip" title="Показать все достопримечательности города &quot;'.$row['cityname'].'&quot;" href="/dostoprim?city='.$row['id_city'].'" style="font-size:13px;">'.$row['cityname'].'</a>
                <span class="divider">/</span>
              </li>
              <li>
                <a title="Показать все '.$row['categoryname'].' '.fn__get_padeg(fn__get_country_name(),1).'" href="/dostoprim?category='.$row['id_type_dostoprim'].'" style="font-size:13px;">
                  '.$row['categoryname'].'
                </a>
                <span class="divider">/</span>
              </li>
              <li class="active" style="font-size:13px;">
                '. date("d:m:Y",strtotime($row['editedon'])).'
              </li>
            </ul>
          </td>
          <td style="width:100px;">
          <a style="color:#fff; padding-left:15px; padding-right:15px;" href="/dostoprim/'.$row['id'].'" class="btn btn-primary btn-mini" title="'.$row['name'].' - прочитать подробнее">Подробнее</a>
          </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>
  


</div>
      ';
    }
    
   // Кeшируем результат функции
   Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));   
   return $ret;
}
//******************************************************************************



























// Функция возвращает список категорий достопримечательностей для опубликованных достопримечательностей
//****************************************************************************** 
function fn__get_list_actual_dostoprimcategory($selected_category=0){ 

  $selected_category = intval($selected_category);

  //----------------------------------------------------
  $id_cache='fn__get_list_actual_dostoprimcategory_'.$selected_category."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
  
  $sql = "SELECT `id`,`name` FROM `xta_type_dostoprim` WHERE `id` in (
             SELECT DISTINCT `id_type_dostoprim` FROM `xta_dostoprim` 
             WHERE `published` = 1 
             AND `xta_dostoprim`.`id_site`=".fn__get_site_id()."
          )";
  $reader =Yii::app()->db->createCommand($sql)->query();   
  foreach ($reader as $row){
    $style='';
    if ($selected_category==$row['id']){
         $style=' style="background-image: url(\'/themes/ukrturne/img/ico/bullet_go_red.png\');"';
       }
    $ret.=' <li>
              <a title="Показать все '.mb_strtolower($row['name'],"utf-8").'" href="/dostoprim?category='.$row['id'].'"'.$style.'>                                     
               '.$row['name'].'
              </a>
            </li>';
  }
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret; 
}
//******************************************************************************




















// Функция возвращает корректен ли ID  достопримечательности
// Входные параметры
// id - ID достопримечательности
//****************************************************************************** 
function fn__is_correct_dostoprim_id($id=0){ 
  $id = intval($id);
  
  //----------------------------------------------------
  $id_cache='fn__is_correct_dostoprim_id_'.$id."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------   
  
  $ret=0;
  $sql = "SELECT count(`id`) as 'count' FROM `xta_dostoprim` 
          WHERE `id` = ".$id." AND `id_site`=".fn__get_site_id();
  $row =Yii::app()->db->createCommand($sql)->queryRow();   
  if ($row['count']){
       $ret=$id;
     }
   
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret; 
}
//******************************************************************************


















// Функция возвращает 3 достопримечательности по фильтру (внизу страницы)
// Входные параметры
// id - ID достопримечательности
//****************************************************************************** 
function fn__get_bottom_list_dostoprim($id_city=0, $exclude_id=0){ 
  $id_city = intval($id_city);
  $exclude_id = intval($exclude_id);
  
  
  //----------------------------------------------------
  $id_cache='fn__get_bottom_list_dostoprim_'.$id_city."_".$exclude_id."_".fn__get_site_id();;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
    
  
  if ($id_city){
    $sql_city=' AND `id_city` = '.$id_city.' ';
  }
  
  if ($exclude_id){
    $sql_exclude=' AND `id` <> '.$exclude_id.' ';
  }
  
  $sql = "SELECT * FROM `xta_dostoprim` 
          WHERE `published` =1 AND `id_site`=".fn__get_site_id()."
          ".$sql_city.$sql_exclude.' 
          order by rand() LIMIT 0 , 3';
  $reader =Yii::app()->db->createCommand($sql)->query();   
  $ret='<div class="bottom_list_dostoprim">
        <table>
        <tr>';
  foreach ($reader as $row){
    $imgsrc=fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album']),'thumb_dostopr_'.$row['id'],200, 200);
            
    $ret.='
  <td>
    <a href="/dostoprim/'.$row['id'].'" title="'.htmlspecialchars($row['name'], ENT_QUOTES).'">
      <img src="'.$imgsrc.'" alt="'.htmlspecialchars($row['name'], ENT_QUOTES).'">
      <div>
        '.$row['name'].'
      </div>
    </a>
  </td>
    ';
  }
  $ret.='</tr>
          </table>';

  if (fn__get_count_published_dostoprim($id_city,0, true)>3){
    $ret.='
      <div style="padding-top:10px; text-align:center;">
        <a class="btn btn-primary btn-small" style="color:#fff;" href="/dostoprim?city='.$id_city.'">
         Посмотреть остальные достопримечательности '.fn__get_padeg(fn__get_city_name($id_city),1).'
        </a>
      </div> 
    ';  
  }
          
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret; 
}
//******************************************************************************
































// Функция возвращает список достопримечательностей для главной
//****************************************************************************** 
function fn__get_main_dostoprim_list($id_category=0,$limit=6){ 
  
  $id_category = intval($id_category);
 
  //----------------------------------------------------
  $id_cache='fn__get_main_dostoprim_list_'.$id_category."_".$limit."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
  if ($id_category){
       $category_filter=' AND `xta_dostoprim`.`id_type_dostoprim` = '.intval($id_category)." "; 
     }
  
  $sql = "SELECT 
          `xta_dostoprim`.*, 
          `xta_city`.`name` as 'cityname',
          `xta_region`.`name` as 'regionname',
          `xta_type_dostoprim`.`name` as 'categoryname'
          FROM `xta_dostoprim`
          LEFT JOIN `xta_city` on
             `xta_city`.`id` = `xta_dostoprim`.`id_city`
          LEFT JOIN `xta_region` on
             `xta_region`.`id` = `xta_city`.`region_id`
          LEFT JOIN `xta_type_dostoprim` on
             `xta_type_dostoprim`.`id` = `xta_dostoprim`.`id_type_dostoprim`
          WHERE
            `xta_dostoprim`.`published` = 1 
            AND `xta_dostoprim`.`id_site`=".fn__get_site_id()."
             ".$category_filter."  
          ORDER BY `xta_dostoprim`.`editedon` DESC   
          LIMIT 0 , ".$limit."
          ";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
$ret='
<table style="height:10px;">
  <tr>
';  
$i=0;
  foreach ($reader as $row)
    {
      $imgsrc=fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album']),'thumb_dostopr_'.$row['id'],120, 120);
      
      $ret.='
<td style="width: 33%; padding:3px; vertical-align:top;">
  <a href="/dostoprim/'.$row['id'].'" class="main_vert_subitem_a">
     <div style="text-align:center;">
       <img src="'.$imgsrc.'" class="corner iradius15" alt="'.htmlspecialchars($row['name'], ENT_QUOTES).'" style="height:120px; width:120px;"> 
     </div> 
    <div class="main_vert_subitem_pagetitle">
      '.$row['name'].'
     </div>  
  </a>
  <div class="city_dostopr">
     (<a title="Показать все достопримечательности '.fn__get_padeg($row['cityname'],1).'" href="/dostoprim?city='.$row['id_city'].'">
       '.$row['cityname'].'
      </a>)
  </div>  
</td>
      ';
    $i++;
      if (($i==3) or ($i==6)){
        $ret.='
          </tr>
          <tr>
        ';   
      }  
    }
$ret.='
  </tr>
</table>
';    

  // Кeшируем результат функции на сутки (обновлям главную страницу раз в сутки)
  Yii::app()->cache->set($id_cache, $ret, 86400);

  return $ret; 
}
//******************************************************************************

































// Функция список типов достопримечательностей как <option>
//****************************************************************************** 
function fn__get_typedostoprim_options($id_default=0){ 
  $id_default = intval($id_default);
  
  $result='';
  $sql = "SELECT `id`,`name` FROM `xta_type_dostoprim`";
  $reader =Yii::app()->db->cache(50000)->createCommand($sql)->query(); 
  foreach ($reader as $row)    
    {
      if ($id_default==intval($row['id']))
         {
           $result.='
<option value="'.$row['id'].'" selected="selected">'.$row['name'].'</option>';
         }else{
                $result.='
<option value="'.$row['id'].'">'.$row['name'].'</option>';
              }           
    }
  return $result;
}
//****************************************************************************** 





















// Функция возвращает максимальную достопримечательность
//****************************************************************************** 
function fn__get_max_dostoprim(){
   $sql="SELECT MAX(`id`) as 'max' FROM `xta_dostoprim`";
   $row =Yii::app()->db->createCommand($sql)->queryRow(); 
   return $row['max'];
}
//****************************************************************************** 











































// Функция публикует и снимает с публикации достопримечательность
//****************************************************************************** 
function fn__publish_dostoprim($id=0,$flag=true){
  $flag=intval($flag);
  $id = intval($id);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_dostoprim` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $val=0;
     if ($flag){$val=1;};
     $sql = "UPDATE `xta_dostoprim` SET `published`=".$val." WHERE `id`=".$id;
     Yii::app()->db->createCommand($sql)->execute();
     return true;
   }
  return false;  
}
//****************************************************************************** 










// Функция возвращает: опубликована ли достопримечательность
//****************************************************************************** 
function fn__is_dostoprim_published($id=0){
  $id = intval($id);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_dostoprim` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $sql = "SELECT `published` FROM `xta_dostoprim` WHERE `id` = ".$id;
     $row =Yii::app()->db->createCommand($sql)->queryRow();
     return intval($row['published']);
   }
  return false;  
}
//****************************************************************************** 



?>
