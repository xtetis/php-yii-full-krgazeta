<?php 





// Функция возвращает список категорий скидок как <option>
//****************************************************************************** 
function fn__get_sale_category_options($id_default=0){
  $id_default = intval($id_default);
  
  //----------------------------------------------------
  $id_cache='fn__get_sale_category_options_'.fn__get_site_id()."_".$id_default;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------    
  
  
  $ret='';
  $sql = "SELECT `id`,`name` FROM `xta_sale_category`";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row)    
    {
      $select = ($id_default==intval($row['id']))?' selected="selected"':'';
      $ret.='<option value="'.$row['id'].'"'.$select.'>'.$row['name'].'</option>';           
    }
  
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret;   
}
//****************************************************************************** 













// Функция максимальный ID скидки
//****************************************************************************** 
function fn__get_max_sale(){
   $sql="SELECT MAX(`id`) as 'max' FROM `xta_sale` WHERE `xta_sale`.`id_site`=".fn__get_site_id();
   $row =Yii::app()->db->createCommand($sql)->queryRow(); 
   return $row['max'];
}
//****************************************************************************** 











// Функция возвращает ID альбома скидки (или создает пустой)
//****************************************************************************** 
function fn__get_sale_album($id_news=0,$albumname=''){ 
  $albumname = mb_substr($albumname,0,100,'utf-8');
  $id_news = intval($id_news);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_sale` WHERE `id` = ".$id_news;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $sql = "SELECT `id_album` FROM `xta_sale` WHERE `id` = ".$id_news;
     $row1 =Yii::app()->db->createCommand($sql)->queryRow();
     $id_album=intval($row1['id_album']);
     
     $sql = "SELECT count(`id`) as 'count' FROM `xta_albums` WHERE `id` = ".$id_album;
     $row1 =Yii::app()->db->createCommand($sql)->queryRow();
     if ($row1['count']){
       return $id_album;
     }else{
       $sql = "INSERT INTO `xta_albums`(`name`, `description`) VALUES ('".$albumname."','');";
       Yii::app()->db->createCommand($sql)->execute();
       $sql = "SELECT MAX(`id`) as 'max' FROM `xta_albums`";
       $row2 =Yii::app()->db->createCommand($sql)->queryRow();
       $id_album=intval($row2['max']);
       $sql = "UPDATE `xta_sale` SET `id_album`=".$row2['max']." WHERE `id`=".$id_news;
       Yii::app()->db->createCommand($sql)->execute();
       return $id_album;
     }
  }
  return false;
}
//****************************************************************************** 
















// Функция возвращает: опубликована ли скидка
//****************************************************************************** 
function fn__is_sale_published($id=0){
  $id = intval($id);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_sale` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $sql = "SELECT `published` FROM `xta_sale` WHERE `id` = ".$id;
     $row =Yii::app()->db->createCommand($sql)->queryRow();
     return intval($row['published']);
   }
  return false;  
}
//****************************************************************************** 


















// Функция публикует и снимает с публикации скидку
//****************************************************************************** 
function fn__publish_sale($id=0,$flag=true){
  $flag=intval($flag);
  $id = intval($id);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_sale` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  if (intval($row['count'])){
     $val=0;
     if ($flag){$val=1;};
     $sql = "UPDATE `xta_sale` SET `published`=".$val." WHERE `id`=".$id;
     Yii::app()->db->createCommand($sql)->execute();
     return true;
   }
  return false;  
}
//****************************************************************************** 




















// Функция возвращает корректен ли ID категории скидки
// Входные параметры
// id - ID города
//****************************************************************************** 
function fn__is_correct_sale_category_id($id=0){

  $id = intval($id);
  //----------------------------------------------------
  $id_cache='fn__is_correct_sale_category_id_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $ret=0;
  $sql = "SELECT count(`id`) as 'count' FROM `xta_sale_category` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();   
  if ($row['count']){
       $ret=$id;
     }
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret;   
}
//******************************************************************************















// Функция возвращает количество опубликованных скидок
// Входные параметры
// id_city - ID города (необязательный параметр)
//****************************************************************************** 
function fn__get_count_published_sale($id_city=0, $id_category=0){ 
  $id_city = intval($id_city);
  $id_category = intval($id_category);
  
  //----------------------------------------------------
  $id_cache='fn__get_count_published_sale_'.$id_city."_".$id_category;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------    
  
  
  $sql = "SELECT count(`id`) as 'count' FROM `xta_sale` 
          WHERE `published` = 1 AND `id_site`=".fn__get_site_id()."
          AND `startdate`<CURRENT_TIMESTAMP
          AND `stopdate`>CURRENT_TIMESTAMP
          ";

  if ($id_city){
    $sql.=' AND `id_city` = '.$id_city;
  }
  
  if ($id_newscategory){
    $sql.=' AND `id_category` = '.$id_category;
  }  
    
  $row =Yii::app()->db->createCommand($sql)->queryRow();
  $ret=$row['count'];

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;  
}
//******************************************************************************




















// Функция возвращает имя категории по ID
//****************************************************************************** 
function fn__get_sale_category_name($id=0){ 

  $id=intval($id);
  
  //----------------------------------------------------
  $id_cache='fn__get_sale_category_name_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $sql = "SELECT `name` FROM `xta_sale_category` WHERE `id` = ".$id;
  $row =Yii::app()->db->createCommand($sql)->queryRow();   
  $ret=$row['name']; 
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;  
}
//******************************************************************************















// Функция возвращает список новостей 
// Входные параметры
// id_city - Фильтр по городу (необязательный)
//****************************************************************************** 
function fn__get_sale_list($page=1,$id_city=0,$id_category=0){ 

  $id_city = intval($id_city);
  $id_newscategory = intval($id_category);
  $page = intval($page);

  //----------------------------------------------------
  $id_cache='fn__get_sale_list_'.fn__get_site_id()."_".$page."_".$id_city."_".$id_category;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  
  if ($id_city){
    $cityfilter=' AND `xta_sale`.`id_city` = '.$id_city." "; 
  } 
  
  if ($id_newscategory){
    $category_filter=' AND `xta_sale`.`id_category` = '.intval($id_category)." "; 
  }
  
  $sql = "SELECT 
          `xta_sale`.`id`, 
          `xta_sale`.`id_album`, 
          `xta_sale`.`id_category`, 
          `xta_sale`.`name`, 
          `xta_sale`.`id_city`,
          `xta_sale`.`description`, 
          `xta_sale`.`price`,
          `xta_sale`.`oldprice`,
          `xta_sale`.`id_valuta`,
          `xta_sale`.`startdate`,
          `xta_sale`.`stopdate`,
          `xta_valuta`.`name` as 'valutaname',
          `xta_city`.`name` as 'cityname',
          `xta_region`.`name` as 'regionname',
          `xta_sale_category`.`name` as 'categoryname'
          FROM `xta_sale`
          LEFT JOIN `xta_city` on
             `xta_city`.`id` = `xta_sale`.`id_city`
          LEFT JOIN `xta_valuta` on
             `xta_valuta`.`id` = `xta_sale`.`id_valuta`
          LEFT JOIN `xta_region` on
             `xta_region`.`id` = `xta_city`.`region_id`
          LEFT JOIN `xta_sale_category` on
             `xta_sale_category`.`id` = `xta_sale`.`id_category`
          WHERE
            `xta_sale`.`published` = 1 
            AND `xta_sale`.`id_site`=".fn__get_site_id()."
          ".$cityfilter.$category_filter."  
            AND `xta_sale`.`startdate`<CURRENT_TIMESTAMP
            AND `xta_sale`.`stopdate`>CURRENT_TIMESTAMP
          ORDER BY `xta_sale`.`id` DESC   
          LIMIT ".(($page-1)*10)." , 10
          ";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row)
    {
      $imgsrc=fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album']),'thumb_sale_'.$row['id'],160, 130);
      
      $ret.='
<div style="padding-bottom:15px;">
  <table style="height:10px;">
    <tr> 
      <td style="width: 100px; vertical-align:top; padding: 5px;"> 
        <img src="'.$imgsrc.'" alt="'.$row['name'].'" title="'.$row['name'].'" class="corner iradius15" style="width:160px; height:130px;">
      </td> 
      <td style="vertical-align:top; padding: 5px;">
        <div style="font-size:14px; font-weight:bold; text-align:left;"> 
          <a style="color: #3465a4; font-weight:bold; font-size:20px;" title="Нажмите, чтоб ознакомиться со скидкой" href="/sale/'.$row['id'].'"> 
            '.$row['name'].' - '.$row['price'].' '.$row['valutaname'].' вместо 
            '.$row['oldprice'].' '.$row['valutaname'].' 
          </a>
        </div>
        <div style="text-align:left; margin-top:5px; font-size:14px; color: #222222;">
           <div class="sale_parcent_block">
             -'.floor(100*(($row['oldprice']-$row['price'])/$row['oldprice'])).'%
           </div>
         '.strip_tags($row['description']).'
         <div style="text-align:center; padding-top:8px;">
           Акция продлится 
           <br>
           с <span style="color:#4e9a06; font-size:19px; font-weight:bold;">
               '.date("d-m-Y",strtotime($row['startdate'])).'
             </span>
           по <span style="color:#4e9a06; font-size:19px; font-weight:bold;">
           '. date("d-m-Y",strtotime($row['stopdate'])).'
           </span>
         </div>
        </div>        
      </td>
    </tr>
    <tr>
      <td colspan="2">
      
        <ul class="breadcrumbs breadcrumb" style="margin:0px; padding-top:2px; padding-bottom:2px;">
          <li>
            <a href="/sale/'.$row['id'].'" style="font-size:13px;">Подробнее ...</a>
            <span class="divider">/</span>
          </li>
          <li>
            <a rel="tooltip" title="Показать все скидки города &quot;'.$row['cityname'].'&quot;" href="/sale?city='.$row['id_city'].'" style="font-size:13px;">'.$row['cityname'].'</a>
            <span class="divider">/</span>
          </li>
          <li>
            <a title="Показать все скидки рубрики &quot;'.$row['categoryname'].'&quot;" href="/sale?category='.$row['id_category'].'" style="font-size:13px;">
              '.$row['categoryname'].'
            </a>
          </li>
        </ul>
        
      </td>
    </tr>
  </table>
  


</div>
      ';
    }
    
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret;      
}
//******************************************************************************

















// Функция возвращает список категорий новостей для опубликованных новостей
//****************************************************************************** 
function fn__get_list_actual_sale_category($selected_category=0){ 

  //----------------------------------------------------
  $id_cache='fn__get_list_actual_sale_category_'.fn__get_site_id()."_".$selected_category;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  


  $selected_category = intval($selected_category);
  $sql = "SELECT `id`,`name` FROM `xta_sale_category` WHERE `id` in (
             SELECT DISTINCT `id_category` 
             FROM `xta_sale` 
             WHERE 
               `published` = 1
                AND 
                `xta_sale`.`id_site`=".fn__get_site_id()."
                AND `xta_sale`.`startdate`<CURRENT_TIMESTAMP
                AND `xta_sale`.`stopdate`>CURRENT_TIMESTAMP
          )";
  $reader =Yii::app()->db->createCommand($sql)->query();   
  foreach ($reader as $row){
    $style='';
    if ($selected_category==$row['id']){
         $style=' style="background-image: url(\'/themes/ukrturne/img/ico/bullet_go_red.png\');"';
       }
    $ret.=' 
    <li>
      <a title="Показать все скидки рубрики &quot;'.$row['name'].'&quot;" href="/sale?category='.$row['id'].'"'.$style.'>                                     
       '.$row['name'].'
      </a>
    </li>';
  }
  
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));
  return $ret; 
}
//******************************************************************************













// Функция возвращает корректен ли ID скидки
// Входные параметры
// id - ID новости
//****************************************************************************** 
function fn__is_correct_sale_id($id=0){ 
  $id = intval($id);
  
  //----------------------------------------------------
  $id_cache='fn__is_correct_sale_id_'.$id."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  
  
  $ret=false;
  $sql = "SELECT count(`id`) as 'count' 
          FROM `xta_sale` 
          WHERE `id` = ".$id." AND `id_site`=".fn__get_site_id();
  $row =Yii::app()->db->createCommand($sql)->queryRow();   
  if ($row['count']){
       $ret=$id;
     }
  
    
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret;   
}
//******************************************************************************

























// Функция возвращает список новостей 
// Входные параметры
// id_city - Фильтр по городу (необязательный)
//****************************************************************************** 
function fn__get_sale_main_block(){ 

  //----------------------------------------------------
  $id_cache='fn__get_sale_main_block_'.fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  
  $sql = "SELECT 
          `xta_sale`.`id`, 
          `xta_sale`.`id_album`, 
          `xta_sale`.`id_category`, 
          `xta_sale`.`name`, 
          `xta_sale`.`id_city`,
          `xta_sale`.`description`, 
          `xta_sale`.`price`,
          `xta_sale`.`oldprice`,
          `xta_sale`.`id_valuta`,
          `xta_sale`.`startdate`,
          `xta_sale`.`stopdate`,
          `xta_valuta`.`name` as 'valutaname',
          `xta_city`.`name` as 'cityname',
          `xta_region`.`name` as 'regionname',
          `xta_sale_category`.`name` as 'categoryname'
          FROM `xta_sale`
          LEFT JOIN `xta_city` on
             `xta_city`.`id` = `xta_sale`.`id_city`
          LEFT JOIN `xta_valuta` on
             `xta_valuta`.`id` = `xta_sale`.`id_valuta`
          LEFT JOIN `xta_region` on
             `xta_region`.`id` = `xta_city`.`region_id`
          LEFT JOIN `xta_sale_category` on
             `xta_sale_category`.`id` = `xta_sale`.`id_category`
          WHERE
            `xta_sale`.`published` = 1 
            AND `xta_sale`.`id_site`=".fn__get_site_id()."
            AND `xta_sale`.`startdate`<CURRENT_TIMESTAMP
            AND `xta_sale`.`stopdate`>CURRENT_TIMESTAMP
          ORDER BY `xta_sale`.`id` DESC   
          LIMIT 0 , 6
          ";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row)
    {
      $imgsrc=fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album']),'thumb_sale_'.$row['id'],120, 90);
      
      $ret.='
<div style="padding-bottom:15px;">
 <table style="height:10px;">
  <tr>
   <td colspan="2" style="padding-bottom:8px;">
    <a style="color: #3465a4; font-weight:bold; font-size:18px;" title="Нажмите, чтоб ознакомиться со скидкой" href="/sale/'.$row['id'].'"> 
     '.$row['name'].' - '.$row['price'].' '.$row['valutaname'].' вместо 
     '.$row['oldprice'].' '.$row['valutaname'].' 
    </a>
   </td>
  </tr> 
  <tr>
   <td>
    <div>
     <img src="'.$imgsrc.'" alt="'.$row['name'].'" title="'.$row['name'].'" style="width:110px; height:80px;  border-top-left-radius: 9px;
  border-top-right-radius: 9px;">
    </div>
    <div style="padding:10px; padding-top:5px; padding-bottom:5px; text-align:center; width:90px; font-size:19px; color:#fff; background:#f57900; font-weight:bold;  border-bottom-right-radius: 9px;
  border-bottom-left-radius: 9px;    ">
      -'.floor(100*(($row['oldprice']-$row['price'])/$row['oldprice'])).'%
    </div>
   </td>
   <td style="padding:5px; vertical-align:top;">
    <div>
     '.strip_tags($row['description']).' со скидкой 
     <span>
       '.floor(100*(($row['oldprice']-$row['price'])/$row['oldprice'])).'%
     </span>  
    </div>
    <div style="text-align:center; padding-top:8px;">
     Акция продлится 
     <br>
     с <span style="color:#4e9a06; font-size:18px; font-weight:bold;">
         '.date("d-m-Y",strtotime($row['startdate'])).'
       </span>
     по <span style="color:#4e9a06; font-size:18px; font-weight:bold;">
     '. date("d-m-Y",strtotime($row['stopdate'])).'
     </span>
    </div>
    <div style="text-align:right; padding-right:10px; padding-top:5px;">
     <a style="color:#000;" href="/sale/'.$row['id'].'" class="btn btn-warning btn-mini">Подробнее</a>
    </div>
   </td>
  </tr>
 </table>
</div>   

      ';
    }
    
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));  
  return $ret;      
}
//******************************************************************************







?>
