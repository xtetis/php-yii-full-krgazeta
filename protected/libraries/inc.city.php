<?php 





// Возвращает список категорий для главной страницы
//****************************************************************************** 
function fn__get_main_districrs($id_default=0){
  $id_default = intval($id_default);
  //----------------------------------------------------  
  $id_cache=__FUNCTION__.'_'.fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------      
  
  $sql = "SELECT * FROM `xta_district` WHERE `id_city` in (
          SELECT `id_city` FROM `xta_site` WHERE `id` = ".fn__get_site_id().")";
  $tpl='<div><img src="/themes/board/img/ico/square.png"><a href="/cat?filter=district:+id+">+name+ район </a></div>';
  $ret=fn__get_select_by_sql_i_tpl($sql,$tpl,$id_default);
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 

















// Возвращает список областей для формы добавления\редактирования объявления
//****************************************************************************** 
function fn__get_select_districrs($default=-1){
  $default = intval($default);
  
  $sql = "SELECT * FROM `xta_district` WHERE `id_city` in (
          SELECT `id_city` FROM `xta_site` WHERE `id` = ".fn__get_site_id().")";
  $tpl="\n".'<option value="+id+"+default+>+name+ район</option>';
  
  $ret=fn__get_select_by_sql_i_tpl($sql,$tpl,$default);
  
  $ret='<option value="0" '.((!$default)?' selected="selected"':'').'>Все районы города</option>'.$ret;
  
  if ($default==-1){
    $ret='<option value="-1" id="option_district_minusone">Выберите район</option>'.$ret;
  }

  
  return $ret;  
}
//****************************************************************************** 








// Возвращает список областей для формы добавления\редактирования объявления
//****************************************************************************** 
function fn__get_correct_district_name($id=0){
  //----------------------------------------------------  
  $id_cache=__FUNCTION__.'_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------      
  
  $id = intval($id);
  if ($id){
    $ret = fn__get_field_val_by_id('xta_district','name',$id).' район';
  }else{
    $ret = 'Все районы';
  }

  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 




























// Возвращает список областей для формы добавления\редактирования объявления
//****************************************************************************** 
function fn__get_select_districts_filter($id_category=0){
  global $filters;
  fn__get_filters();
  
  //----------------------------------------------------
  $id_cache=__FUNCTION__.serialize(func_get_args()).$_SERVER['HTTP_HOST'].serialize($filters);
  $ret=fn__get_cache($id_cache);
  if($ret!==false){return $ret;}
  //----------------------------------------------------
  
  
  if ((isset($filters['district']))&&(intval($filters['district']))){
    $district = intval($filters['district']);
  }else{
    $district = 0;
  }
  $tempfilters = $filters;
  
  
  
  $sql = "SELECT * FROM `xta_district` WHERE `id_city` in (
          SELECT `id_city` FROM `xta_site` WHERE `id` = ".fn__get_site_id().")";
          
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row){
     $tempfilters['district'] = $row['id'];
     $newurl = fn__get_caregory_url($id_category, $tempfilters, array());
     
     
     if ($row['id']==$district){
       $ret.="\n".'<option value="'.$newurl.'" selected="selected">
                   '.$row['name'].' район</option>';
     }else{
       $ret.="\n".'<option value="'.$newurl.'">
                   '.$row['name'].' район</option>';
     }
  }
  
  unset($tempfilters['district']);
  $newurl = fn__get_caregory_url($id_category, $tempfilters, array());
  
  $ret='<option value="'.$newurl.'" '.((!$default)?' selected="selected"':'').'>
        Все районы города</option>'.$ret;
  
  // Кeшируем результат функции
  fn__set_cache($id_cache,$ret,array('xta_district'));
  return $ret;
}
//****************************************************************************** 

?>
