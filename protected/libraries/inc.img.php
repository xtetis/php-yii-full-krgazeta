<?php 



//****************************************************************************** 
function fn__create_album($name=''){ 
   $sql="INSERT INTO `xta_album`(`name`) VALUES ('".$name."')";
   Yii::app()->db->createCommand($sql)->execute();
   return fn__get_max_table_id('xta_album');
}
//******************************************************************************


















//****************************************************************************** 
function fn__save_base64_to_img($id_album=0,$encodedData=''){ 
  global $error_img_insert;
  $id_album = intval($id_album);
  if (strpos($encodedData,'eval(')!==false){$error_img_insert='В изображении присутствует "eval("'; return false;}
  if (strpos($encodedData,'<?')!==false){$error_img_insert='В изображении присутствует "<?"'; return false;}
  if (strpos($encodedData,'?>')!==false){$error_img_insert='В изображении присутствует "?>"'; return false;}
  if (strpos($encodedData,'REQUEST')!==false){$error_img_insert='В изображении присутствует "REQUEST"'; return false;}
  if (strpos($encodedData,'_GET')!==false){$error_img_insert='В изображении присутствует "_GET"'; return false;}
  if (strpos($encodedData,'_POST')!==false){$error_img_insert='В изображении присутствует "_POST"'; return false;}
  
  
  $data = explode(',', $encodedData);
  if (count($data)<2){return false;}
  
  $encodedData = str_replace(' ','+',$data[1]);
  $decodedData = base64_decode($encodedData);
  
  if (strpos($decodedData,'eval(')!==false){$error_img_insert='В изображении присутствует "eval("'; return false;}
  if (strpos($decodedData,'php')!==false){$error_img_insert='В изображении присутствует "php"'; return false;}
  if (strpos($decodedData,'REQUEST')!==false){$error_img_insert='В изображении присутствует "REQUEST"'; return false;}
  if (strpos($decodedData,'_GET')!==false){$error_img_insert='В изображении присутствует "_GET"'; return false;}
  if (strpos($decodedData,'_POST')!==false){$error_img_insert='В изображении присутствует "_POST"'; return false;}
  
  $randomName = md5(substr_replace(sha1(microtime(true)), '', 12)).'.png';
  
  $id_albim_path = floor($id_album/1000).'/'.$id_album;
  
  $_uploaddir='images/album/'.$id_albim_path.'/';
  $uploaddir=$_SERVER['DOCUMENT_ROOT'].'/'.$_uploaddir;



   $tmp = $_SERVER['DOCUMENT_ROOT'].'/images/album/'.floor($id_album/1000);
   if (!is_dir($tmp)){
     if (!mkdir($tmp)) {
       $error_img_insert='Невозможно создать директорию "'.$tmp.'"'; 
       return false;
     }
   }
 
   if (!is_dir($uploaddir)){
     if (!mkdir($uploaddir)) {
       $error_img_insert='Невозможно создать директорию "'.$uploaddir.'"'; 
       return false;
     }
   }
  
  if(file_put_contents($uploaddir.$randomName, $decodedData)){
     $sql="INSERT INTO `xta_image`(`id_album`, `src`) VALUES (".$id_album.",'".$_uploaddir.$randomName."')";
     Yii::app()->db->createCommand($sql)->execute();
     return $_uploaddir.$randomName;
  }else{
    $error_img_insert='Невозможно записать в файл "'.$uploaddir.$randomName.'"'; 
    return false;
  }
}
//******************************************************************************















// Функция возвращает SRC уменьшенного изображения
//****************************************************************************** 
function fn__get_img_thumb($src='',$nameprefix='thumb',$width=1, $height=1,$defaultimg='themes/board/img/content/no-photobig.jpg')
{
	$width = intval($width);
	$height = intval($height);
	if (file_exists($src))
	{
		$newname = 'images/thumbs/'.$nameprefix.'_w'.strval($width).'_h'.strval($height).'_'.md5($src).'.jpg';
		if (file_exists($newname))
		{
			return $newname;
		}
		
		$thumb=@Yii::app()->phpThumb->create($src);
		@$thumb->resize($width,$height);
		@$thumb->save($newname);
		
		if (file_exists($newname))
		{
			return $newname;
		}
		else
		{
			return fn__get_img_thumb($defaultimg,'thumb',$width,$height);
		}
		
	}
	else
	{
		return fn__get_img_thumb($defaultimg,'thumb',$width,$height);
	}
}
//******************************************************************************








// Функция проверяет существование в изображения
//****************************************************************************** 
// Первый параметр - ID изображения, второй параметр - необходимость проверки
// существования файла
function fn__image_exists($id_img=0,$file_exists=false){ 
  $sql = "SELECT count(`id`) as 'count' FROM `xta_image` WHERE `id` = ".intval($id_img);
  $row1 =Yii::app()->db->createCommand($sql)->queryRow();
  if ($row1['count']){
    return true; 
  }
  return false;
}
//****************************************************************************** 





















// Функция возвращает главное изображение альбома или false
// Входные параметры
// id - ID альбома
//****************************************************************************** 
function fn__get_album_mainimg_src($id=0,$watermarked=true){ 

  //----------------------------------------------------
  $id_cache=__FUNCTION__.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

   $id = intval($id);
   $sql = "SELECT count(`id`) as 'count' FROM `xta_album` WHERE `id` = ".$id;
   $row1 =Yii::app()->db->createCommand($sql)->queryRow();

   if ($row1['count']){
       $sql = "SELECT `id_image` FROM `xta_album` WHERE `id` = ".$id;
       $row =Yii::app()->db->createCommand($sql)->queryRow();
       $id_mainimg = $row['id_image'];
   
       $sql = "SELECT count(`id`) as 'count' 
       FROM `xta_image` WHERE `id`= ".$id_mainimg;
       $row =Yii::app()->db->createCommand($sql)->queryRow();   
       
       if ($row['count'])
          {
            $sql = "SELECT `src` FROM `xta_image` WHERE `id`= ".$id_mainimg;
            $row =Yii::app()->db->createCommand($sql)->queryRow();  
            $ret=$row['src'];
            
            if ($watermarked){
              $ret = fn__get_watermerked_img_src($ret);
            }
            

          }else{
                 $sql = "SELECT count(`id`) as 'count' 
                         FROM `xta_image` WHERE `id_album`= ".$id;
                 $row =Yii::app()->db->createCommand($sql)->queryRow();
                 if ($row['count']){
                      $sql = "SELECT `id` FROM `xta_image` WHERE `id_album`=".$id." LIMIT 0,1";
                      $row =Yii::app()->db->createCommand($sql)->queryRow();
                      $sql = "UPDATE `xta_album` SET `id_image`=".$row['id']." WHERE `id`=".$id;
                      Yii::app()->db->createCommand($sql)->execute(); 
                      $ret=fn__get_album_mainimg_src($id);
											if ($watermarked){
											  $ret = fn__get_watermerked_img_src($ret);
											}
                    }else{
                      $ret= false;
                    }
               }
     }else{   
       $ret=false;
     }
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
  return $ret;       
}
//****************************************************************************** 






















// Функция возврашает watermark изображение по его оригинальному
//****************************************************************************** 
function fn__get_watermerked_img_src($original_src='',$watermark_img=''){ 
$original_img=$_SERVER['DOCUMENT_ROOT'].'/'.$original_src;
$watermarked_img=$_SERVER['DOCUMENT_ROOT'].'/images/watermarked/wm_'.md5($original_img).'.jpg';
$otnosit_watermarked_img='images/watermarked/wm_'.md5($original_img).'.jpg';
if (file_exists($watermarked_img)){
  return $otnosit_watermarked_img;
}

if (!file_exists($original_img)) {
    return $original_src;
   }else{
    $watermark_img=$_SERVER['DOCUMENT_ROOT'].fn__get_setting('watermark_img_path');
    
    
    
    
    Yii::app()->ih->load($original_img)->watermark($watermark_img, 0,0,CImageHandler::CORNER_CENTER, 0.8)->save($watermarked_img);     
    return $otnosit_watermarked_img;
   }
}
//****************************************************************************** 
















// Функция возвращает количество изображений в альбоме
//******************************************************************************
function fn__get_album_images_count($id_album=0){ 


  //----------------------------------------------------
  $id_cache='fn__get_album_images_count_'.$id_album;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $id_album = intval($id_album);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_album` WHERE `id` = ".$id_album;
  $row =Yii::app()->db->createCommand($sql)->queryRow();   

  if (!$row['count']){
        $ret= false;
     }else{
        $sql = "SELECT count(`id`) as 'count' FROM `xta_image` WHERE `id_album` = ".$id_album;
        $row =Yii::app()->db->createCommand($sql)->queryRow();   
        $ret=intval($row['count']);
     }
     
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;     
}
//******************************************************************************



























// Функция возвращает изображения для галлереи
// id_album
// imgalt  - какие ALT добавлять в картинки  (котография *кого-чего* # )
function fn__get_album_html($id_album=0, $imgalt='', $exclude_main=true){ 
  


  //----------------------------------------------------
  $id_cache=__FUNCTION__.$id_album.$imgalt;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //---------------------------------------------------- 



  $id_album = intval($id_album);
  $sql = "SELECT * FROM `xta_image` WHERE `id_album` = ".$id_album."";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $ret.='<div class="album_img_container">';
  foreach ($reader as $row){  
      if (($exclude_main)&&(fn__get_album_main_img($id_album)==$row['id'])){continue;}
        
      $ret.='
      <div class="album_img_container_inner">
        <img src="'.fn__get_watermerked_img_src($row['src']).'" alt="Фотография '.$imgalt.' #'.$i.'" title="Фотография '.$imgalt.' #'.$i.'">
      </div>'; 
  }
  $ret.='</div>';
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;
}
//******************************************************************************


































// Функция возвращает ID главного изображения альбома
//****************************************************************************** 
function fn__get_album_main_img($id_album=0){ 

  //----------------------------------------------------
  $id_cache='fn__get_album_main_img_'.$id_album;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------  

  $id_album = intval($id_album);
  $sql = "SELECT count(`id`) as 'count' FROM `xta_album` WHERE `id` = ".intval($id_album);
  $row =Yii::app()->db->createCommand($sql)->queryRow();    

  if ($row['count']){
      $sql = "SELECT `id_image` FROM `xta_album` WHERE `id` = ".intval($id_album);
      $row1 =Yii::app()->db->createCommand($sql)->queryRow();    
      $id_main_img=intval($row1['id_mainimg']);
      
      $sql = "SELECT count(`id`) as 'count' FROM `xta_image` WHERE `id` = ".intval($id_main_img);
      $row1 =Yii::app()->db->createCommand($sql)->queryRow();    
      if ($row1['count']){
         $ret=$id_main_img; 
         

      }else{
         $sql = "SELECT count(`id`) as 'count' FROM `xta_image` WHERE `id_album` = ".$id_album;
         $row =Yii::app()->db->createCommand($sql)->queryRow();    
         if ($row['count']){
           $sql = "SELECT `id` FROM `xta_image` WHERE `id_album` = ".$id_album." LIMIT 0,1";
           $row =Yii::app()->db->createCommand($sql)->queryRow();    
           $id_img = intval($row['id']);
               
           $sql = "UPDATE `xta_album` SET `id_image`=".$id_img." WHERE `id`=".$id_album;
           Yii::app()->db->createCommand($sql)->execute();  
           $ret= $id_img;
         }else{
           $sql = "UPDATE `xta_album` SET `id_image`=0 WHERE `id`=".intval($id_album);
           Yii::app()->db->createCommand($sql)->execute();
           $ret= false;
         }
      }
   }
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec'));    
    
  return $ret;   
}
//****************************************************************************** 




?>
