<?php 

// Функция возвращает список разделов статей как <option>
//****************************************************************************** 
function fn__get_admin_obj_parent_category_options($id_default=0){
  $id_default = intval($id_default);
  
  //----------------------------------------------------  
  $id_cache=__FUNCTION__.'_'.$id_default."_".fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------      
  
  $selected=($id_default==0)? ' selected="selected"' : '' ;
  $ret='<option value="0" '.$selected.'>Корневая директория</option>';
  $ret.='<optgroup label="Корневая директория">';
  
  $sql = "SELECT * FROM `xta_obj_category` WHERE `id_parent` = 0";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row)    
    {
      $selected=($id_default==intval($row['id']))? ' selected="selected"' : '' ;
      $ret.='<option value="'.$row['id'].'"'.$selected.' style="padding-left:15px;">'.$row['name'].'</option>';
    }
  $ret.='</optgroup>';
  
  
  $sql = "SELECT * FROM `xta_obj_category` WHERE `id_parent` = 0";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row)    
    {
      $ret.='<optgroup label="'.$row['name'].'">';
      $sql = "SELECT * FROM `xta_obj_category` WHERE `id_parent` = ".$row['id'];
      $reader1 =Yii::app()->db->createCommand($sql)->query(); 
      foreach ($reader1 as $row1)    
        {
          $selected=($id_default==intval($row1['id']))? ' selected="selected"' : '' ;
          $ret.='<option value="'.$row1['id'].'"'.$selected.' style="padding-left:15px;">'.$row1['name'].'</option>';
        }
      $ret.='</optgroup>';
    }
    
      
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 










// Возвращает список категорий для главной страницы
//****************************************************************************** 
function fn__get_main_categories(){
  
  //----------------------------------------------------  
  $id_cache=__FUNCTION__.'_'.fn__get_site_id();
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------      
  
  $sql = "SELECT * FROM `xta_obj_category` WHERE `id_parent` = 0";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $i=0;
  foreach ($reader as $row)    
    {
       $ret.='
       <td class="categ_td" style="padding: 4px !important;">
         <a href="/cat/'.$row['id'].'">
         <table style="width:100px; height:100%;">
           <tr>
             <td>
               <img src="'.$row['img'].'" alt="'.htmlspecialchars($row['name']).'" style="width: 120px;
height: 120px;">
             </td>
           </tr>
           <tr>
             <td class="categ_td_text" style="padding: 3px; text-align: center;">
               <div title="'.fn__get_category_obj_count($row['id']).' объявлений" style="padding:1px;">'.$row['name'].'</div>
             </td>
           </tr>
         </table>
         </a>
       </td>
       '; 
       $i++;
       if ($i==7){
       $ret.='</tr><tr>';
       }
    }
  
  
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 













// Возвращает список категорий для главной страницы
//****************************************************************************** 
function fn__get_select_categories($click_gotocat=false){
  
  
  $sql = "SELECT * FROM `xta_obj_category` WHERE `id_parent` = 0";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $ret.='<div id="select_categs_parents">';
  foreach ($reader as $row)    
    {
       if ($click_gotocat){$a='<a href="/cat/'.$row['id'].'">';}else{
       $a='<a href="javascript: void(0)"  idx="'.$row['id'].'" class="a__cat_a_select">';
       }

       $ret.='
       <div style="width:120px; height:130px; display:inline; float:left; padding:3px;" >
         '.$a.'
           <div style="text-align:center;">
             <img src="'.$row['img'].'" style="width:110px; height:110px;">
           </div>
           <div style="text-align:center; font-size:13px;" class="div_cat_name_select">
             <div style="line-height:14px;">'.$row['name'].'</div>
           </div>
         </a>
       </div>
       '; 
    }
    $ret.='</div>';
    
    
  if (!$click_gotocat){
    
    
  
    $ret.='<div id="select_categs_parents_lvl2" style="display:none;">
      <div style="display:inline; width:285px; float:left; height: 280px;">
        <div class="header_cat_selector_1">
          Рубрика    &#10174;  
        </div>
        <div style="height:265px; width:284px; background-color:#f3f3f3; border-right:1px #d3d7cf solid;" class="contentHolder" id="rubric_1">
       ++parent_lvl1++
        </div> 
      </div>
      <div style="display:inline; width:284px; float:left; height: 280px;">
         ++parent_lvl2++
      </div>
      <div style="display:inline; width:284px; float:left; height: 280px;">
      ++parent_lvl3++
      </div>
    ';
    $ret.='</div>';
    
    
    
  
  $sql = "SELECT `id`, `name` FROM `xta_obj_category` WHERE `id_parent` = 0";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row){
    $ret1.='<div class="rubr_item" idx="'.$row['id'].'" id="select_categs_parents_lvl2_id'.$row['id'].'">
                '.$row['name'].' 
                '.((fn__get_count_by_where('xta_obj_category','`id_parent` = '.$row['id']))?'&rarr;':'').'
            </div>';
            
    $sql = "SELECT `id`, `name` FROM `xta_obj_category` WHERE `id_parent` = ".$row['id'];
    $reader1 =Yii::app()->db->createCommand($sql)->query(); 
      $ret2.='
      <div class="header_cat_selector_2 rubr_item_second parent_'.$row['id'].'">
          '.$row['name'].'  &#10174;
      </div>
      <div style="height:265px; width:284px; display:none; border-right: 1px #CCC solid;" 
           class="rubr_item_second parent_'.$row['id'].' contentHolder">
      ';
    foreach ($reader1 as $row1){
      $addclass="item_nochild_category";
      $count_children = fn__get_count_by_where('xta_obj_category','`id_parent` = '.$row1['id']);
      if ($count_children){$addclass="";}

      $ret2.='<div class="rubr_item_lvl2 '.$addclass.'" idx="'.$row1['id'].'">
                '.$row1['name'].' '.(($count_children)?'&rarr;':'').'
              </div>';
      $ret3.='
      <div class="header_cat_selector_3 rubr_item_third rubr_item_third_header parent_'.$row1['id'].'"
           style="height:24px !important;">
        '.$row1['name'].'
      </div>
      <div class="rubr_item_third parent_'.$row1['id'].' contentHolder">
      ';
      $sql = "SELECT `id`, `name` FROM `xta_obj_category` WHERE `id_parent` = ".$row1['id'];
      $reader2 =Yii::app()->db->createCommand($sql)->query(); 
      foreach ($reader2 as $row2){
        $ret3.='<div class="rubr_item_lvl3 item_nochild_category" idx="'.$row2['id'].'">'.$row2['name'].'</div>';
        }
      $ret3.='</div>';
    }
    $ret2.='</div>';
  }
  
  
  $ret = str_replace('++parent_lvl1++',$ret1,$ret);
  $ret = str_replace('++parent_lvl2++',$ret2,$ret);
  $ret = str_replace('++parent_lvl3++',$ret3,$ret);
  }
  
  return $ret;  
}
//****************************************************************************** 




















// Возвращает список категорий для главной страницы
//****************************************************************************** 
function fn__get_rubric_selector($id=0, $editcat=true,$insert_links=false){
  
  //----------------------------------------------------  
//  $id_cache=__FUNCTION__.$id.'_'.fn__get_site_id();
//  $ret=Yii::app()->cache->get($id_cache);
//  if($ret!==false)
//    {
//      return $ret;
//    }
  //----------------------------------------------------      
  
  $name = fn__get_field_val_by_id('xta_obj_category','name',$id);
  
  if ($insert_links){
    $ret='<a href="/cat/'.$id.'">'.$name.'</a>';
  }else{
    $ret=$name;
  }
  $img = fn__get_field_val_by_id('xta_obj_category','img',$id);
  
  $id_parent = fn__get_field_val_by_id('xta_obj_category','id_parent',$id);
  if ($id_parent){
    $name_parent = fn__get_field_val_by_id('xta_obj_category','name',$id_parent);
    if ($insert_links){
      $ret='<a href="/cat/'.$id_parent.'">'.$name_parent.'</a> → '.$ret;
    }else{
      $ret=$name_parent.' → '.$ret;
    }
    if (!strlen($img)){
       $img = fn__get_field_val_by_id('xta_obj_category','img',$id_parent);
       }
    
    $id_sub_parent = fn__get_field_val_by_id('xta_obj_category','id_parent',$id_parent);
    if ($id_sub_parent){
      $name_sub_parent = fn__get_field_val_by_id('xta_obj_category','name',$id_sub_parent);
      if ($insert_links){
        $ret='<a href="/cat/'.$id_sub_parent.'">'.$name_sub_parent.'</a> → '.$ret;
      }else{
        $ret=$name_sub_parent.' → '.$ret;
      }
    
    if (!strlen($img)){
       $img = fn__get_field_val_by_id('xta_obj_category','img',$id_sub_parent);
       }
    }
  }
  
  if ($editcat){
  
  if (strlen($img)){
     $ret = '<img src="'.$img.'" style="width:40px; height:40px;"> &nbsp;'.$ret;
     }
  
  $ret = '<div class="get_rubric_selector">'.$ret.'</div>';
  
  }

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 






















// Возвращает список категорий для главной страницы
//****************************************************************************** 
function fn__get_user_account_obj($archive=0,$id_user=0){
  if (!$id_user){
    $id_user = fn__get_user_id();
  }
  

  //****************************************************************************
  //  Получаем корректную страницу
  //****************************************************************************
  $count_obj = fn__get_count_by_where('xta_obj',"
   `id_user` = ".$id_user." AND
   `id_city` = ".fn__get_field_val_by_id('xta_site','id_city',fn__get_site_id())
  );
  $pages_count = floor($count_obj/10);
  if ($count_obj%10){$pages_count++;}
  $page = fn__get_correct_page($pages_count);
  //****************************************************************************
  
  
  //****************************************************************************
  //  Получаем пагинатор
  //****************************************************************************
  $paginator_url='/account/obj?page=';
  $_paginator=fn__get_pagination($paginator_url, $pages_count, $page);
  //****************************************************************************
  

  $start = 10*(intval($page)-1);
  $sql = "SELECT 
            `xta_obj`.*,
            `xta_valuta`.`shortname`
          FROM `xta_obj` 
          LEFT JOIN `xta_valuta` on `xta_valuta`.`id` = `xta_obj`.`id_valuta`
          WHERE `id_user` = ".$id_user." AND
          `id_city` = ".fn__get_field_val_by_id('xta_site','id_city',fn__get_site_id())." 
          ORDER BY `xta_obj`.`id` DESC 
          LIMIT ".$start.", 10
          ";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $i=0;
  foreach ($reader as $row)    {
    
    $view_link = '
          <a href="/board/'.$row['id'].'" target="blank">
             <i class="glyphicon glyphicon-share-alt"></i> <span>Просмотреть</span>
          </a>';
    $unpub_link = '
          <a href="/account/obj?unpubobj='.$row['id'].'" 
             onclick="return confirm(\'Вы уверены что хотите снять объявление с публикации?\');">
             <i class="glyphicon glyphicon-eye-close"></i> <span>Деактивировать</span>
          </a>';
    $obj_item_style="";
    $obj_item_pub="<span style=\"color:#4e9a06;\">Активно</span>";
    if (!intval($row['published'])){
        $unpub_link = '
              <a href="/account/obj?pubobj='.$row['id'].'">
                 <i class="glyphicon glyphicon-eye-open"></i> <span>Опубликовать</span>
              </a>';
        $obj_item_style="background-color:#d3d7cf;";
        $obj_item_pub="<span style=\"color:#a40000;\">Не активно</span>";
        $view_link = '';
       }
       
    $count_messages_item = "";
    $count_messages = fn__get_count_by_where('xta_user_message','`id_obj`='.$row['id']);
    if ($count_messages){
       $count_messages_item = '
        <a class="btn btn-primary" href="/account/messages">
          <i class="glyphicon glyphicon-envelope"></i> <span class="badge">'.$count_messages.'</span>
        </a>';
       }
  
  
       $ret.='
<div class="account_obj_item" style="'.$obj_item_style.'">
  <table style="width:100%;">
    <tr>
      <td style="font-size:12px; padding-left:4px; padding-right:8px; width:90px; text-align:center;">
        '.$row['createdon'].'
      </td>
      <td style="width:56px; padding-left:8px; padding-right:8px;">
        <a class="thumbnail" rel="tooltip" data-title="Фотография объявления" style="margin-bottom: 0px;">
            <img src="'.fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album'],true),'thumb_',50,40).'" alt="">
        </a>
      </td>
      <td style="vertical-align:top; padding-left:6px;">
        <div style="font-size:16px; font-weight:bold; padding-left:3px;">
          '.$row['name'].' 
        </div>
        <div class="account_obj_item_edita">
          '.$view_link.'
          <a href="/board/edit/'.$row['id'].'">
             <i class="glyphicon glyphicon-pencil"></i> <span>Редактировать</span>
          </a>
          '.$unpub_link.'
        </div>
      </td>
      <td style="font-size:15px; padding-left:2px; padding-right:2px; width:80px; font-weight:bold; text-align:center;">
        '.$obj_item_pub.'
      </td>
      <td style="font-size:22px; font-weight:bold; color:#3465a4; vertical-align:middle; width:70px;">
        '.$count_messages_item.'
      </td>
    </tr>
  </table>
</div>
       '; 
    }
  
  
  $ret.=$_paginator;
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 















// Возвращает блок категорий в топе списка объявлений
//****************************************************************************** 
function fn__get_category_block_in_cat($id=0){
  global $filters;
  fn__get_filters();

  //----------------------------------------------------
  $id_cache=__FUNCTION__.serialize(func_get_args()).$_SERVER['HTTP_HOST'].serialize($filters).'-'.$_GET['sort'];
  $ret=fn__get_cache($id_cache);
  if($ret!==false){return $ret;}
  //----------------------------------------------------
  
  
  
  // Проверяем существование категории
  //----------------------------------------------------            
  if ($id){
     if (!fn__get_count_by_where('xta_obj_category','`id` = '.$id)){
        return '';
        }  
     }
  //----------------------------------------------------      
  
  
  
  //----------------------------------------------------      
  $cat_name = 'Бесплатные объявления';
  if ($id) $cat_name = fn__get_field_val_by_id('xta_obj_category','name',$id);
  //----------------------------------------------------      
  
  $count_children = fn__get_count_by_where('xta_obj_category','`id_parent` = '.$id);
  
  $parent_name_block='';
  $parent_name_block_h1='';
  
  
  
  // Если существуют родительский элемент
  //------------------------------------------------------------------------------------------------
  if (fn__get_field_val_by_id('xta_obj_category','id_parent',$id)){
     $parent_id = fn__get_field_val_by_id('xta_obj_category','id_parent',$id);
     $parent_name = fn__get_field_val_by_id('xta_obj_category','name',$parent_id);
     $parent_name_block=' <a href="/cat/'.$parent_id.'">'.$parent_name.'</a> → ';
     $parent_name_block_h1=': '.mb_strtolower($parent_name,'utf-8');
     }
  //------------------------------------------------------------------------------------------------
  
  
  
  //------------------------------------------------------------------------------------------------
  $h1 = ''.$cat_name.$parent_name_block_h1.' в Кривом Роге';
  $hone = fn__get_field_val_by_id('xta_obj_category','hone',$id);
  if (strlen($hone)) $h1 = $hone.' в Кривом Роге';
  //------------------------------------------------------------------------------------------------
  
  
  
  //------------------------------------------------------------------------------------------------
  $category_img='/themes/board/img/ico/home.png';
  if ($id) $category_img = fn__get_category_img($id);
  //------------------------------------------------------------------------------------------------
  
  
  $ret='
  <div id="fn__get_category_block_in_cat">
    <div id="fn__get_category_block_in_cat_top">
      <table style="width:100%;">
        <tr>
          <td style="padding-left:10px; width: 40px;">
           <img src="'.$category_img.'" class="image_40x40">
          </td>
          <td style="padding-left:10px;">
            <div class="top_cat_title">
             '.$parent_name_block.$cat_name.'
            </div>
            <div class="top_cat_count">
              <h1 style="font-size: 13px; display: inline; font-weight: normal; line-height: 14px;">
                '.$h1.'
              </h1>:
              '.fn__get_category_obj_count($id).' объявлений
            </div>
          </td>
          <td>
          </td>
          <td style="width:200px; padding-top: 5px; text-align:right;">
            <div class="btn-group">
              <a data-toggle="dropdown" class="btn dropdown-toggle" href="#"> 
                '.$cat_name.'
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu pull-right" role="menu" 
                  id="ddmenucat" aria-labelledby="dLabel">
              </ul>
            </div>
          </td>
        </tr>
      </table>
    </div>
    <div style="text-align:center;">
      <table style="">
        <tr>
          ++subcat++
        </tr>
      </table>
    </div>
  </div>
  <div style="border: 1px solid #D3D7CF; padding: 10px; background-color: #F3F3F3; border-bottom-left-radius: 9px; border-bottom-right-radius: 9px; margin-bottom: 20px;">

		<div class="row">
			<div class="col-md-4">
				<b>Район</b><br>
				<select style="padding-top: 5px; cursor:pointer;"  class="form-control" id="change_district">
					'.fn__get_select_districts_filter($id).'
				</select>
				<script>
						$(function(){

							$(\'#change_district\').bind(\'change\', function () {
								  var url = $(this).val(); // get selected value
								  if (url) { // require a URL
								      window.location = url; // redirect
								  }
								  return false;
							});
						});
				</script>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4"></div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-5" style="font-size:12px; color: gray;">
				Сортировка: 
					'.fn__get_sort_links(fn__get_caregory_url($id, $filters, array())).'
			</div>
		</div>

  </div>
  ';
  

  
  $sql = "SELECT * FROM `xta_obj_category` WHERE `id_parent`=".$id;
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $i=0;
  
  
  $have_parent = fn__get_field_val_by_id('xta_obj_category','id_parent',$id);
  $have_children = fn__get_count_by_where('xta_obj_category','`id_parent`='.$id,true);
  
  if ($have_children){
    if ($have_parent){
      $subcat.='
       <td class="categ_td">
         <a href="/cat/'.$have_parent.'">
           <div>
             <img src="/themes/board/img/ico/arrow_back.png" style="max-width: 100px; max-height: 100px;">
           </div>
           <div style="text-align:center; height:50px; margin-top:3px; margin-bottom:3px;" class="categ_td_text">
             <div title="Назад в категорию &quot;'.fn__get_field_val_by_id('xta_obj_category','name',$have_parent).'&quot;">Назад</div>
           </div>
         </a>
        </td>
      '; 
    }else{
      $subcat.='
       <td class="categ_td">
         <a href="'.($id?'/cat':'/').'">
           <table style="width:100px; height:100%;">
             <tr>
               <td class="">
                 <img src="/themes/board/img/ico/'.($id?'arrow_back.png':'home.png').'">
               </td>
             </tr>
             <tr>
               <td class="categ_td_text">
                 <div>'.($id?'Все объявления':'На главную').'</div>
               </td>
             </tr>
           </table>
         </a>
        </td>
      '; 
    }
  }

  

  
  foreach ($reader as $row){
    $img = $row['img'];
    if ((!file_exists($_SERVER['DOCUMENT_ROOT'].$row['img']))||(!strlen($img))){
        $img='/images/categories/nophoto.png';}
    $subcat.='
     <td class="categ_td">
       <a href="/cat/'.$row['id'].'">
       <table style="width:100px; height:100%;">
         <tr>
           <td>
             <img src="'.$img.'" alt="'.htmlspecialchars($row['name']).'">
           </td>
         </tr>
         <tr>
           <td class="categ_td_text" style="padding-left: 2px; padding-right: 2px;">
             <div title="'.fn__get_category_obj_count($row['id']).' объявлений">'.$row['name'].'</div>
           </td>
         </tr>
       </table>
       </a>
      </td>
    '; 
    $i++;
    
    
    if (($i==8)||((($i-8)%9)==0)){
       $subcat.='</tr><tr>';
    }
  }
  $ret = str_replace('++subcat++',$subcat,$ret);
  
  // Кeшируем результат функции
  fn__set_cache($id_cache,$ret,array('xta_obj_category','xta_district'));
  return $ret;
}
//****************************************************************************** 













// Возвращает изображение категории
//****************************************************************************** 
function fn__get_category_img($id=0){

  //----------------------------------------------------  
  $id_cache=__FUNCTION__.'_'.fn__get_site_id().'_'.$id;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------      
  
  $ret='';
  $img = fn__get_field_val_by_id('xta_obj_category','img',$id);
  
  $id_parent = fn__get_field_val_by_id('xta_obj_category','id_parent',$id);
  if ($id_parent){
    if (!strlen($img)){
       $img = fn__get_field_val_by_id('xta_obj_category','img',$id_parent);
       }
    
    $id_sub_parent = fn__get_field_val_by_id('xta_obj_category','id_parent',$id_parent);
    if ($id_sub_parent){
      if (!strlen($img)){
         $img = fn__get_field_val_by_id('xta_obj_category','img',$id_sub_parent);
         }
    }
  }
  
  if (strlen($img)){
     $ret = $img;
     }
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 




















// Возвращает количество объявлений в категории (+ в подкатегориях)
//****************************************************************************** 
function fn__get_category_obj_count($id=0,$in_subcategory=true){

  //----------------------------------------------------  
  $id_cache=__FUNCTION__.'_'.fn__get_site_id().'_'.$id.'_'.$in_subcategory;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------      
  
  
  if (!$id){
     $ret = fn__get_count_by_where('xta_obj');
     }else{
      $ret='0';
      $categories.= $id;
      
      if ($in_subcategory){
        $sql = "SELECT `id` FROM `xta_obj_category` WHERE `id_parent`=".$id;
        $reader =Yii::app()->db->createCommand($sql)->query(); 
        foreach ($reader as $row){
          $categories.=','.$row['id'];
          $sql = "SELECT `id` FROM `xta_obj_category` WHERE `id_parent`=".$row['id'];
          $reader1 =Yii::app()->db->createCommand($sql)->query(); 
          foreach ($reader1 as $row1){
            $categories.=','.$row1['id'];
            $sql = "SELECT `id` FROM `xta_obj_category` WHERE `id_parent`=".$row1['id'];
            $reader2 =Yii::app()->db->createCommand($sql)->query(); 
            foreach ($reader2 as $row2){
               $categories.=','.$row2['id'];
               }
          }
        }
      }
      $ret = fn__get_count_by_where('xta_obj','`id_category` in ('.$categories.')');     
     }
  
  
  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}






















// Возвращает список объявлений
//****************************************************************************** 
function fn__get_obj_list($id_cat=0,$page=1,$params=''){
  global $filters;
  fn__get_filters();
  
  
  //----------------------------------------------------
  $id_cache=__FUNCTION__.serialize(func_get_args()).$_SERVER['HTTP_HOST'].serialize($filters).$_GET['page'].$_GET['sort'];
  $ret=fn__get_cache($id_cache);
  if($ret!==false){return $ret;}
  //----------------------------------------------------
  

  
  
  
  
  
  

  //****************************************************************************
  //  Получаем список категорий, из которых брать объявления
  //****************************************************************************
  $categories_array = array($id_cat);
  if ($id_cat){
     $sql = "SELECT `id` FROM `xta_obj_category` WHERE `id_parent`=".$id_cat;
     $reader =Yii::app()->db->createCommand($sql)->query(); 
     foreach ($reader as $row){
       $categories_array[]=$row['id'];
       $sql = "SELECT `id` FROM `xta_obj_category` WHERE `id_parent`=".$row['id'];
       $reader1 =Yii::app()->db->createCommand($sql)->query(); 
       foreach ($reader1 as $row1){
         $categories_array[]=$row1['id'];
         $sql = "SELECT `id` FROM `xta_obj_category` WHERE `id_parent`=".$row['id'];
         $reader2 =Yii::app()->db->createCommand($sql)->query(); 
         foreach ($reader2 as $row2){
            $categories_array[]=$row2['id'];
            }
       }
     }
     $categories_array = array_unique($categories_array);
  }
  else
  {
     $sql = "SELECT `id` FROM `xta_obj_category` WHERE `id_parent`=".$id_cat." AND hideobjmain=0";
     $reader =Yii::app()->db->createCommand($sql)->query(); 
     foreach ($reader as $row){
       $categories_array[]=$row['id'];
       $sql = "SELECT `id` FROM `xta_obj_category` WHERE `id_parent`=".$row['id']." AND hideobjmain=0";
       $reader1 =Yii::app()->db->createCommand($sql)->query(); 
       foreach ($reader1 as $row1){
         $categories_array[]=$row1['id'];
         $sql = "SELECT `id` FROM `xta_obj_category` WHERE `id_parent`=".$row['id']." AND hideobjmain=0";
         $reader2 =Yii::app()->db->createCommand($sql)->query(); 
         foreach ($reader2 as $row2){
            $categories_array[]=$row2['id'];
            }
       }
     }
     $categories_array = array_unique($categories_array);
  }
  //****************************************************************************
  
  
  
  
  //****************************************************************************
  //  Получаем фильтр
  //****************************************************************************
    fn__get_filters();
    $tempfilters = array();
    //  Район
    if ((isset($filters['district']))&&(intval($filters['district']))){
      $sql_add=' AND (`xta_obj`.`id_district` = '.intval($filters['district']).'
                 OR  `xta_obj`.`id_district` = 0 ) ';
      $tempfilters['district'] = intval($filters['district']);
    }
    
    
  //****************************************************************************





	//****************************************************************************
	//  Получаем сортировку
	//****************************************************************************
	$cursort = '';
	if ((isset($_GET['sort']))&&(in_array($_GET['sort'],array('lowprice','highprice'))))
	{
		$cursort = $_GET['sort'];
	}
	//****************************************************************************



  
  
  
  
  //****************************************************************************
  //  Получаем корректную страницу
  //****************************************************************************
  $where_published=" `published` = 1 ";
  if ($id_cat)
  {
  	$_where = '`id_category` in ('.implode(',',$categories_array).') AND'.$where_published.$sql_add;
  	$count_obj = fn__get_count_by_where('xta_obj',$_where);
  }
  else
  {
    //$count_obj = fn__get_count_by_where('xta_obj',$where_published.$sql_add);
  	$_where = '`id_category` in ('.implode(',',$categories_array).') AND'.$where_published.$sql_add;
  	$count_obj = fn__get_count_by_where('xta_obj',$_where);
  }
  $pages_count = floor($count_obj/20);
  if ($count_obj%20){$pages_count++;}
  $page = fn__get_correct_page($pages_count);
  //****************************************************************************
  
  
  //****************************************************************************
  //  Получаем пагинатор
  //****************************************************************************
  $paginator_url = fn__get_caregory_url($id_cat, $tempfilters,array('sort'=>$cursort,'page'=>''));
  $_paginator=fn__get_pagination($paginator_url, $pages_count, $page);
  //****************************************************************************
  
  
  

  
  
  //****************************************************************************
  // Получаем сами объявления
  //****************************************************************************
	$sort = "ORDER BY `xta_obj`.`createdon` DESC ";
	if ($cursort == 'lowprice')
	{
		$sort = "ORDER BY `xta_obj`.`price` ASC";
	}
	if ($cursort == 'highprice')
	{
		$sort = "ORDER BY `xta_obj`.`price` DESC";
	}
  $start = 20*(intval($page)-1);
  $sql = "SELECT 
            `xta_obj`.*,
            `xta_valuta`.`shortname`
          FROM `xta_obj` 
          LEFT JOIN `xta_valuta` on `xta_valuta`.`id` = `xta_obj`.`id_valuta`
          WHERE 
            ".$where_published." 
            AND `xta_obj`.`id_category` in (".implode(',',$categories_array).") 
            ".$sql_add."
            ".$sort."
          LIMIT ".$start.", 20
          ";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $i=0;
  foreach ($reader as $row){
  
  
$price='';
if (intval($row['price'])){
  $price=$row['price'].' '.$row['shortname'];
}

	$district='Все районы города';
	if (intval($row['id_district']))
	{
		$district=fn__get_field_val_by_id('xta_district','name',$row['id_district']);
	}
  
  
  $ret.='
  <div style="border-bottom:1px #babdb6 solid; padding-top:10px; padding-bottom:4px;">
  <table style="width:100%;">
    <tr>
      <td style="width:100px; text-align: center;">
        '.$row['createdon'].'
      </td>
      <td style="width:100px; text-align:center;">
        <img src="'.fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album'],false),'thumb_',80,60).'" alt="'.trim($row['name']).'" style="border-radius:4px;">
      </td>
      <td>
        <div style="font-size:16px; font-weight:bold; padding-left:0px;">
          <a href="/board/'.$row['id'].'">'.trim($row['name']).' </a>
        </div>
        <div style="color:gray; font-size: 12px;">
          '.fn__get_rubric_selector($row['id_category'],false).'
        </div>
        <div style="color:gray; font-size: 12px;">
          Кривой Рог → '.$district.'
        </div>
      </td>
      <td style="width:120px; font-size: 16px; text-align:right;">
        <strong class="c000">
          '.$price.'
        </strong>
      </td>
    </tr>
  </table>
  </div>
  ';
  
	if (($i==10)&&($id_cat!=0))
	{
		$ret.='<div style="width:100%;">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90, создано 31.08.10 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9808838827380357"
     data-ad-slot="5365470580"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
					</div>';
	}
	
  $i++;
  }
  //****************************************************************************
  
  
  $ret.=$_paginator;

 
  
  // Кeшируем результат функции
  fn__set_cache($id_cache,$ret,array('xta_obj','xta_valuta','xta_obj_category'));
  return $ret;
}
//****************************************************************************** 


























// Возвращает изображение категории
//****************************************************************************** 
function fn__get_admin_category_tree($id_obj_option=0){


  $sql="SELECT * FROM `xta_obj_category` WHERE `id_parent` = 0 ORDER BY `name`  ASC  ";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $select='
  <table class="items table table-striped table-bordered table-condensed" style="height:10px;">
    <thead>
      <tr>
       <th id="yw0_c0">#</th>
       <th id="yw0_c1" colspan="3">Название</th>
       <th class="button-column" id="yw0_c2" style="width:50px;">Выбрать</th>
      </tr>
    </thead>
    
	  ';
	  
	  foreach ($reader as $row){
      $select.='
      <tr>
        <td style="width: 30px;">'.$row['id'].'</td>
        <td colspan="3">'.$row['name'].' ('.fn__get_count_by_where('xta_obj_category','`id_parent` = '.$row['id']).')</td>
        <td style="width: 50px;">
          <input type="checkbox" name="category['.$row['id'].']" '.((fn__get_count_by_where('xta_obj_option_in_category','`id_obj_option` = '.$id_obj_option.' AND `id_obj_category`='.$row['id']))?' checked="checked"':'').' value="1" onchange=" $(\'.id_parent_'.$row['id'].'\').prop(\'checked\', $(this).prop(\'checked\')); " />
        </td>
      </tr>

      ';

	    $sql="SELECT * FROM `xta_obj_category` 
	          WHERE `id_parent` = ".$row['id']." ORDER BY `name` ASC";
	    $reader1 =Yii::app()->db->createCommand($sql)->query(); 
	    foreach ($reader1 as $row1){
	    $select.='
        <tr class="level_2_'.$row['id'].'">
          <td style="width: 30px;">'.$row1['id'].'</td>
          <td style="width:30px;">&nbsp;</td>
          <td  colspan="2">'.$row1['name'].'  ('.fn__get_count_by_where('xta_obj_category','`id_parent` = '.$row1['id']).')</td>
          <td style="width: 50px;">
             
             <input type="checkbox" name="category['.$row1['id'].']" '.((fn__get_count_by_where('xta_obj_option_in_category','`id_obj_option` = '.$id_obj_option.' AND `id_obj_category`='.$row1['id']))?' checked="checked"':'').' class="id_parent_'.$row['id'].'" value="1" onchange=" $(\'.id_parent_'.$row1['id'].'\').prop(\'checked\', $(this).prop(\'checked\')); " />
          </td>
        </tr>
	    ';
	    
	    
	    
	    
	        $sql="SELECT * FROM `xta_obj_category` 
	              WHERE `id_parent` = ".$row1['id']." ORDER BY `name` ASC";
	        $reader2 =Yii::app()->db->createCommand($sql)->query(); 
	        foreach ($reader2 as $row2){
	        $select.='
            <tr class="level_3_'.$row1['id'].'">
              <td style="width: 30px;">'.$row2['id'].'</td>
              <td style="width:30px;">&nbsp;</td>
              <td style="width:30px;">&nbsp;</td>
              <td>'.$row2['name'].'</td>
              <td style="width: 50px;">
               <input type="checkbox" name="category['.$row2['id'].']" '.((fn__get_count_by_where('xta_obj_option_in_category','`id_obj_option` = '.$id_obj_option.' AND `id_obj_category`='.$row2['id']))?' checked="checked"':'').' class="id_parent_'.$row1['id'].' id_parent_'.$row['id'].'"  value="1" />  
              </td>
            </tr>
	        ';
	        }
	    
	    
	    }
      
	  }
	  $select.='</table>';
  $ret= $select;   
  
  return $ret;  
}
//****************************************************************************** 









// Возвращает изображение категории
//****************************************************************************** 
function fn__get__add_option($id_obj_option=0, $post=array()){

  $tpl='
  <tr>
    <td class="b_add_legend" style="padding-top:5px;">
      +option_name+

      <span title="Обязательное поле">*</span>
    </td>
    <td class="td_b_add_input" style="padding-top:5px;">
      <div class="b_add_legend_bottom popover_hover" data-title="+option_description+" data-content="+option_introtext+" rel="popover">
        +option_input+
        <div class="b_add_legend_bottom_text">
          +option_description+
        </div>
      </div>
      
    </td>
    <td style="vertical-align:top;" style="padding-top:5px;">
<script>
$(".popover_hover").popover({ trigger: "hover click",\'html\': true});
</script>
    </td>
  </tr>
  ';

  $option_type = fn__get_field_val_by_id('xta_obj_option','option_type',$id_obj_option);

  if ($option_type==0){ //'select'
    $_name = fn__get_field_val_by_id('xta_obj_option','name',$id_obj_option);
    $tpl = str_replace('+option_name+',$_name,$tpl);
    
    
    $sql = 'SELECT * FROM `xta_obj_option_available` WHERE `id_obj_option` = '.$id_obj_option.'
            ORDER BY `value` ASC ';

    $_default = 0;
    if (isset($post['formdata']['option_'.$id_obj_option])){
       $_default = intval($post['formdata']['option_'.$id_obj_option]);
       }
    $tpl_1 = '<option value="+id+" +default+>+value+</option>';
    $_options = fn__get_select_by_sql_i_tpl($sql,$tpl_1,$_default);
    
    $_input = '
      <select class="span3 popover_hover" name="formdata[option_'.$id_obj_option.']" id="b_add_form_option_'.$id_obj_option.'"  style="cursor: pointer !important;">
       '.$_options.'
      </select>
    ';
    $tpl = str_replace('+option_input+',$_input,$tpl);
    
    $_description = fn__get_field_val_by_id('xta_obj_option','description',$id_obj_option);
    $tpl = str_replace('+option_description+',$_description,$tpl);
    
    $_introtext = fn__get_field_val_by_id('xta_obj_option','introtext',$id_obj_option);
    $tpl = str_replace('+option_introtext+',$_introtext,$tpl);
    
    $ret = $tpl;
  }

  return $ret;  
}
//****************************************************************************** 



























// Возвращает имя категории на верхнем уровне
//****************************************************************************** 
function fn__get_category_first_name($id_category=0){
  
  $ret=fn__get_field_val_by_id('xta_obj_category','name',$id_category);
  
  $id_parent = fn__get_field_val_by_id('xta_obj_category','id_parent',$id_category);
  if ($id_parent){
    $ret=fn__get_field_val_by_id('xta_obj_category','name',$id_parent);
    
    $id_sub_parent = fn__get_field_val_by_id('xta_obj_category','id_parent',$id_parent);
    if ($id_sub_parent){
      $ret=fn__get_field_val_by_id('xta_obj_category','name',$id_sub_parent);
    
    }
  }

  return $ret;  
}
//****************************************************************************** 














// Возвращает дерево категорий
//****************************************************************************** 
function fn__get_category_tree_list(){
  return '';
  //----------------------------------------------------  
  $id_cache=__FUNCTION__;
  $ret=Yii::app()->cache->get($id_cache);
  if($ret!==false)
    {
      return $ret;
    }
  //----------------------------------------------------      
  $sql="SELECT `id`,`name` FROM `xta_obj_category` WHERE `id_parent` = 0";
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $ret='';
  $i0=0;
  foreach ($reader as $row){
    $childcount=fn__get_count_by_where('xta_obj_category','`id_parent` = '.$row['id'],true);
    $ret.='
      <li '.($childcount?'class="dropdown-submenu  pull-left"':'').'>
        <a tabindex="-1" href="/cat/'.$row['id'].'">'.$row['name'].'</a>';
    if ($childcount){
         $sql="SELECT `id`,`name` FROM `xta_obj_category` WHERE `id_parent` = ".$row['id'];
         $reader1 =Yii::app()->db->createCommand($sql)->query(); 
         $ret.='<ul class="dropdown-menu " style="min-width: 280px; margin-top:'.(($i0>=$childcount)?(-26*($childcount-1)):(-26*$i0)).'px;">';
         $i1=0;
         foreach ($reader1 as $row1){
            $childcount1=fn__get_count_by_where('xta_obj_category','`id_parent` = '.$row1['id'],true);
            $ret.='
              <li '.($childcount1?'class="dropdown-submenu pull-left"':'').'>
                <a tabindex="-1" href="/cat/'.$row1['id'].'">'.$row1['name'].'</a>';
            if ($childcount1){
               $sql="SELECT `id`,`name` FROM `xta_obj_category` WHERE `id_parent` = ".$row1['id'];
               $reader2 =Yii::app()->db->createCommand($sql)->query(); 
               $ret.='<ul class="dropdown-menu " style="min-width: 280px; margin-top:'.(($i1>=$childcount1)?(-26*($childcount1-1)):(-26*$i1)).'px;">';
               foreach ($reader2 as $row2){
                   $ret.='
                      <li>
                        <a tabindex="-1" href="/cat/'.$row2['id'].'">'.$row2['name'].'</a>
                      </li>
                      ';
                   }
               $ret.='</ul>';
               }
            $ret.='</li>';
            $i1++;
            }
         $ret.='</ul>';
       }
    $ret.='
      </li>
    ';
    $i0++;
  }
  $ret.='';

  // Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 
















// 
//****************************************************************************** 
function fn__get_post_obj_options($id_category=0){
  $id_category = intval($id_category);
    
  $sql='
    SELECT DISTINCT
      `id_obj_option`
    FROM 
      `xta_obj_option_in_category`
    WHERE
      `id_obj_category` = '.$id_category.'';
      
  $ret='';
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  $i=0;
  foreach ($reader as $row){
     $option = fn__get__add_option($row['id_obj_option'],$_POST);
     $ret.=$option;
     $i++;
  }
  $ret = '
  <table style="width:100%; height:100%;">
  '.$ret.'
  </table>
  ';
  return $ret;  
}
//****************************************************************************** 





















// Функция проверяет корректность заполненных полей при добавлении объявления
//******************************************************************************
function fn__obj_add_validate($post){
  $params = array(
                  array('required_fields',
                        array('name'=>'Заголовок объявления',
                              'id_category'=>'Категория',
                              'id_district'=>'Район',
                              'about'=>'Текст объявления',
                              'price'=>'Цена',
                              'id_valuta'=>'Валюта',
                              'phone'=>'Номер телефона',
                              'username'=>'Контактное лицо',
                             )
                      ),
                 array('length',array('name'=>'Заголовок объявления'),array('minlength'=>20)),
                 array('length',array('name'=>'Заголовок объявления'),array('maxlength'=>70)),
                 array('nostopwords',array('name'=>'Заголовок объявления')),
                 array('notags',array('name'=>'Заголовок объявления')),
                 
                 
                 array('number',array('id_category'=>'Категория')),
                 array('exists',array('id_category'=>'Категория'),
                                   array('table'=>'xta_obj_category',
                                         'field'=>'id',
                                         'message'=>'Выбрана несуществующая категория')),
                 
                 array('exists',array('id_district'=>'Район'),
                                   array('table'=>'xta_obj_category',
                                         'field'=>'id',
                                         'allowarr'=>array(0),
                                         'message'=>'Выбран несуществующий район')),
                                         
                 array('length',array('about'=>'Текст объявления'),array('minlength'=>100)),
                 array('length',array('about'=>'Текст объявления'),array('maxlength'=>6000)),
                 array('nostopwords',array('about'=>'Текст объявления')),
                 array('notags',array('about'=>'Текст объявления')),
                 
                 array('number',array('price'=>'Цена')),
                 
                 array('number',array('id_valuta'=>'Валюта')),
                 array('exists',array('id_valuta'=>'Валюта'),
                                   array('table'=>'xta_valuta',
                                         'field'=>'id',
                                         'message'=>'Выбрана несуществующая валюта')),
                                         
                 array('number',array('phone'=>'Номер телефона')),
                 array('length',array('phone'=>'Номер телефона'),array('minlength'=>6)),
                 array('length',array('phone'=>'Номер телефона'),array('maxlength'=>12)),
                 
                 array('length',array('username'=>'Контактное лицо'),array('minlength'=>3)),
                 array('length',array('username'=>'Контактное лицо'),array('maxlength'=>40)),
                 array('nostopwords',array('username'=>'Контактное лицо')),

                 array('length',array('skype'=>'Skype'),array('maxlength'=>20)),
                 array('skype',array('skype'=>'Skype')),
                 array('nostopwords',array('skype'=>'Skype')),
                 
                 
                 );
                 
                 
                 
                 

                 
  $params_email = array(
                 array('required_fields',array('email'=>'Email')),
                 array('length',array('email'=>'Email'),array('minlength'=>6)),
                 array('email',array('email'=>'Email')),
                 array('notexists',array('email'=>'Email'),
                                   array('table'=>'xta_user',
                                         'field'=>'email',
                                         'message'=>'Такой email уже зарегистрирован')),
                 );
  if (!fn__get_user_id()){
     $params = array_merge($params,$params_email);
     }
  $result_validate = fn__validate_field_list($post,$params);
  return $result_validate;
}
//******************************************************************************   





















// Функция проверяет корректность заполненных полей при добавлении объявления
//******************************************************************************
function fn__obj_add_prepare_post_data($post){
  $params = array(
                  'name'=>array('strip_tags','htmlspecialchars'),
                  'id_category'=>array('intval'),
                  'id_district'=>array('intval'),
                  'about'=>array('strip_tags','htmlspecialchars'),
                  'price'=>array('intval'),
                  'id_valuta'=>array('intval'),
                  'phone'=>array('get_only_number'),
                  'username'=>array('strip_tags','htmlspecialchars'),
                  'email'=>array('strip_tags'),
                  'skype'=>array('strip_tags'),
                 );
  $post = fn__prepare_post_data($post,$params);
  
  
  return $post;
}
//******************************************************************************   
























// Функция обрабатывает опции объвлений
//******************************************************************************
function fn__obj_add_prepare_options($post){
  $post['option']= array();
  $sql='SELECT DISTINCT
          `id_obj_option`
        FROM 
          `xta_obj_option_in_category`
        WHERE
          `id_obj_category` = '.$post['id_category'].'';
    $reader =Yii::app()->db->createCommand($sql)->query(); 
    foreach ($reader as $row){
       if (isset($_POST['formdata']['option_'.$row['id_obj_option']])){
          $_value = intval($_POST['formdata']['option_'.$row['id_obj_option']]);
          $correct = fn__get_count_by_where('xta_obj_option_available',
                     '`id_obj_option` = '.$row['id_obj_option'].' AND `id` = '.$_value);
          if (intval($correct)){
             $post['option'][$row['id_obj_option']] = intval($_POST['formdata']['option_'.$row['id_obj_option']]);
             }
          }
    }
  return $post;
}
//******************************************************************************























// СОздаем объявление
//******************************************************************************
function fn__create_obj($data,$id_user=0){
  if (!$id_user){
     $id_user=fn__get_user_id();
     }
  $id_album = fn__create_album('obj_new');
  $sql ="INSERT INTO `xta_obj`(
            `id_city`, 
            `id_district`, 
            `id_category`, 
            `id_user`, 
            `id_album`,
            `id_valuta`,
            `username`, 
            `name`, 
            `about`, 
            `phone`, 
            `skype`,
            `unpub_date`,
            `price`
            ) 
        VALUES (
            '".fn__get_field_val_by_id('xta_site','id_city',fn__get_site_id())."',
            '".$data['id_district']."',
            '".$data['id_category']."',
            '".$id_user."',
            '".$id_album."',
            '".$data['id_valuta']."',
            '".sql_valid($data['username'])."',
            '".sql_valid($data['name'])."',
            '".sql_valid($data['about'])."',
            '".$data['phone']."',
            '".sql_valid($data['skype'])."',
            DATE_ADD(NOW(), INTERVAL 30 DAY),
            '".$data['price']."'
        )
      ";
  Yii::app()->db->createCommand($sql)->execute();
  $id_obj = fn__get_max_table_id('xta_obj');

  $sql = "UPDATE `xta_obj` SET `id_album`=".$id_album." WHERE `id`=".$id_obj;
  Yii::app()->db->createCommand($sql)->execute();

  // Отправляем письмо админу о создании пользователя
  $tpl = fn__get_setting('tpl__email_obj_create_to_admin');
  $tpl = str_replace('+http_host+',ucfirst($_SERVER['HTTP_HOST']),$tpl);
  $tpl = str_replace('+username+',$data['username'],$tpl);
  $tpl = str_replace('+name+',$data['name'],$tpl);
  $tpl = str_replace('+id+',$id_obj,$tpl);
  $sublect = 'На сайте '.ucfirst($_SERVER['HTTP_HOST']).' добавлено объявление';
  $to = fn__get_setting('admin_email');
  fn__send_email($to,$sublect, $tpl);
      



  foreach ($data['option'] as $key => $value){
    $sql ="INSERT INTO `xta_obj_option_value`(
             `id_obj`, 
             `id_obj_option`, 
             `id_obj_option_available`) 
           VALUES (
             '".$id_obj."',
             '".intval($key)."',
             '".intval($value)."'
           )";
    Yii::app()->db->createCommand($sql)->execute();
    }


  for ($i = 0; $i < 7; $i++){
    if ((isset($_POST['img'.$i]))&&(strlen($_POST['img'.$i]))){
         $res = fn__save_base64_to_img($id_album,$_POST['img'.$i]);
       }
  }

  return $id_obj;
}
//******************************************************************************














// Функция возвращает массив с ID других объявлений автора
//******************************************************************************
function fn__get_author_another_obj_id($id_obj)
{
  //----------------------------------------------------
  $id_cache=__FUNCTION__.serialize(func_get_args()).$_SERVER['HTTP_HOST'];
  $ret=fn__get_cache($id_cache);
  if($ret!==false){return $ret;}
  //----------------------------------------------------
  
  $ret = array();
	$id_author = fn__get_field_val_by_id('xta_obj','id_user',$id_obj);
	$id_category = fn__get_field_val_by_id('xta_obj','id_category',$id_obj);
	
	$total_user_obj = fn__get_count_by_where('xta_obj','`id_user` = '.$id_author." AND `id`<>".$id_obj." AND `published`=1");
	if ($total_user_obj)
	{
		$obj_count_in_cur_category = fn__get_count_by_where('xta_obj','`id_user` = '.$id_author.' AND `id_category`='.$id_category." AND `id`<>".$id_obj." AND `published`=1");
		if ($obj_count_in_cur_category)
		{
			$sql = "SELECT * FROM `xta_obj` WHERE `id_user` = ".$id_author." AND `id_category`=".$id_category." AND `published`=1 ORDER BY RAND() DESC  LIMIT 0,3";
			$reader =Yii::app()->db->createCommand($sql)->query(); 
			$ret = array();
			foreach ($reader as $row)
			{
				$ret[]=$row['id'];
			}
			return $ret;
		}
	}
	
  // Кeшируем результат функции
  fn__set_cache($id_cache,$ret,array('xta_obj'));
  return $ret;
}
//******************************************************************************








// Функция возвращает блок с объявлениями
//******************************************************************************
function fn__obj_block($array_id_obj)
{
	//----------------------------------------------------
	$id_cache=__FUNCTION__.serialize(func_get_args()).$_SERVER['HTTP_HOST'];
	$ret=fn__get_cache($id_cache);
	if($ret!==false){return $ret;}
	//----------------------------------------------------
	
	$sql = "SELECT 
						`xta_obj`.*,
						`xta_valuta`.`shortname`
					FROM `xta_obj` 
					LEFT JOIN `xta_valuta` on `xta_valuta`.`id` = `xta_obj`.`id_valuta`
					WHERE `xta_obj`.`id` in (".implode(',',$array_id_obj).")";
	$reader =Yii::app()->db->createCommand($sql)->query(); 
	foreach ($reader as $row)
	{
	
		$price='';
		if (intval($row['price']))
		{
			$price=$row['price'].' '.$row['shortname'];
		}

		$ret.='
<div class="account_obj_item" style="'.$obj_item_style.'">
  <table style="width:100%;">
    <tr>
      <td style="font-size:12px; padding-left:4px; padding-right:8px; width:90px; text-align:center;">
        '.$row['createdon'].'
      </td>
      <td style="width:56px; padding-left:8px; padding-right:8px;">
        <a class="thumbnail" rel="tooltip" data-title="Фотография объявления" style="margin-bottom: 0px;">
            <img src="'.fn__get_img_thumb(fn__get_album_mainimg_src($row['id_album'],true),'thumb_',50,40).'" alt="">
        </a>
      </td>
      <td style="vertical-align:top; padding-left:6px;">
        <div style="font-size:15px; padding-left:3px;">
          <a href="/board/'.$row['id'].'">'.$row['name'].' </a>
        </div>
        <div style="color:gray; font-size: 12px;">
          '.fn__get_rubric_selector($row['id_category'],false).'
        </div>
      </td>
      <td style="font-size:15px; padding-left:2px; padding-right:2px; width:80px; font-weight:bold; text-align:center;">
        '.$price.'
      </td>
    </tr>
  </table>
</div>
       '; 
	}
	
	
	// Кeшируем результат функции
	fn__set_cache($id_cache,$ret,array('xta_obj'));
	return $ret;
}
//******************************************************************************





// Функция создает блок ссылок для сортировки
//******************************************************************************
function fn__get_sort_links($current_url){

	$original_current_url = $current_url;
	if (strpos($current_url,'?')===false)
	{
		$current_url.='?sort=';
	}
	else
	{
		$current_url.='&sort=';
	}

	$cursort = '';
	if ((isset($_GET['sort']))&&(in_array($_GET['sort'],array('newest','lowprice','highprice'))))
	{
		$cursort = $_GET['sort'];
	}
	else
	{
		$cursort = 'newest';
	}
  
  //echo $cursort; exit;
  $ret=($cursort=='newest')?'<span style="color:#000;margin-left:10px;">Самые новые</span>' : '<a href="'.$original_current_url.'" style="margin-left:10px;">Самые новые</a>';

  $ret.=($cursort=='lowprice')?'<span style="color:#000;margin-left:10px;">Самые дешевые</span>' : '<a href="'.$current_url.'lowprice" style="margin-left:10px;">Самые дешевые</a>';

  $ret.=($cursort=='highprice')?'<span style="color:#000;margin-left:10px;">Самые дорогие</span>' : '<a href="'.$current_url.'highprice" style="margin-left:10px;">Самые дорогие</a>';
  
  return $ret;
}
//******************************************************************************










