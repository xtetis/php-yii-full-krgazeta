<?php 








// Получаем кеш
// $id_cache - передаем строку с ID кеша и получаем значение кеша
// v 0.2
//**************************************************************************************************
function fn__get_cache($id_cache=''){
	$cache=((isset($_GET['nocache']))&&intval($_GET['nocache']))?false:true;
	if (fn__get_user_role()=='admin')
	{
		$cache = false;
	}
  $id_cache = md5($id_cache);
  $ret=Yii::app()->cache->get($id_cache);
  if (($ret!==false)&&($cache)){
     return $ret;
     }
  return false;
}
//**************************************************************************************************
















// Возвращает значение кеша
// $id_cache - уникальное значение для формирования файлы
// $tags - Теги для формирования файла кеша
// $value
// v 0.2
//**************************************************************************************************
function fn__set_cache($id_cache,$value,$tags=array(),$allsite=false){
  $id_cache = md5($id_cache);
  Yii::app()->cache->set($id_cache, $value, 99999999); 

  $cache_exists=fn__get_count_by_where('xta_cache',"`name`='".$id_cache."'");
  if ($cache_exists){
     $id = fn__get_fieldval_by_where('xta_cache','id',"`name`='".$id_cache."'");
     fn__del_record_by_id('xta_cache',$id);
     $sql = "DELETE FROM `xta_cache_in_tags` WHERE `id_cache`=".$id;
     Yii::app()->db->createCommand($sql)->execute();
     }
     
  $sql = "INSERT INTO `xta_cache`(`name`) VALUES ('".$id_cache."');";
  Yii::app()->db->createCommand($sql)->execute();
  $id = fn__get_max_table_id('xta_cache');

  if (!$allsite){$tags[]=$_SERVER['HTTP_HOST'];}
  foreach ($tags as $tag){
      $tag_exists = fn__get_count_by_where('xta_cache_tags',"`name`= '".$tag."'");
      if ($tag_exists){
         $id_tag = fn__get_fieldval_by_where('xta_cache_tags','id' ,"`name`= '".$tag."'");
         }else{
         $sql = "INSERT INTO `xta_cache_tags`(`name`) VALUES ('".$tag."')";
         Yii::app()->db->createCommand($sql)->execute();
         $id_tag = fn__get_max_table_id('xta_cache_tags');
         }
      
      $cache_in_tags_exists = fn__get_count_by_where('xta_cache_in_tags',
                                                    "`id_cache`= ".$id." AND `id_cache_tag`=".$id_tag);
      if (!$cache_in_tags_exists){
          $sql = "INSERT INTO `xta_cache_in_tags`(`id_cache`, `id_cache_tag`) 
                  VALUES (".$id.",".$id_tag.")";
          Yii::app()->db->createCommand($sql)->execute();
         }
    }
}
//**************************************************************************************************


















// Очищает кеш по имеющимся тегам
// Парамет бывает в 4 состояниях
// 0 = удалить мультисайтовый кеш (который существует без привязки к сайту)
// 1 = удалить кеш текущего сайта
// 2 = удалить кеш всех сайтов кроме мультисайтового
// 3 = удалить кеш всех сайтов и мультисайтовый
// v 0.2
//**************************************************************************************************
function fn__clear_cache($tags=array(),$allsite=true){
  
  if ((!count($tags))&&$allsite){
     Yii::app()->cache->flush();
     $sql = "DELETE FROM `xta_cache_in_tags`";
     Yii::app()->db->createCommand($sql)->execute();
     $sql = "DELETE FROM `xta_cache`";
     Yii::app()->db->createCommand($sql)->execute();
     return true;
     }
     
  
  $tagsstr = array();
  foreach ($tags as $tag){$tagsstr[] = "'".$tag."'";}
  $tagsstr = implode(',',$tagsstr);
  
  $sql = "SELECT * FROM `xta_cache` WHERE `id` in 
           (
             SELECT `id_cache` FROM `xta_cache_in_tags` WHERE `id_cache_tag` IN 
               (
                 SELECT `id` FROM `xta_cache_tags` WHERE `name` in (".$tagsstr.")
               )
           )";
  if (!$allsite){
     $sql.="
     AND `id` in 
             (
               SELECT `id_cache` FROM `xta_cache_in_tags` WHERE `id_cache_tag` IN 
                 (
                   SELECT `id` FROM `xta_cache_tags` WHERE `name` in ('".$_SERVER['HTTP_HOST']."')
                 )
             )
     ";
     }
  $reader =Yii::app()->db->createCommand($sql)->query(); 
  foreach ($reader as $row){
     Yii::app()->cache->delete($row['name']);
     }
  $sql = "DELETE FROM `xta_cache` WHERE ".$sql_site." `id` in (
          SELECT `id_cache` FROM `xta_cache_in_tags` WHERE `id_cache_tag` IN (
           SELECT `id` FROM `xta_cache_tags` WHERE `name` in (".$tagsstr.")
          )
         )";
  if (!$allsite){
     $sql.="
     AND `id` in 
             (
               SELECT `id_cache` FROM `xta_cache_in_tags` WHERE `id_cache_tag` IN 
                 (
                   SELECT `id` FROM `xta_cache_tags` WHERE `name` in ('".$_SERVER['HTTP_HOST']."')
                 )
             )
     ";
     }
  Yii::app()->db->createCommand($sql)->execute();
  $sql = "DELETE FROM `xta_cache_in_tags` WHERE `id_cache_tag` IN (
           SELECT `id` FROM `xta_cache_tags` WHERE `name` in (".$tagsstr.")
          )";
  if (!$allsite){
     $sql.="
     AND `id_cache` in 
             (
               SELECT `id_cache` FROM `xta_cache_in_tags` WHERE `id_cache_tag` IN 
                 (
                   SELECT `id` FROM `xta_cache_tags` WHERE `name` in ('".$_SERVER['HTTP_HOST']."')
                 )
             )
     ";
     }
  Yii::app()->db->createCommand($sql)->execute();
}
//**************************************************************************************************


?>
