<?php 






// Аналог функции mysql_real_escape_string(), но без подключения к MySQL
//******************************************************************************
 function sql_valid($data) {
  $data = str_replace("\\", "\\\\", $data);
  $data = str_replace("'", "\'", $data);
  $data = str_replace('"', '\"', $data);
  $data = str_replace("\x00", "\\x00", $data);
  $data = str_replace("\x1a", "\\x1a", $data);
  $data = str_replace("\r", "\\r", $data);
  $data = str_replace("\n", "\\n", $data);
  return($data); 
 }  
//******************************************************************************





// Функция возвращает максимальный ID страницы
//****************************************************************************** 
function fn__get_max_table_id($table=''){
   $sql="SELECT MAX(`id`) as 'max' FROM `".$table."`";
   $row =Yii::app()->db->createCommand($sql)->queryRow(); 
   return $row['max'];
}
//****************************************************************************** 






// Функция возвращает максимальный ID страницы
//****************************************************************************** 
function fn__del_record_by_id($table='',$id=0){
   $sql = "DELETE FROM `".$table."` WHERE `id`=".intval($id);
   Yii::app()->db->createCommand($sql)->execute();
}
//****************************************************************************** 




// Функция возвращает значение поля по ID
//****************************************************************************** 
function fn__get_field_val_by_id($table='',$field='',$id=0){
  //----------------------------------------------------  
  $id_cache=__FUNCTION__.serialize(func_get_args());
  $ret=Yii::app()->cache->get($id_cache);
  if (($ret!==false)&&($cache))
    {
      return $ret;
    }
  //---------------------------------------------------- 
   
   $sql = "SELECT `".$field."` FROM `".$table."` WHERE `id`=".intval($id);
   $row =Yii::app()->db->createCommand($sql)->queryRow(); 
   $ret=$row[$field];
   
  //Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 





// Функция возвращает список по скрипту по шаблону
//****************************************************************************** 
function fn__get_select_by_sql_i_tpl($sql='',$tpl='',$default_id=0){ 
   $reader =Yii::app()->db->createCommand($sql)->query(); 
   foreach ($reader as $row)    
      {
        $_tpl = $tpl;
        foreach ($row as $key => $value)
        {
          $_tpl=str_replace('+'.$key.'+',$value,$_tpl);
          if (($default_id)&&($key=='id')){
                if ($value==$default_id){
			              $_tpl=str_replace('+default+',' selected="selected"',$_tpl);
                   }else{
			              $_tpl=str_replace('+default+','',$_tpl);
                   }
             }
          
        }
        $_tpl=str_replace('+default+','',$_tpl);
        $ret.=$_tpl;
      }
  return $ret;  
}
//****************************************************************************** 









// Функция возвращает количество запосей в таблице
//****************************************************************************** 
function fn__get_count_by_where($table='',$where='',$cache=false){ 
  
  //----------------------------------------------------  
  $id_cache=__FUNCTION__.serialize(func_get_args());
  $ret=Yii::app()->cache->get($id_cache);
  if (($ret!==false)&&($cache))
    {
      return $ret;
    }
  //---------------------------------------------------- 
  
  
  if (strlen($where)){$where=' WHERE '.$where;}
  $from = "`".$table."`";
  $from = str_replace('``','`',$from);
  $sql = "SELECT count(`id`) as 'count' FROM ".$from." ".$where;
  $row =Yii::app()->db->createCommand($sql)->queryRow(); 
  $ret=intval($row['count']);
   
  //Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 







// Функция возвращает значение поля
//****************************************************************************** 
function fn__get_fieldval_by_where($table='', $field='' ,$where='', $cache=false){ 
  //----------------------------------------------------  
  $id_cache=__FUNCTION__.serialize(func_get_args());
  $ret=Yii::app()->cache->get($id_cache);
  if (($ret!==false)&&($cache))
    {
      return $ret;
    }
  //---------------------------------------------------- 

  if (strlen($where)){$where=' WHERE '.$where;}
  $sql = "SELECT `".$field."` as 'field' FROM `".$table."` ".$where;
  $row =Yii::app()->db->createCommand($sql)->queryRow(); 
  $ret=$row['field'];
   
  //Кeшируем результат функции
  Yii::app()->cache->set($id_cache, $ret, fn__get_setting('cache_live_sec')); 
  return $ret;  
}
//****************************************************************************** 





// Функция проверяет наличие записи в талице
//****************************************************************************** 
function fn__get_record_exists($table='',$id=''){ 
  $sql = "SELECT count(`id`) as 'count' FROM `".$table."` WHERE `id`=".intval($id);
  $row =Yii::app()->db->createCommand($sql)->queryRow(); 
  $ret=intval($row['count']);
  return $ret;  
}
//****************************************************************************** 




?>
